$(function(){
	'use strict';
	
	$('html').removeClass('no-js').addClass('js');
	
	/*=========================================================================
		Initialize fitVids
	=========================================================================*/
	$('.video-container').fitVids();
	
	/*=========================================================================
		Menu Functioning
	=========================================================================*/
	$('.menu-btn').on('click', function(e){
		e.preventDefault();
		$('body').toggleClass('show-menu');
	});
	
	$('.menu > ul > li > a').on('click', function(){
		var $offset = $( $(this).attr('href') ).offset().top;
		$('body, html').animate({
			scrollTop: $offset
		}, 700);
		$('body').removeClass('show-menu');
	});
	
	
	
	$(window).on('load', function(){
		
		$('body').addClass('loaded');
		
	});
	
	/*=========================================================================
		Screenshots popup box
	=========================================================================*/
	$('.screenshots-slider li > .inner > .overlay > a').magnificPopup({
		type: 'image',
		gallery:{
			enabled:true
		}
	});
	
	/*=========================================================================
		Testimonials Slider
	=========================================================================*/
	$('.testimonials-slider').owlCarousel({
		items: 1,
		autoplay: true,
		autoplaySpeed: 1000
	});
	
	/*=========================================================================
		Screenshots Slider
	=========================================================================*/
	$('.screenshots-slider').owlCarousel({
		items: 4,
		autoplay: true,
		autoplaySpeed: 1500,
		responsive: {
			1024: {
				items: 4
			},
			992: {
				items: 3 
			},
			768: {
				items: 2
			},
			0: {
				items: 1
			}
		}
	});
	
	
	
	/*=========================================================================
		Contact Form
	=========================================================================*/


});