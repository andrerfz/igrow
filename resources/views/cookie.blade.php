
@section('cookie')

    <div id="cookies" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="background-color: rgba(0, 0, 0, 0.84);">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cookies</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="kt-scroll ps" data-scroll="true" data-height="400" style="height: 400px; overflow: hidden;">

                        <div class="kt-portlet">
                            <div class="kt-portlet__body">
                                <ul class="nav nav-tabs  nav-tabs-line" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#cookies">O que é um cookie?</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tipo_cookies">Tipos de cookies</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#terceiro_cookies">Possíveis Cookies de terceiros</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="cookies" role="tabpanel">

                                        <p>Se quiser saber mais sobre o uso de cookies do site Studex Brasil (Regional Acre): https://brincosstudexacre.com.br, você está no lugar certo. Em seguida, vamos explicar o que são exatamente os cookies; quais os tipos de cookies utilizados e para que, e como você pode exercer o seu direito para configurar seu navegador e rejeitar o uso de qualquer uma delas.</p>
                                        <p>Isso sim, deves saber que, se você decidir não usar alguns cookies, este site pode não funcionar perfeitamente, afetando a sua experiência de usuário.</p>

                                        <p>O QUE É UM COOKIE?</p>

                                        <p>Um cookie é um arquivo de texto que é baixado em seu computador ao acessar determinadas páginas da web ou blogs.</p>
                                        <p>Os cookies permitem a essa página, entre outras coisas, armazenar e recuperar informação sobre os seus hábitos de navegação ou de sua equipe, e de acordo com a informação que contenha e do modo como você usa seu computador, podem ser utilizadas para lhe reconhecer.</p>
                                        <p>O navegador do usuário memoriza cookies no disco rígido, apenas durante a sessão atual, ocupando um espaço de memória mínima e não prejudicial ao computador. Os cookies não contêm nenhum tipo de informação pessoal específica, e a maioria das mesmas são apagadas do disco rígido ao encerrar a sessão do navegador (chamados cookies de sessão).</p>
                                        <p>A maioria dos navegadores aceita como padrão os cookies e, com a independência das mesmas, permitem ou impedem as definições de segurança cookies temporários ou memorizadas.</p>
                                        <p>Os cookies associam-se ao navegador, não a pessoa, por isso não costumam armazenar informações sensíveis sobre você como cartões de crédito ou dados bancários, fotografias, informações pessoais e etc...</p>
                                        <p>Os dados que guardam são de caráter técnico, estatísticos, preferências pessoais, personalização de conteúdo...</p>

                                        <p>ACEITAÇÃO DE COOKIES: REGULAMENTAÇÃO VIGENTE</p>

                                        <p>Ao acessar este site, e de acordo com a normativa vigente em matéria de proteção de dados, informamos o uso de cookies, dando-lhe a opção de aceitar expressamente e de ter acesso a mais informações através desta Política de Cookies.
                                        Você deve saber que, no caso de continuar navegando, você está dando seu consentimento para a utilização de cookies. Mas, em qualquer momento, pode mudar de opinião e bloquear a sua utilização através de seu navegador.</p>

                                        <p>Nome do cookie que será instalado: studex_acre_cookie_consent.</p>

                                        <p>Para sua total tranquilidade, este site está em conformidade com o estipulado na legislação em vigor em relação com o uso de cookies e de seus dados pessoais:</p>

                                        <li>
                                            <ul>Lei geral de proteção de dados pessoais (projeto de lei aprovado recentemente, baseado na lei Europeia)</ul>
                                            <ul>Lei Brasileira n.º 12.965, de 2014</ul>
                                            <ul>RGPD (Regulamento (UE) 2016/679 do Parlamento Europeu e do Conselho, de 27 de abril de 2016, relativo à proteção das pessoas físicas) que é a nova legislação da União Europeia que unifica a regulamentação do tratamento dos dados pessoais nos diferentes países da UE.</ul>
                                            <ul>Esta Política de Cookies pode ser alterada a qualquer momento para se adaptar às novidades legislativas ou mudanças em nossas atividades, sendo vigente a cada momento a política que está publicada no site. Em caso de alterações avisaremos no site.</ul>
                                        </li>

                                    </div>
                                    <div class="tab-pane" id="tipo_cookies" role="tabpanel">

                                        <p>TIPOS DE COOKIES</p>

                                        <p>Para oferecer uma melhor experiência do usuário, para obter dados analíticos, armazenar e recuperar informação acerca dos seus hábitos de navegação ou de sua equipe e desenvolver a sua atividade, o site Studex Brasil (Regional Acre), utiliza cookies próprias e de terceiros.</p>

                                        <p>Quais os tipos de cookies são utilizados neste site?</p>

                                        <p>🍪 Cookies técnicas</p>
                                        <p>São aquelas que permitem ao usuário a navegação através de uma página web, plataforma ou aplicação e a utilização das diferentes opções ou serviços que nela existam, como, por exemplo, controlar o trânsito e a comunicação de dados, identificar a sessão, acessar partes de acesso restrito, recordar os elementos que integram um pedido, realizar o processo de compra de um pedido, realizar o pedido de inscrição ou participação em um evento, usar itens de segurança durante a navegação, armazenar conteúdos para a difusão de vídeos ou som ou compartilhar conteúdo através das redes sociais.</p>

                                        <p>🍪 Cookies de personalização</p>
                                        <p>São aquelas que permitem ao utilizador acessar ao serviço, com algumas características de caráter geral predefinidas em função de uma série de critérios, em que o terminal do usuário como, por exemplo, seria o idioma, o tipo de navegador através do qual acessa o serviço, a localidade de onde acessa ao serviço, etc.</p>

                                        <p>🍪 Cookies de análise</p>
                                        <p>São aquelas que bem tratadas por nós ou por terceiros, nos permitem quantificar o número de usuários, e assim, realizar a medição e a análise estatística da utilização que os usuários fazem do serviço oferecido. Para isso ele analisa a sua navegação em nosso site, com o fim de melhorar a oferta de produtos ou serviços que lhe oferecemos.</p>

                                        <p>🍪 Cookies de publicidade</p>
                                        <p>São aquelas que bem tratadas por nós ou por terceiros, nos permitem gerir de forma mais eficaz possível a oferta de espaços publicitários na página web, adequando o conteúdo do anúncio ao conteúdo do serviço solicitado ou ao uso que se faça de nossa página web.
                                        Para isso podemos analisar seus hábitos de navegação na Internet e podemos mostrar publicidade relacionada com o seu perfil de navegação.</p>

                                        <p>🍪 Cookies de publicidade comportamental</p>
                                        São aquelas que permitem a gestão, da forma mais eficiente possível, dos espaços publicitários que no seu caso, o editor tenha incluído em uma página da web, aplicação ou plataforma a partir da qual presta o serviço solicitado.
                                        Estes cookies armazenam informação do comportamento dos usuários, obtidas através da observação contínua de seus hábitos de navegação, o que permite desenvolver um perfil específico para exibir publicidade em função do mesmo.</p>

                                        <p>Cookies de terceiros</p>
                                        <p>Este site, Studex Brasil (Regional Acre): https://brincosstudexacre.com.br, pode usar serviços de terceiros que, por conta do Google, recolhem informação para fins estatísticos, de uso do site por parte do usuário e para a prestação de outros serviços relacionados com a atividade do website e outros serviços de Internet.</p>

                                        <p>Este blog, como a maioria de sites e blogs, inclui funcionalidades que são fornecidas por terceiros.
                                        Tenha em conta que grande parte do meu trabalho consiste em testar as funcionalidades e recursos de terceiros (software de análise, plugins, ferramentas de SEO, etc.) para analisá-los e explicar-lhes em seguida as minhas descobertas e experiências (também para proporcionar uma melhor experiência de usuário), e isso pode ocasionalmente modificar a configuração de cookies e que não estejam detalhadas nesta política.
                                        É importante que você saiba que são cookies temporárias que nem sempre são possíveis de informar e que tem apenas fins de estudo, avaliação e proporcionar novas funcionalidades.
                                        Em nenhum caso serão utilizados cookies que comprometam a sua privacidade.</p>

                                        <p>Entre as cookies de terceiros mais estáveis que poderiam instalar navegando pelo site Studex Brasil (Regional Acre) são:</p>

                                        <li>
                                            <ul>🍪 Cookies De E-mail Marketing</ul>
                                            <ul>Utilizo MailChimp para serviço de e-mail marketing. MailChimp utiliza cookies próprias e de terceiros para poder identificar as páginas visitadas, inscritos, e proporcionar com totalidade seus serviços.
                                                MailChimp instala um fragmento de acompanhamento de JavaScript ("Snippet") na minha web. Este trecho permite que se definam cookies, pixels e outras tecnologias para facilitar o uso de certas automações, características e funcionalidades oferecidas por MailChimp como o envio de e-mails.
                                                Para saber mais sobre a política de cookies de MailChimp visite a seguinte url: https://mailchimp.com/legal/cookies/.</ul>
                                            <ul>🍪 Plataformas De Serviços De Afiliação</ul>
                                            <ul>Instalam cookies no seu navegador para acompanhar as vendas que são feitas no blog.
                                                Quer dizer, que se eu recomendo, por exemplo, um produto do Hotmart ou um serviço de hosting que sou afiliado e você acaba comprando/contratando, essas cookies serão utilizadas para informar ao fornecedor do produto que a venda foi feita através do meu link de afiliado.
                                                Desta forma poderei ganhar uma comissão pela venda. É dessa forma que obtenho renda com este site e pago minhas contas. Obrigado!</ul>
                                            <ul>🍪 Cookies De Redes Sociais</ul>
                                            <ul>Cookies de redes sociais podem ser armazenadas no seu navegador enquanto você navega pelo blog, por exemplo, quando você usa o botão de compartilhar um artigo do Studex Brasil (Regional Acre) em alguma rede social.</ul>
                                        </li>


                                        <p>As empresas que geram esses cookies correspondentes as redes sociais que utiliza este blog tem suas próprias políticas de cookies:</p>

                                        <li>
                                            <ul>Cookie do Twitter, segundo o disposto em sua Política de privacidade e de uso de cookies.</ul>
                                            <ul>Cookie do Google+, de acordo com o disposto em sua Política de cookies.</ul>
                                            <ul>Cookie de Pinterest, segundo o disposto em sua Política de privacidade e de utilização de cookies.</ul>
                                            <ul>Cookie do LinkedIn, segundo o disposto em sua Política de cookies.</ul>
                                            <ul>Cookie de Facebook, de acordo com o disposto em sua Política de cookies.</ul>
                                            <ul>Cookie do YouTube, de acordo com o disposto em sua Política de cookies.</ul>
                                            <ul>Cookie de Instagram, segundo o disposto em sua Política de cookies.</ul>
                                            <ul>Assim, as implicações de privacidade serão em função de cada rede social e dependem da configuração de privacidade que tenha nessa rede.</ul>
                                        </li>

                                        <p>Em nenhum caso, nem eu Solange Fernandez Vidal, como o responsável por este site, Studex Brasil (Regional Acre), nem os anunciantes, podemos obter informações de identificação pessoal de cookies.</p>

                                        <p>🍪 Cookies De Google Analytics</p>
                                        <p>Em particular, este site utiliza o Google Analytics, um serviço de análise web fornecido pela Google, Inc. com sede nos Estados Unidos, com sede em 1600 Amphitheatre Parkway, Mountain View, ca 94043.
                                        Para a prestação destes serviços, o Google utiliza cookies que coletam informações, incluindo o endereço IP do usuário, que será transmitida, tratada e armazenada pelo Google nos termos fixados na web Google.com. Isso inclui a possível transmissão de tais informações a terceiros por motivos de exigência legal ou quando estes terceiros processem a informação por conta do Google.</p>

                                        <p>Para consultar o tipo de cookie utilizado pelo Google, além do cookie do Google+ e Google Maps na url abaixo:</p>

                                        <p>https://policies.google.com/technologies/types?hl=pt-br</p>

                                        <p>Outras Especificações de cookies</p>

                                        <li>
                                            <ul>Área de membros</ul>
                                            <ul>Início de sessão: esses cookies são para iniciar sessão permitindo entrar e sair de sua conta em Studex Brasil (Regional Acre)</ul>
                                            <ul>Personalização: estes cookies me ajudam a lembrar com que pessoas ou sites você interagiu, para que possa mostrar conteúdo relacionado.</ul>
                                            <ul>Preferências: estes cookies me permitem recordar as suas definições e preferências, como o seu idioma preferido, e sua configuração de privacidade.</ul>
                                            <ul>Segurança: estes cookies são para evitar criar riscos de segurança. Principalmente para detectar quando alguém está tentando invadir sua conta em Studex Brasil (Regional Acre).</ul>
                                        </li>

                                    </div>
                                    <div class="tab-pane" id="terceiro_cookies" role="tabpanel">

                                        <p>Possíveis Cookies de terceiros</p>

                                        <p>O Google Tag Manager: o Google Tag Manager é um sistema de gestão de etiquetas que permite atualizar de forma rápida e fácil, as etiquetas e os fragmentos de código do site ou aplicativo móvel. Uma vez que o fragmento de Tag Manager foi adicionado ao site ou aplicativo móvel, você pode configurar as etiquetas através de uma interface de usuário baseada na Web, sem ter que mudar e implementar o código adicional. Deste modo, reduz-se o número de erros e evita-se recorrer a um programador ou desenvolvedor para fazer mudanças.</p>

                                        <p>GA Audiences: recolhem o comportamento dos usuários no site e os agrupa em audiências de remarketing com base nos critérios que definem. São utilizados principalmente em campanhas de remarketing do google Ads em redes de pesquisa e Display.</p>
                                        <p>O Google Ads Conversion: trata-se do pixel, que recolhe as conversões do Google Ads. </p>
                                        <p>O Google Ads User List: idem que GA Audiencies, mas permite criar listas de remarketing do google Ads diretamente. </p>
                                        <p>Google Dynamic Remarketing: é uma etiqueta que permite fazer remarketing para os visitantes de nosso site na rede de Display do Google com anúncios que contêm produtos que já vimos. </p>
                                        <p>Facebook Custom Audience: recolhem o comportamento dos usuários no site e os agrupa em audiências de remarketing com base nos critérios que definem. </p>
                                        <p>Facebook Pixel: Facebook Pixel é uma ferramenta de análise que nos ajuda a medir a eficácia da nossa publicidade, elaboração de relatórios das ações que as pessoas fazem quando visitam o site. </p>
                                        <p>Facebook Impression: permite rastrear as impressões em campanhas CPM. </p>
                                        <p>Facebook Connect: permite navegar na Internet com a sua identidade de usuário. Assim, um usuário pode comentar e compartilhar conteúdo de outros sites e toda esta atividade aparecerá no seu mural. No nosso site é feito através do botão de Compartilhar no fichas de produto e outras páginas da web. </p>

                                        <p>Cookies de WooCommerce. Necessárias para acompanhar os dados do carrinho, WooCommerce utiliza 3 cookies:</p>

                                        <p>woocommerce_cart_hash</p>
                                        <p>woocommerce_items_in_cart</p>
                                        <p>wp_woocommerce_session</p>

                                        <p>Os primeiros dois cookies contêm informações sobre o carrinho de compras na sua totalidade e ajudam a WooCommerce para saber quando alterar os dados do carrinho de compras.
                                        O cookie final (wp_woocommerce_session_) contém um código único para cada cliente, para que saiba onde encontrar os dados do carrinho no banco de dados para cada cliente. Não se armazena informação pessoal dentro de cookies.</p>

                                        <p>GERIR E RECUSAR O USO DE COOKIES</p>

                                        <p>Em qualquer momento, você pode adaptar a configuração do navegador para gerenciar, rejeitar o uso de cookies e ser notificado antes que sejam baixados.</p>
                                        <p>Você também pode adaptar a configuração do navegador para recusar todos os cookies, ou apenas os cookies de terceiros. E você também pode remover qualquer um dos cookies que já se encontram em seu computador.</p>
                                        <p>Para isso, você deve ter em mente que você terá que adaptar separadamente a configuração de cada navegador e computador que você use uma vez que, como já comentado anteriormente, as cookies se associam ao navegador, não a pessoa.</p>

                                        <p>Google Chrome https://support.google.com/chrome/answer/95647?hl=pt-BR</p>
                                        <p>Internet Explorer https://support.microsoft.com/pt-br/help/17442/windows-internet-explorer-delete-manage-cookies#ie=ie-10</p>
                                        <p>Mozilla Firefox https://support.mozilla.org/pt-BR/kb/ative-e-desative-os-cookies-que-os-sites-usam</p>
                                        <p>Apple Safari https://support.apple.com/pt-br/HT201265</p>

                                        <p>Mais informações sobre cookies</p>
                                        <p>Pode consultar o regulamento sobre o uso das cookies e obter mais informações: http://www.aboutcookies.org/.</p>
                                        <p>E se você deseja ter um maior controle sobre a instalação de cookies, você pode instalar programas ou plug-ins no navegador, conhecidos como ferramentas de "Do Not Track", que lhe permitem escolher os cookies que você deseja permitir.</p>
                                        <p>Espero que esta política de cookies tenha servido de ajuda, e se você tiver qualquer dúvida basta entrar em contato comigo.</p>

                                        <p>Se você tiver qualquer dúvida sobre esta Política de Cookies, você pode entrar em contato com Studex Brasil (Regional Acre): https://brincosstudexacre.com.br, enviando um e-mail para solange.fvidal@gmail.com.</p>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px; height: 200px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 72px;"></div></div></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    @if (Cookie::get('studex_acre_cookie_consent') == null)
                        <button type="button" class="btn btn-warning cookie-consent__modal">Aceitar</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection