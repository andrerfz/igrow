@extends('layouts.app')

@section('extra_css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('fix_head')

    <div class="kt-subheader kt-grid__item" id="kt_subheader">
        <div class="kt-container kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">Configurações</h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc">Usuários</span>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc">Colaborador</span>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc">{{$user->full_name}}</span>
                <!--
                <a href="javascript:" id="new_employee" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10"> Adicionar novo </a>
                <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
                    <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
                    <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span>
                        <i class="flaticon2-search-1"></i></span>
                    </span>
                </div>
                -->
            </div>
            <!--
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">-->

                    <!--
                    <a href="#" class="btn kt-subheader__btn-primary">
                        Actions &nbsp;
                    </a>
                    -->

                    <!--
                    <div class="dropdown dropdown-inline" data-toggle-="kt-tooltip" title="Quick actions" data-placement="left">
                        <a href="#" class="btn btn-icon"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon id="Shape" points="0 0 24 0 24 24 0 24"/>
                                    <path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" id="Combined-Shape" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                    <path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" id="Combined-Shape" fill="#000000"/>
                                </g>
                            </svg>
                             -->
                            <!--<i class="flaticon2-plus"></i>-->

                        <!--
                        </a>
                        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right"> -->
                            <!--begin::Nav-->
                            <!--
                            <ul class="kt-nav">
                               <li class="kt-nav__item">
                                   <a href="#" class="kt-nav__link"> <i class="kt-nav__link-icon flaticon2-plus"></i> <span class="kt-nav__link-text">Colaborador</span> </a>
                               </li>
                           </ul>
                           -->
                           <!--end::Nav-->
                        <!--
                        </div>
                    </div>
                </div>
            </div>
            -->
        </div>
    </div>

@endsection

@section('content')

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-user"></i>
                </span>
                <h3 class="kt-portlet__head-title">@if(Auth::user()->language->name == 'en') Search filter @elseif(Auth::user()->language->name == 'es') Filtros de busqueda @elseif(Auth::user()->language->name == 'pt-BR') Filtros de busca @endif</h3>
            </div>
            <!--
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <div class="dropdown dropdown-inline">
                            <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="la la-download"></i> Exportar
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__section kt-nav__section--first">
                                        <span class="kt-nav__section-text">Escolha uma opção</span>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-print"></i>
                                            <span class="kt-nav__link-text">Print</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-copy"></i>
                                            <span class="kt-nav__link-text">Copy</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                            <span class="kt-nav__link-text">Excel</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-text-o"></i>
                                            <span class="kt-nav__link-text">CSV</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                            <span class="kt-nav__link-text">PDF</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        &nbsp
                    </div>
                </div>
            </div>-->
        </div>
        <div class="kt-portlet__body">

            <div class="row">
                <div class="col">
                    <div class="alert alert-light alert-elevate fade show" role="alert" style="margin:0">
                        <div class="btn btn-outline-brand btn-square"><a style="padding-right: 0; cursor: pointer;" class="fa fa-exclamation-circle fa-lg kt-font-warning"></a></div>
                        <div class="btn btn-outline-brand btn-square"><a style="padding-right: 0; cursor: pointer;" class="fa fa-check-circle fa-lg kt-font-success"></a></div>
                        <div class="alert-text col col-sm-12 col-md-9 col-lg-9" style="padding-left: 10px;">
                            As permissões empregado terminadas pelo cargo. Os icones ao lado indicam que uma permissão foi alterada.
                            <br>
                            Essa configuração permanecerá mesmo que o empregado troque de cargo. Toque no botão ao lodo para voltar as configurações ao padrão.
                        </div>
                        <div class="btn btn-square"><a style="padding-right: 0; cursor: pointer;" class="reset_permissions fa fa-undo-alt fa-lg kt-font-danger"></a></div>
                        <div class="alert-text col col-sm-12 col-md-2 col-lg-2" style="padding-left: 10px;">
                            Toque para voltar as configurações ao padrão.
                        </div>
                    </div>
                </div>
            </div>

            <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>
            <table class="table table-striped- table-bordered table-hover table-checkable" id="users_permissions_table">
                <thead>
                <tr>
                    <th>Plataforma</th>
                    <th>Modulo</th>
                    <th>Ver</th>
                    <th>Criar</th>
                    <th>Atualizar</th>
                    <th>Deletar</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>

    <div id="modal_user_permissions" class="modal fade" data-callback="reload_user_permissions_table" tabindex="" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <form action="/{{$language}}/admin/users/ajax/deleteUserPermissions" class="form">
                <input type="hidden" name="user_id">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ModalLabel"> <p>@if(Auth::user()->language->name == 'en') Redefine permissions @elseif(Auth::user()->language->name == 'es') Redefinir permisiones @elseif(Auth::user()->language->name == 'pt-BR') Padronizar permissões? @endif</p></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">

                        <p>@if(Auth::user()->language->name == 'en') Do you want to continue? @elseif(Auth::user()->language->name == 'es') ¿Desea eliminar las permisiones? @elseif(Auth::user()->language->name == 'pt-BR') Deseja continuar? @endif</p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary send-form">Apagar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection


@section('extra_js')

    <!--begin::Page Vendors(used by this page) -->
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/demo1/pages/crud/datatables/advanced/column-rendering.js') }}" type="text/javascript"></script>
    <!--end::Page Scripts -->

    <script>
        $(document).ready(function () {

            /** Global variables **************************************************************************/

            let line_data_pickup = null, /*Variable to guard the dataTable line values*/
                validator = null, /*Variable to guard form validation rules*/
                language = '{{ $language }}', /*Variable to guard user language*/
                user_id = {!! $user->id !!}, /* Guard User ID*/
                /** User Table - Retrieve lines from table users to Datatable's. */
                ajax_user_permissions_table = {
                    url: "/" + language + "/admin/users/ajax/getUserPermissions",
                    type: "POST",
                    data: function (d) {
                        d.user_id = user_id;
                        d._token = window.Laravel.csrfToken;
                    }
                },
                user_permissions_table = $('#users_permissions_table').DataTable({
                    "language": {
                        "url": dataTableLanguage
                    },
                    ajax: ajax_user_permissions_table,
                    responsive: false,
                    pagingType: 'full_numbers',
                    paging: true,
                    columns: [
                        {
                            data: "platform",
                            sClass: "va-middle"
                        },
                        {
                            data: "module_name",
                            sClass: "va-middle"
                        },
                        {
                            data: "view",
                            sClass: "va-middle align-center"
                        },
                        {
                            data: "create",
                            sClass: "va-middle align-center"
                        },
                        {
                            data: "update",
                            sClass: "va-middle align-center"
                        },
                        {
                            data: "delete",
                            sClass: "va-middle align-center"
                        }
                    ],
                    columnDefs: [
                        {targets: -1, orderable: false, width: '1%'},
                        {targets: -2, orderable: false, width: '1%'},
                        {targets: -3, orderable: false, width: '1%'},
                        {targets: -4, orderable: false, width: '1%'},
                    ],
                    order: [0, 'asc'],
                    "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                    "pageLength": -1,

                }).on('click', 'td div a.link_permission', function () {
                    $.ajax({
                        type: 'POST',
                        url: "/" + language + '/admin/users/ajax/setUserPermission',
                        data: {
                            user_id: user_id,
                            plataform: $(this).data('platform'),
                            module: $(this).data('module'),
                            permission: $(this).data('permission'),
                            value: $(this).data('value'),
                            _token: window.Laravel.csrfToken
                        },
                        success: function (data) {
                            reload_user_permissions_table();
                            if (data.not) {
                                toastr[data.not.tipo](data.not.mensaje, data.not.titulo);
                            }
                        }
                    });
                });


            /** Events *****************************************************************************/


            $(document).on('click', 'a.reset_permissions', function () {
                $('#modal_user_permissions .send-form').removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                $('#modal_user_permissions input[name="user_id"]').val(user_id);
                $('#modal_user_permissions').modal({backdrop: 'static', keyboard: false, show: true});
            }).on('click', '.send-form', function (e) {
                e.preventDefault();
                let $btn = $(this), form = $(this).closest('form'), modal = $(this).closest('.modal');
                $btn.addClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                sendForm($btn, form, modal);
            });


            /** Functions *****************************************************************************/

            /**
             * Function to Send Data from form DataTable
             *
             */
            function sendForm($btn, form, modal) {
                let callback = modal.data('callback'), action = form.attr('action'), formdata = new FormData(form[0]);
                setTimeout(function () {
                    $.ajax({
                        type: "POST",
                        url: action,
                        data: formdata,
                        async: false,
                        success: function (data) {
                            if (data.status === 1 || data.status === 15) {
                                modal.modal('hide');
                            }
                            if (callback) {
                                eval(callback + '()');
                            }
                            if (data.not) {
                                toastr[data.not.tipo](data.not.mensaje, data.not.titulo);
                            }
                            $btn.removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                        },
                        error: function () {
                            $btn.removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                }, 1000);
            }

            function reload_user_permissions_table() {

                user_permissions_table.ajax.reload(null, false);
            }
        });
    </script>

@endsection