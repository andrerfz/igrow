@extends('layouts.app')

@section('extra_css')

    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>

    <style>

        .selection {
            width: 100%;
        }
        .select2-container {
            min-width: 150px;
            display: inline-flex;
            height: 32px !important;
            padding-top: 0;
            padding-bottom: 0;
            margin-top: 0.25rem;
            margin-bottom: 0.25rem;
        }

        .select2-selection {
            display: inline-flex;
            height: 32px !important;
            padding-top: 0;
            padding-bottom: 0;
            margin-bottom: 0.25rem;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 1.2 !important;
        }

    </style>
@endsection

@section('fix_head')

    <div class="kt-subheader kt-grid__item" id="kt_subheader">
        <div class="kt-container kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">@if(Auth::user()->language->name == 'en') Configurations @elseif(Auth::user()->language->name == 'es') Configuración @elseif(Auth::user()->language->name == 'pt-BR') Configurações @endif</h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc">@if(Auth::user()->language->name == 'en') Users @elseif(Auth::user()->language->name == 'es') Usuarios @elseif(Auth::user()->language->name == 'pt-BR') Usúarios @endif</span>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc">@if(Auth::user()->language->name == 'en') Role @elseif(Auth::user()->language->name == 'es') Role @elseif(Auth::user()->language->name == 'pt-BR') Cargos @endif</span>
                <a href="javascript:" id="new_employee" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10"> Adicionar novo </a>
                <!--
                <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
                    <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
                    <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span>
                        <i class="flaticon2-search-1"></i></span>
                    </span>
                </div>
                -->
            </div>
            <div class="kt-subheader__toolbar">
                <div style="padding-right: 15px;" data-toggle="kt-tooltip" data-placement="left" data-original-title="@if(Auth::user()->language->name == 'en') Select the company @elseif(Auth::user()->language->name == 'es') Seleccionar la empresa @elseif(Auth::user()->language->name == 'pt-BR') Selecionar a empresa @endif">
                    <select class="btn form-control kt-input select2" id="company_filter" style="width: 100%;">
                        <option value="0">@if(Auth::user()->language->name == 'en') All companys @elseif(Auth::user()->language->name == 'es') Todas empresas @elseif(Auth::user()->language->name == 'pt-BR') Todas empresas @endif</option>
                        @foreach($companys as $company)
                            <option value="{{$company->id}}" {{Session::get('company_filter') == $company->id ? 'selected' : ''}}> {{$company->trading_name && $company->business_name ? $company->trading_name .' ('. $company->business_name . ')' : ($company->trading_name ?: $company->business_name)}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <!--
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">-->

                    <!--
                    <a href="#" class="btn kt-subheader__btn-primary">
                        Actions &nbsp;
                    </a>
                    -->

                    <!--
                    <div class="dropdown dropdown-inline" data-toggle-="kt-tooltip" title="Quick actions" data-placement="left">
                        <a href="#" class="btn btn-icon"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon id="Shape" points="0 0 24 0 24 24 0 24"/>
                                    <path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" id="Combined-Shape" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                    <path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" id="Combined-Shape" fill="#000000"/>
                                </g>
                            </svg>
                             -->
                            <!--<i class="flaticon2-plus"></i>-->

                        <!--
                        </a>
                        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right"> -->
                            <!--begin::Nav-->
                            <!--
                            <ul class="kt-nav">
                               <li class="kt-nav__item">
                                   <a href="#" class="kt-nav__link"> <i class="kt-nav__link-icon flaticon2-plus"></i> <span class="kt-nav__link-text">Colaborador</span> </a>
                               </li>
                           </ul>
                           -->
                           <!--end::Nav-->
                        <!--
                        </div>
                    </div>
                </div>
            </div>
            -->
        </div>
    </div>

@endsection

@section('content')

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-user"></i>
                </span>
                <h3 class="kt-portlet__head-title">@if(Auth::user()->language->name == 'en') Search filter @elseif(Auth::user()->language->name == 'es') Filtros de busqueda @elseif(Auth::user()->language->name == 'pt-BR') Filtros de busca @endif</h3>
            </div>
            <!--
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <div class="dropdown dropdown-inline">
                            <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="la la-download"></i> Exportar
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__section kt-nav__section--first">
                                        <span class="kt-nav__section-text">Escolha uma opção</span>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-print"></i>
                                            <span class="kt-nav__link-text">Print</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-copy"></i>
                                            <span class="kt-nav__link-text">Copy</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                            <span class="kt-nav__link-text">Excel</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-text-o"></i>
                                            <span class="kt-nav__link-text">CSV</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                            <span class="kt-nav__link-text">PDF</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        &nbsp
                    </div>
                </div>
            </div>-->
        </div>
        <div class="kt-portlet__body">

            <div class="row">
                <div class="col-lg-3 col-md-3 kt-margin-b-10-tablet-and-mobile">
                    <label>Permissões:</label>
                    <select class="form-control kt-input select2" id="permission_filter">
                        <option value="0">Todos</option>
                        @foreach($permissions as $permission)
                            <option value="{{$permission->id}}"> {{$permission->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-2 col-md-2 kt-margin-b-10-tablet-and-mobile">
                    <label>Estado:</label>
                    <select class="form-control kt-input select2" id="state_filter">
                        <option value="0">Todos</option>
                        <option value="1">Ativos</option>
                        <option value="2">Inativos</option>
                    </select>
                </div>
                <div class="col-lg-1 col-md-1 kt-margin-b-10-tablet-and-mobile">
                    <label>Apagados:</label>
                    <div class="kt-radio-inline">
                        <span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--warning">
                            <label>
                                <input type="checkbox" id="recycle_filter">
                                <span></span>
                            </label>
                        </span>
                    </div>
                </div>
            </div>

            <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>
            <table class="table table-striped- table-bordered table-hover table-checkable" id="roles_table">
                <thead>
                <tr>
                    <th>Nome</th>
                    <th>Permissão Extendida</th>
                    <th>Ver</th>
                    <th>Criar</th>
                    <th>Atualizar</th>
                    <th>Deletar</th>
                    <th nowrap>Usuários</th>
                    <th style="text-align: center">Estado</th>
                    <th style="text-align: center">Ações</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>

    <div id="modal_new_role" class="modal fade" data-callback="reload_roles_table" tabindex="" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form action="/{{$language}}/admin/users/ajax/setRole" class="form" id="form_new_role">
                <input type="hidden" name="id">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ModalLabel"></h5>
                        <button type="reset" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="kt-portlet__body">
                            <div class="form-group row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <label class="form-control-label">* Permissão:</label>
                                    <div class="input-group">
                                        <select class="form-control kt-input select2" name="permission" style="width: 100%">
                                            <option value="0">Todos</option>
                                            @foreach($permissions as $permission_)
                                                <option value="{{$permission_->id}}"> {{$permission_->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <label class="form-control-label">* Nome:</label>
                                    <input type="text" name="name" class="form-control" placeholder="Nome do cargo">
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary send-form-modal_new_role">Salvar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="modal_lock" class="modal fade" data-callback="reload_roles_table" tabindex="" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <form action="/{{$language}}/admin/users/role/ajax/toggleActive" class="form">
                <input type="hidden" name="id">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ModalLabel"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary send-form">Enviar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="modal_delete" class="modal fade" data-callback="" tabindex="" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <form action="" class="form">
                <input type="hidden" name="id">
                <input type="hidden" name="trashed">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ModalLabel"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary send-form">Apagar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection


@section('extra_js')

    <!--begin::Page Vendors(used by this page) -->
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/demo1/pages/crud/datatables/advanced/column-rendering.js') }}" type="text/javascript"></script>
    <!--end::Page Scripts -->

    <script>
        $(document).ready(function () {

            /** Global variables **************************************************************************/

            let line_data_pickup = null, /*Variable to guard the dataTable line values*/
                validator = null, /*Variable to guard form validation rules*/
                language = '{{ $language }}', /*Variable to guard user language*/
                /** Roles Table - Retrieve lines from table users to Datatable's. */
                ajax_table_roles_table = {
                    url: "/" + language + "/admin/users/ajax/getRoles",
                    type: "POST",
                    data: function (d) {
                        d.state_filter = $('#state_filter').val();
                        d.permission_filter = $('#permission_filter').val();
                        d.recycle_filter = $('#recycle_filter').is(':checked');
                        d._token = window.Laravel.csrfToken;
                    }
                },
                roles_table = $('#roles_table').DataTable({
                    "language": {
                        "url": dataTableLanguage
                    },
                    ajax: ajax_table_roles_table,
                    responsive: false,
                    pagingType: 'full_numbers',
                    paging: true,
                    columns: [
                        {
                            data: "name",
                            sClass: "va-middle"
                        },
                        {
                            data: "permission_name",
                            sClass: "va-middle"
                        },
                        {
                            data: "permission_view",
                            sClass: "va-middle align-center"
                        },
                        {
                            data: "permission_create",
                            sClass: "va-middle align-center"
                        },
                        {
                            data: "permission_update",
                            sClass: "va-middle align-center"
                        },
                        {
                            data: "permission_delete",
                            sClass: "va-middle align-center"
                        },
                        {
                            data: "users_count",
                            sClass: "va-middle align-center"
                        },
                        {
                            data: "active",
                            sClass: "va-middle align-center"
                        },
                        {
                            data: "actions",
                            sClass: "va-middle align-center"
                        },
                    ],
                    columnDefs: [
                        {targets: -1, orderable: false, width: '5%'},
                        {targets: -2, orderable: true, width: '1%'},
                        {targets: -3, orderable: false, width: '1%'},
                        {targets: -4, orderable: false, width: '1%'},
                        {targets: -5, orderable: false, width: '1%'},
                        {targets: -6, orderable: false, width: '1%'},
                        {targets: -7, orderable: false, width: '1%'},
                    ],
                    order: [0, 'asc'],

                }).on('click', 'span.linkActive', function () {
                    let tr = $(this).parent().parent(), id = roles_table.row(tr).data().id;
                    $.ajax({
                        type: 'POST',
                        url: "/" + language + '/admin/users/role/ajax/toggleActive',
                        data: {
                            id: id,
                            _token: window.Laravel.csrfToken
                        },
                        success: function (data) {
                            reload_roles_table();
                            if (data.not) {
                                toastr[data.not.tipo](data.not.mensaje, data.not.titulo);
                            }
                        }
                    });

                }).on('click', 'span.dropdown', function () {
                    let tr = $(this).parent().parent().parent();
                    line_data_pickup = roles_table.row(tr).data();
                }).on('click', 'td div a', function () {
                    let tr = $(this).parent().parent().parent();
                    line_data_pickup = roles_table.row(tr).data();
                }).on('click', 'td.insertFile', function () {
                    let tr = $(this).parent(), data = roles_table.row(tr).data();
                    if (data.id != undefined) {
                        $('#modal_new_picture input[name="id"]').val(data.id);
                        $('#modal_new_picture input[type="file"]').val('');
                        $('#modal_new_picture').modal({
                            backdrop: 'static',
                            keyboard: false,
                            show: true
                        }).on('hidden.bs.modal', function () {
                            reload_roles_table();
                        });
                    }
                });


            /** Events *****************************************************************************/


            $(document).on('select2:select', '#permission_filter', function () {
                reload_roles_table();
            }).on('select2:select', '#state_filter', function () {
                reload_roles_table();
            }).on('change', '#recycle_filter', function () {
                reload_roles_table();
            }).on('click', '#new_employee', function () {
                validation('roles', true);
                validator.resetForm();
                $('#modal_new_role #mail').show();
                $('#modal_new_role .send-form-modal_new_role').removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                $('#modal_new_role .modal-title').text('Novo cargo');
                $('#modal_new_role input[name="id"]').val('');
                $('#modal_new_role input[type="text"]').val('');
                $('#modal_new_role select').val(0).trigger('change');
                $('#modal_new_role').modal({
                    backdrop: 'static',
                    keyboard: false,
                    show: true
                }).on('hidden.bs.modal', function () {
                    reload_roles_table();
                });
            }).on('click', '.send-form-modal_new_role', function (e) {
                e.preventDefault();
                let $btn = $(this), form = $(this).closest('form'), modal = $(this).closest('.modal');
                $btn.addClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                if (form.valid()) {
                    sendForm($btn, form, modal);
                } else {
                    $btn.removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                }
            }).on('click', '.send-form', function (e) {
                e.preventDefault();
                let $btn = $(this), form = $(this).closest('form'), modal = $(this).closest('.modal');
                $btn.addClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                if (form.valid()) {
                    sendForm($btn, form, modal);
                } else {
                    $btn.removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                }
            }).on('click', 'a.role_options', function () {
                let action = $(this).data('action');
                switch (action) {

                    case 'see':
                        op_see(line_data_pickup);
                        break;

                    case 'lock':
                        op_lock(line_data_pickup);
                        break;

                    case 'edit':
                        op_edit(line_data_pickup);
                        break;

                    case 'softdelete':
                        op_delete(line_data_pickup, 'Eliminar cargo', '<p>Deseja continuar?</p>', 'softdelete', "/" + language + '/admin/users/ajax/deleteRole', 'reload_roles_table');
                        break;

                    case 'recycle':
                        op_delete(line_data_pickup, 'Recuperar cargo', '<p>Deseja continuar?</p>', 'recycle', "/" + language + '/admin/users/ajax/deleteRole', 'reload_roles_table');
                        break;

                    default:
                        return false;
                }

            });


            /** Functions *****************************************************************************/


            /**
             * Function to Send Data from form DataTable
             *
             */
            function sendForm($btn, form, modal) {
                let callback = modal.data('callback'), action = form.attr('action'), formdata = new FormData(form[0]);
                setTimeout(function () {

                    $.ajax({
                        type: "POST",
                        url: action,
                        data: formdata,
                        async: false,
                        success: function (data) {

                            if (data.status === 1 || data.status === 15) {
                                modal.modal('hide');
                            }

                            if (callback) {
                                eval(callback + '()');
                            }

                            if (data.not) {
                                toastr[data.not.tipo](data.not.mensaje, data.not.titulo);
                            }

                            $btn.removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                        },
                        error: function () {

                            $btn.removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });

                }, 1000);
            }

            /**
             * Function to Reload DataTable
             *
             */
            function reload_roles_table() {
                roles_table.ajax.reload(null, false);
            }

            function op_see(data) {
                //window.location.href = data.url;
                window.open(data.url, '_self');
            }

            function op_lock(data) {
                $('#modal_lock input[name="id"]').val(data.id);
                if (data.active_o) {
                    $('#modal_lock .modal-title').text('Desativar cargo');
                    $('#modal_lock .modal-body').html('<p>Se desativar o cargo, todos os usuarios associados não poderão utilizar a aplicação. Deseja continuar?</p>');
                } else {
                    $('#modal_lock .modal-title').text('Ativar cargo');
                    $('#modal_lock .modal-body').html('<p>Se ativar o cargo, todos os usuarios associados poderão utilizar a aplicação. Deseja continuar?</p>');
                }
                $('#modal_lock').modal('show');
            }

            function op_edit(data) {
                validation('roles', false);
                validator.resetForm();
                $('#modal_new_role #mail').hide();
                $('#modal_new_role .modal-title').text('Editar cargo');
                $('#modal_new_role input[name="id"]').val(data.id);
                $('#modal_new_role input[name="name"]').val(data.name);
                $('#modal_new_role select[name="permission"]').val(data.permission_id).trigger('change');
                $('#modal_new_role .send-form-modal_new_role').removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                $('#modal_new_role').modal({
                    backdrop: 'static',
                    keyboard: false,
                    show: true
                }).on('hidden.bs.modal', function () {
                    reload_roles_table();
                });
            }

            function op_delete(data, title, mensage, options, action, callback) {
                $('#modal_delete form').attr('action', action);
                $('#modal_delete input[name="id"]').val(data.id);
                $('#modal_delete input[name="trashed"]').val(options);
                $('#modal_delete .modal-title').text(title);
                $('#modal_delete .modal-body').html(mensage);
                $('#modal_delete').data('callback', callback).modal('show');
            }

            function validation(form_name, rule) {
                if (form_name === 'roles') {
                    validator = $("#form_new_role").validate({
                        // define validation rules
                        rules: {
                            name: {
                                required: true,
                                minlength: 1
                            },
                            permission: {
                                required: true,
                                notEqualTo: 0
                            },
                        },
                        //display error alert on form submit
                        invalidHandler: function (event) {
                            swal.fire({
                                "title": "",
                                "html": "Existem alguns erros no formulário. <br />Por favor corrigi-los.",
                                "type": "error",
                                "confirmButtonClass": "btn btn-brand",
                                "onClose": function (e) {
                                    //console.log('on close event fired!');
                                }
                            });
                            event.preventDefault();
                        },
                        submitHandler: function () {
                            return true;
                        }
                    });
                }
            }
        });
    </script>

@endsection