@extends('layouts.app')

@section('extra_css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/demo1/pages/wizard/wizard-4.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('fix_head')

    <div class="kt-subheader kt-grid__item" id="kt_subheader">
        <div class="kt-container kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">@if($en) Configurations @elseif($es) Configuración @elseif($ptBR) Configurações @endif</h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc">@if($en) Company Profile @elseif($es) Perfil de la empresa @elseif($ptBR) Perfil da empresa @endif</span>
                <a href="javascript:" id="new_company" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10"> @if($en) New branch @elseif($es) Adicionar sucursal @elseif($ptBR) Adicionar filial @endif </a>
                <!--
                <a href="javascript:" id="new_company" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10"> Adicionar novo </a>
                <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
                    <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
                    <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span>
                        <i class="flaticon2-search-1"></i></span>
                    </span>
                </div>
                -->
            </div>
            <div class="kt-subheader__toolbar">
                <div class="btn-group">
                    <button type="button" class="btn btn-brand btn-bold" name="save_profile">Save Changes </button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('content')

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-toolbar">
                    <ul class="nav nav-tabs nav-tabs-space-xl nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#company_1" role="tab">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon id="Bound" points="0 0 24 0 24 24 0 24" />
                                        <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" id="Shape" fill="#000000" fill-rule="nonzero" />
                                        <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" id="Path" fill="#000000" opacity="0.3" />
                                    </g>
                                </svg>
                                @if($en) Branchs @elseif($es) Sucursales @elseif($ptBR) Filiais @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#company_2" role="tab">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon id="Bound" points="0 0 24 0 24 24 0 24" />
                                        <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" id="Shape" fill="#000000" fill-rule="nonzero" />
                                        <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" id="Path" fill="#000000" opacity="0.3" />
                                    </g>
                                </svg>
                                @if($en) General Information @elseif($es) Información general @elseif($ptBR) Informações gerais @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#company_3" role="tab">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon id="Bound" points="0 0 24 0 24 24 0 24" />
                                        <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" id="Shape" fill="#000000" fill-rule="nonzero" />
                                        <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" id="Path" fill="#000000" opacity="0.3" />
                                    </g>
                                </svg>
                                @if($en) Tax data @elseif($es) Datos fiscales @elseif($ptBR) Dados fiscais @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#company_4" role="tab">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect id="bound" x="0" y="0" width="24" height="24" />
                                        <path d="M6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,12 C19,12.5522847 18.5522847,13 18,13 L6,13 C5.44771525,13 5,12.5522847 5,12 L5,3 C5,2.44771525 5.44771525,2 6,2 Z M7.5,5 C7.22385763,5 7,5.22385763 7,5.5 C7,5.77614237 7.22385763,6 7.5,6 L13.5,6 C13.7761424,6 14,5.77614237 14,5.5 C14,5.22385763 13.7761424,5 13.5,5 L7.5,5 Z M7.5,7 C7.22385763,7 7,7.22385763 7,7.5 C7,7.77614237 7.22385763,8 7.5,8 L10.5,8 C10.7761424,8 11,7.77614237 11,7.5 C11,7.22385763 10.7761424,7 10.5,7 L7.5,7 Z" id="Combined-Shape" fill="#000000" opacity="0.3" />
                                        <path d="M3.79274528,6.57253826 L12,12.5 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 Z" id="Combined-Shape" fill="#000000" />
                                    </g>
                                </svg>
                                @if($en) Notifications @elseif($es) Notificaciones @elseif($ptBR) Notificações @endif
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="kt-portlet__body">
                <form id="company_profile" action="/{{$language}}/admin/company/ajax/setCompanyProfile">
                    {{ csrf_field() }}
                    <div class="tab-content">
                        <div class="tab-pane active" id="company_1" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                                    <label>Estado:</label>
                                    <select class="form-control kt-input select2" id="state_filter">
                                        <option value="0">Todos</option>
                                        <option value="1">Ativos</option>
                                        <option value="2">Inativos</option>
                                    </select>
                                </div>
                                <div class="col-lg-1 col-md-1 kt-margin-b-10-tablet-and-mobile">
                                    <label>Apagados:</label>
                                    <div class="kt-radio-inline">
                                        <span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--warning">
                                            <label>
                                                <input type="checkbox" id="recycle_filter">
                                                <span></span>
                                            </label>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="campany_table">
                                <thead>
                                <tr>
                                    <th>@if($en) Picture @elseif($es) Foto @elseif($ptBR) Foto @endif</th>
                                    <th>@if($en) Name @elseif($es) Nombre @elseif($ptBR) Nome @endif</th>
                                    <th>Email</th>
                                    <th nowrap>@if($en) Mobile @elseif($es) Tel. Móvil @elseif($ptBR) Tel. Móvel @endif</th>
                                    <th nowrap>@if($en) Tax number @elseif($es) NIE @elseif($ptBR) CNPJ @endif</th>
                                    <th>@if($en) Employees @elseif($es) Empleados @elseif($ptBR) Colaboradores @endif</th>
                                    <th>@if($en) Branchs @elseif($es) Sucursales @elseif($ptBR) Filiais @endif</th>
                                    <th>@if($en) Estate @elseif($es) Estado @elseif($ptBR) Estado @endif</th>
                                    <th>@if($en) Actions @elseif($es) Acciones @elseif($ptBR) Ações @endif</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="tab-pane" id="company_2" role="tabpanel">
                            <div class="row">
                                <div class="col-sm-12 col-md-4 col-lg-4">
                                    <div class="kt-portlet kt-portlet--height-fluid-">

                                        <div class="kt-portlet__head  kt-portlet__head--noborder">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">@if($en) Resumen: @elseif($es) Resumen: @elseif($ptBR) Resumo: @endif</h3>
                                            </div>
                                            <div class="kt-portlet__head-toolbar">

                                            </div>
                                        </div>
                                        <div class="kt-portlet__body kt-portlet__body--fit-y">

                                            <div class="kt-widget kt-widget--user-profile-1">
                                                <div class="kt-widget__body">
                                                    <div class="kt-widget__content">
                                                        <div class="kt-widget__info">
                                                            <span class="kt-widget__label">Email:</span>
                                                            <span class="kt-widget__data">{{$company->email}}</span>
                                                        </div>
                                                        <div class="kt-widget__info">
                                                            <span class="kt-widget__label">@if($en) Phone: @elseif($es) Fijo: @elseif($ptBR) Fixo: @endif</span>
                                                            <span class="kt-widget__data">{{$company->phone}}</span>
                                                        </div>
                                                        <div class="kt-widget__info">
                                                            <span class="kt-widget__label">@if($en) Mobile: @elseif($es) Móvil: @elseif($ptBR) Móvel: @endif</span>
                                                            <span class="kt-widget__data">{{$company->mobile}}</span>
                                                        </div>
                                                        <div class="kt-widget__info">
                                                            <span class="kt-widget__label">Web:</span>
                                                            <span class="kt-widget__data">{{$company->web}}</span>
                                                        </div>
                                                        <div class="kt-widget__info">
                                                            <span class="kt-widget__label">@if($en) Direction: @elseif($es) Dirección: @elseif($ptBR) Endereço: @endif</span>
                                                            <span class="kt-widget__data">{{$company->direction}}</span>
                                                        </div>
                                                        <div class="kt-widget__info">
                                                            <span class="kt-widget__label">@if($en) Town: @elseif($es) Municipio: @elseif($ptBR) Município: @endif</span>
                                                            <span class="kt-widget__data">{{$company->town}}</span>
                                                        </div>
                                                        @if($es)
                                                        <div class="kt-widget__info">
                                                            <span class="kt-widget__label">Province: </span>
                                                            <span class="kt-widget__data">{{$company->province}}</span>
                                                        </div>
                                                        @endif
                                                        <div class="kt-widget__info">
                                                            <span class="kt-widget__label">@if($en) State: @elseif($es) Estado: @elseif($ptBR) Estado: @endif</span>
                                                            <span class="kt-widget__data">{{$company->state}}</span>
                                                        </div>
                                                        <div class="kt-widget__info">
                                                            <span class="kt-widget__label">@if($en) Country: @elseif($es) Pais: @elseif($ptBR) Pais: @endif</span>
                                                            <span class="kt-widget__data">{{$company->country}}</span>
                                                        </div>
                                                    </div>
                                                    <div class="kt-widget__items">

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="kt-form kt-form--label-right col-sm-12 col-md-8 col-lg-8">
                                    <div class="kt-form__body">
                                        <div class="kt-portlet__body kt-portlet__body--fit-y">

                                            <div class="kt-section kt-section--first">
                                                <div class="kt-section__body">
                                                    <div class="row">
                                                        <label class="col-xl-3"></label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <h3 class="kt-section__title kt-section__title-sm">@if($en) Company Info: @elseif($es) Informaciones Generales: @elseif($ptBR) Infomações Gerais: @endif</h3>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">@if($en) Company logo @elseif($es) Logo de la empresa @elseif($ptBR) Logo da empresa @endif</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="kt-avatar kt-avatar--outline kt-avatar--square" id="kt_apps_user_add_avatar">
                                                                <div class="kt-avatar__holder" style="background-image: url({{$company->logo_url}}); width: 320px !important;"></div>
                                                                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="@if($en) Change logo @elseif($es) Cambiar logo @elseif($ptBR) Trocar logo @endif">
                                                                    <i class="fa fa-pen"></i>
                                                                    <input type="file" name="file" accept=".png, .jpg, .jpeg">
                                                                </label>
                                                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                                                    <i class="fa fa-times"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">@if($en) Email Address @elseif($es) Email de la empresa @elseif($ptBR) Email da empresa @endif</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-at"></i></span></div>
                                                                <input type="text" class="form-control" name="email" value="{{$company->email}}" placeholder="Email" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">@if($en) Phone number @elseif($es) Número fijo @elseif($ptBR) Número fixo @endif</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                                                <input type="text" class="form-control" name="phone" value="{{$company->phone}}" placeholder="@if($en) Phone number @elseif($es) Número fijo @elseif($ptBR) Número fixo @endif">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">@if($en) Mobile number @elseif($es) Números móvil @elseif($ptBR) Número móvel @endif</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                                                <input type="text" class="form-control" name="mobile" value="{{$company->mobile}}" placeholder="@if($en) Mobile phone @elseif($es) Números móvil @elseif($ptBR) Número móvel @endif">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">@if($en) Company Site @elseif($es) Sitio de la empresa @elseif($ptBR) Site da empresa @endif</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="web" value="{{$company->web}}" placeholder="@if($en) Company Site @elseif($es) Sitio de la empresa @elseif($ptBR) Site da empresa @endif">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">@if($en) Town: @elseif($es) Municipio: @elseif($ptBR) Município: @endif</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <input type="text" class="form-control" name="town" value="{{$company->town}}" placeholder="@if($en) Town @elseif($es) Municipio @elseif($ptBR) Município @endif">
                                                        </div>
                                                    </div>
                                                    @if($es)
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Province: </label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <input type="text" class="form-control" name="province" value="{{$company->province}}" placeholder="@if($en) Province @elseif($es) Province @elseif($ptBR) Provincia @endif">
                                                        </div>
                                                    </div>
                                                    @endif
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">@if($en) State: @elseif($es) Estado: @elseif($ptBR) Estado: @endif</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <input type="text" class="form-control" name="state" value="{{$company->state}}" placeholder="@if($en) State @elseif($es) Estado @elseif($ptBR) Estado @endif">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">@if($en) Country: @elseif($es) Pais: @elseif($ptBR) Pais: @endif</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <input type="text" class="form-control" name="country" value="{{$company->country}}" placeholder="@if($en) Country @elseif($es) Pais @elseif($ptBR) Pais @endif">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="company_3" role="tabpanel">
                            <div class="row">
                                <div class="col-sm-12 col-md-4 col-lg-4">
                                    <div class="kt-portlet kt-portlet--height-fluid-">

                                        <div class="kt-portlet__head  kt-portlet__head--noborder">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">@if($en) Resumen: @elseif($es) Resumen: @elseif($ptBR) Resumo: @endif</h3>
                                            </div>
                                            <div class="kt-portlet__head-toolbar">

                                            </div>
                                        </div>
                                        <div class="kt-portlet__body kt-portlet__body--fit-y">

                                            <div class="kt-widget kt-widget--user-profile-1">
                                                <div class="kt-widget__body">
                                                    <div class="kt-widget__content">
                                                        <div class="kt-widget__info">
                                                            <span class="kt-widget__label">@if($en) Company name @elseif($es) Nombre comercial @elseif($ptBR) Nome fantasia @endif</span>
                                                            <span class="kt-widget__data">{{$company->business_name}}</span>
                                                        </div>
                                                        <div class="kt-widget__info">
                                                            <span class="kt-widget__label">@if($en) Trading name @elseif($es) Razón social @elseif($ptBR) Razão social @endif</span>
                                                            <span class="kt-widget__data">{{$company->trading_name}}</span>
                                                        </div>
                                                        <div class="kt-widget__info">
                                                            <span class="kt-widget__label">@if($en) Federal Tax Number: @elseif($es) NIF: @elseif($ptBR) CNPJ: @endif</span>
                                                            <span class="kt-widget__data">{{$company->federal_tax_number}}</span>
                                                        </div>
                                                        @if($ptBR)
                                                        <div class="kt-widget__info">
                                                            <span class="kt-widget__label">Inscrição Estadual:</span>
                                                            <span class="kt-widget__data">{{$company->state_tax_number}}</span>
                                                        </div>
                                                        @endif
                                                        <div class="kt-widget__info">
                                                            <span class="kt-widget__label">@if($en) Retail: @elseif($es) Móvil: @elseif($ptBR) Varejo: @endif</span>
                                                            <span class="kt-widget__data">@if($en && $company->retail) Yes @elseif($en && !$company->retail) No @elseif($es && $company->retail) Yes @elseif($es && !$company->retail) No @elseif($ptBR && $company->retail) Sim @elseif($ptBR && !$company->retail) Não @endif</span>
                                                        </div>
                                                        <div class="kt-widget__info">
                                                            <span class="kt-widget__label">@if($en) Wholesale: @elseif($es) Móvil: @elseif($ptBR) Atacado: @endif</span>
                                                            <span class="kt-widget__data">@if($en && $company->wholesale) Yes @elseif($en && !$company->wholesale) No @elseif($es && $company->wholesale) Yes @elseif($es && !$company->wholesale) No @elseif($ptBR && $company->wholesale) Sim @elseif($ptBR && !$company->wholesale) Não @endif</span>
                                                        </div>
                                                    </div>
                                                    <div class="kt-widget__items">

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="kt-form kt-form--label-right col-sm-12 col-md-8 col-lg-8">
                                    <div class="kt-form__body">
                                        <div class="kt-portlet__body kt-portlet__body--fit-y">

                                            <div class="kt-section kt-section--first">
                                                <div class="kt-section__body">
                                                    <div class="row">
                                                        <label class="col-xl-3"></label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <h3 class="kt-section__title kt-section__title-sm">@if($en) Company Info: @elseif($es) Informaciones Generales: @elseif($ptBR) Infomações Gerais: @endif</h3>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">@if($en) Company name @elseif($es) Nombre comercial @elseif($ptBR) Nome fantasia @endif</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <input class="form-control" type="text" name="business_name" value="{{$company->business_name}}" placeholder="@if($en) Company name @elseif($es) Nombre de la empresa @elseif($ptBR) Nome fantasia @endif">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">@if($en) Trading name @elseif($es) Razón social @elseif($ptBR) Razão social @endif</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <input class="form-control" type="text" name="trading_name" value="{{$company->trading_name}}" placeholder="@if($en) Trading name @elseif($es) Razón social @elseif($ptBR) Razão social @endif">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">@if($en) Federal Tax Number: @elseif($es) NIF: @elseif($ptBR) CNPJ: @endif</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <input class="form-control" type="text" name="federal_tax_number" value="{{$company->federal_tax_number}}"  placeholder="@if($en) Federal Tax Number: @elseif($es) NIF: @elseif($ptBR) CNPJ: @endif">
                                                        </div>
                                                    </div>
                                                    @if($ptBR)
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Inscrição Estadual:</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <input class="form-control" type="text" name="state_tax_number" value="{{$company->state_tax_number}}"  placeholder="Inscrição Estadual">
                                                        </div>
                                                    </div>
                                                    @endif
                                                    <div class="row">
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">@if($en) Activity Related: @elseif($es) Sector comercial: @elseif($ptBR) Setor comercial: @endif</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="kt-checkbox-list">
                                                                <label class="kt-checkbox">
                                                                    <input type="checkbox" name="retail" @if($company->retail) checked @endif> @if($en) Retail: @elseif($es) Retail: @elseif($ptBR) Varejo: @endif
                                                                    <span></span>
                                                                </label>
                                                                <label class="kt-checkbox">
                                                                    <input type="checkbox" name="wholesale" @if($company->wholesale) checked @endif> @if($en) Wholesale: @elseif($es) Wholesale: @elseif($ptBR) Atacado: @endif
                                                                    <span></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="company_4" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <div class="row">
                                                <label class="col-xl-3"></label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <h3 class="kt-section__title kt-section__title-sm">Setup Email Notification:</h3>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-sm row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Email Notification</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <span class="kt-switch">
                                                        <label>
                                                            <input type="checkbox" checked="checked" name="">
                                                            <span></span>
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-last row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Send Copy To Personal Email</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <span class="kt-switch">
                                                        <label>
                                                            <input type="checkbox" name="">
                                                            <span></span>
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-separator kt-separator--border-dashed kt-separator--portlet-fit kt-separator--space-lg"></div>
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <div class="row">
                                                <label class="col-xl-3"></label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <h3 class="kt-section__title kt-section__title-sm">Activity Related Emails:</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">When To Email</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="kt-checkbox-list">
                                                        <label class="kt-checkbox">
                                                            <input type="checkbox"> You have new notifications.
                                                            <span></span>
                                                        </label>
                                                        <label class="kt-checkbox">
                                                            <input type="checkbox"> You're sent a direct message
                                                            <span></span>
                                                        </label>
                                                        <label class="kt-checkbox">
                                                            <input type="checkbox" checked="checked"> Someone adds you as a connection
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-last row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">When To Escalate Emails</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="kt-checkbox-list">
                                                        <label class="kt-checkbox kt-checkbox--brand">
                                                            <input type="checkbox"> Upon new order.
                                                            <span></span>
                                                        </label>
                                                        <label class="kt-checkbox kt-checkbox--brand">
                                                            <input type="checkbox"> New membership approval
                                                            <span></span>
                                                        </label>
                                                        <label class="kt-checkbox kt-checkbox--brand">
                                                            <input type="checkbox" checked="checked"> Member registration
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-separator kt-separator--border-dashed kt-separator--portlet-fit kt-separator--space-lg"></div>
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <div class="row">
                                                <label class="col-xl-3"></label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <h3 class="kt-section__title kt-section__title-sm">Updates From Keenthemes:</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Email You With</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="kt-checkbox-list">
                                                        <label class="kt-checkbox">
                                                            <input type="checkbox"> News about Metronic product and feature updates
                                                            <span></span>
                                                        </label>
                                                        <label class="kt-checkbox">
                                                            <input type="checkbox"> Tips on getting more out of Keen
                                                            <span></span>
                                                        </label>
                                                        <label class="kt-checkbox">
                                                            <input type="checkbox" checked="checked"> Things you missed since you last logged into Keen
                                                            <span></span>
                                                        </label>
                                                        <label class="kt-checkbox">
                                                            <input type="checkbox" checked="checked"> News about Metronic on partner products and other services
                                                            <span></span>
                                                        </label>
                                                        <label class="kt-checkbox">
                                                            <input type="checkbox" checked="checked"> Tips on Metronic business products
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="modal_new_company" class="modal fade" data-callback="reload_companys_table" tabindex="" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form action="/{{$language}}/admin/company//ajax/setCompanyBranch" class="form" id="form_new_user">
                <input type="hidden" name="id">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ModalLabel"></h5>
                        <button type="reset" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="kt-portlet__body">
                            <div class="kt-section" id="section_one">
                                <h3 class="kt-section__title">@if($en) Tax Information @elseif($es) Informaciones fiscais @elseif($ptBR) Informações fiscais @endif:</h3>
                                <div class="kt-section__content">
                                    <div class="form-group row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <label class="form-control-label">* @if($en) Business Name @elseif($es) Nombre comercial @elseif($ptBR) Nome comercial @endif:</label>
                                            <input type="text" name="business_name" class="form-control" placeholder="@if($en) Business Name @elseif($es) Nombre comercial @elseif($ptBR) Nome comercial @endif">
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <label class="form-control-label">* @if($en) Trade Name @elseif($es) Nombre fiscal @elseif($ptBR) Nome fiscal @endif:</label>
                                            <input type="text" name="trading_name" class="form-control" placeholder="@if($en) Trade Name @elseif($es) Nombre fiscal @elseif($ptBR) Nome fiscal @endif">
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="mail">
                                        <div class="col-lg-12">
                                            <label class="form-control-label">* Email:</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="email" placeholder="Email">
                                                <div class="input-group-append"><a href="#" class="btn btn-brand btn-icon">@</a></div>
                                            </div>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <label class="form-control-label">@if($en) Phone @elseif($es) Tel. Fijo @elseif($ptBR) Tel. Fixo @endif:</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control mask_phone_brazil_fix" name="fix_phone" placeholder="@if($en) Phone @elseif($es) Tel. Fijo @elseif($ptBR) Tel. Fixo @endif">
                                                <div class="input-group-append"><a href="#" class="btn btn-brand btn-icon"><i class="la la-phone"></i></a></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <label class="form-control-label">* @if($en) Mobile @elseif($es) Tel. Móvil @elseif($ptBR) Tel. Móvel @endif:</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control mask_phone_brazil_mobile" name="mobile_phone" placeholder="@if($en) Mobile @elseif($es) Tel. Móvil @elseif($ptBR) Tel. Móvel @endif">
                                                <div class="input-group-append"><a href="#" class="btn btn-brand btn-icon"><i class="la la-mobile-phone"></i></a></div>
                                            </div>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <label class="form-control-label">* @if($en) Tax number @elseif($es) CIF @elseif($ptBR) CNPJ @endif:</label>
                                            <input type="text" class="form-control mask_number" name="identification_number" placeholder="@if($en) Tax number @elseif($es) CIF @elseif($ptBR) CNPJ @endif">
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <label class="form-control-label">* @if($en) State Tax number @elseif($es) Inscripción Estadual @elseif($ptBR) Inscrição Estadual @endif:</label>
                                            <input type="text" class="form-control mask_number" name="state_tax_number" placeholder="@if($en) State Tax number @elseif($es) Inscripción Estadual @elseif($ptBR) Inscrição Estadual @endif">
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-section" id="section_two">
                                <h3 class="kt-section__title">@if($en) Tax Information @elseif($es) Informaciones fiscais @elseif($ptBR) Informações fiscais @endif:</h3>
                                <div class="kt-section__content">
                                    <div class="form-group row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <label class="form-control-label">* @if($en) Business Name @elseif($es) Nombre comercial @elseif($ptBR) Nome comercial @endif:</label>
                                            <input type="text" name="business_name" class="form-control" placeholder="@if($en) Business Name @elseif($es) Nombre comercial @elseif($ptBR) Nome comercial @endif">
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <label class="form-control-label">* @if($en) Trade Name @elseif($es) Nombre fiscal @elseif($ptBR) Nome fiscal @endif:</label>
                                            <input type="text" name="trading_name" class="form-control" placeholder="@if($en) Trade Name @elseif($es) Nombre fiscal @elseif($ptBR) Nome fiscal @endif">
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="mail">
                                        <div class="col-lg-12">
                                            <label class="form-control-label">* Email:</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="email" placeholder="Email">
                                                <div class="input-group-append"><a href="#" class="btn btn-brand btn-icon">@</a></div>
                                            </div>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <label class="form-control-label">@if($en) Phone @elseif($es) Tel. Fijo @elseif($ptBR) Tel. Fixo @endif:</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control mask_phone_brazil_fix" name="fix_phone" placeholder="@if($en) Phone @elseif($es) Tel. Fijo @elseif($ptBR) Tel. Fixo @endif">
                                                <div class="input-group-append"><a href="#" class="btn btn-brand btn-icon"><i class="la la-phone"></i></a></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <label class="form-control-label">* @if($en) Mobile @elseif($es) Tel. Móvil @elseif($ptBR) Tel. Móvel @endif:</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control mask_phone_brazil_mobile" name="mobile_phone" placeholder="@if($en) Mobile @elseif($es) Tel. Móvil @elseif($ptBR) Tel. Móvel @endif">
                                                <div class="input-group-append"><a href="#" class="btn btn-brand btn-icon"><i class="la la-mobile-phone"></i></a></div>
                                            </div>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <label class="form-control-label">* @if($en) Tax number @elseif($es) CIF @elseif($ptBR) CNPJ @endif:</label>
                                            <input type="text" class="form-control mask_number" name="identification_number" placeholder="@if($en) Tax number @elseif($es) CIF @elseif($ptBR) CNPJ @endif">
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <label class="form-control-label">* @if($en) State Tax number @elseif($es) Inscripción Estadual @elseif($ptBR) Inscrição Estadual @endif:</label>
                                            <input type="text" class="form-control mask_number" name="state_tax_number" placeholder="@if($en) State Tax number @elseif($es) Inscripción Estadual @elseif($ptBR) Inscrição Estadual @endif">
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary ">@if($en) Back @elseif($es) Volver @elseif($ptBR) Atrás @endif</button>
                        <button type="button" class="btn btn-primary ">@if($en) next @elseif($es) Seguinte @elseif($ptBR) Seguinte @endif</button>
                        <button type="button" class="btn btn-primary send-form-modal">Salvar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- end:: Content -->


@endsection


@section('extra_js')

    <!--begin::Page Vendors(used by this page) -->
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/demo1/pages/crud/datatables/advanced/column-rendering.js') }}" type="text/javascript"></script>
    <!--end::Page Scripts -->

    <script>
        $(document).ready(function () {

            /** Global variables **************************************************************************/

            let line_data_pickup = null, /* Variable to guard the dataTable line values */
                validator = null, /* Variable to guard form validation rules */
                language = '{{ $language }}', /* Variable to guard user language */
                /** Comany Table - Retrieve lines from table comany to Datatable's. **/
                ajax_campany_table = {
                    url: "/" + language + "/admin/company/ajax/getCompanys",
                    type: "POST",
                    data: function (d) {
                        d.state_filter = $('#state_filter').val();
                        d.recycle_filter = $('#recycle_filter').is(':checked');
                        d.role_id = $('#role_filter').val();
                        d._token = window.Laravel.csrfToken;
                    }
                },
                campany_table = $('#campany_table').DataTable({
                    "language": {
                        "url": dataTableLanguage
                    },
                    ajax: ajax_campany_table,
                    responsive: false,
                    pagingType: 'full_numbers',
                    paging: true,
                    columns: [
                        {
                            data: "logo",
                            sClass: "insertFile va-middle padding-foto padding-top-bottom"
                        },
                        {
                            data: "full_name",
                            sClass: "va-middle"
                        },
                        {
                            data: "email",
                            sClass: "va-middle",
                            render: function (data, type, full, meta) {
                                return data ? '<a class="kt-link" href="mailto:' + data + '">' + data + '</a>' : '';
                            },
                        },
                        {
                            data: "mobile_phone",
                            sClass: "va-middle"
                        },
                        {
                            data: "federal_tax_number",
                            sClass: "va-middle"
                        },
                        {
                            data: "users_count",
                            sClass: "va-middle"
                        },
                        {
                            data: "branch",
                            sClass: "va-middle"
                        },
                        {
                            data: "active",
                            sClass: "va-middle"
                        },
                        {
                            data: "actions",
                            sClass: "va-middle align-center"
                        },
                    ],
                    columnDefs: [
                        {targets: -1, orderable: false, width: '5%'},
                        {targets: -2, orderable: true, width: '1%'},
                        {targets: 0, orderable: false, width: '1%'},
                    ],
                    order: [1, 'asc'],

                }).on('click', 'span.linkActive', function () {
                    let tr = $(this).parent().parent(), id = campany_table.row(tr).data().id;
                    $.ajax({
                        type: 'POST',
                        url: "/" + language + '/admin/users/employee/ajax/toggleActive',
                        data: {
                            id: id,
                            _token: window.Laravel.csrfToken
                        },
                        success: function (data) {
                            reload_companys_table();
                            if (data.not) {
                                toastr[data.not.tipo](data.not.mensaje, data.not.titulo);
                            }
                        }
                    });
                }).on('click', 'span.dropdown', function () {
                    let tr = $(this).parent().parent().parent();
                    line_data_pickup = campany_table.row(tr).data();
                }).on('click', 'td div a', function () {
                    let tr = $(this).parent().parent().parent();
                    line_data_pickup = campany_table.row(tr).data();
                }).on('click', 'td.insertFile', function () {
                    let tr = $(this).parent(), data = campany_table.row(tr).data();
                    if (data.id != undefined) {
                        $('#modal_new_picture input[name="id"]').val(data.id);
                        $('#modal_new_picture input[type="file"]').val('');
                        $('#modal_new_picture').modal({
                            backdrop: 'static',
                            keyboard: false,
                            show: true
                        }).on('hidden.bs.modal', function () {
                            reload_companys_table();
                        });
                    }
                });


            /** Events *****************************************************************************/


            $(document).on('select2:select', '#state_filter', function () {
                reload_companys_table();
            }).on('change', '#recycle_filter', function () {
                reload_companys_table();
            }).on('click', '[name="save_profile"]', function (e) {
                e.preventDefault();
                let $btn = $(this), form = $('#company_profile').closest('form');
                $btn.addClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                sendForm($btn, form);
            }).on('click', '#new_company', function () {
                validation('company', true);
                validator.resetForm();
                $('#modal_new_company #mail').show();
                $('#modal_new_company .send-form-modal').removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                $('#modal_new_company .modal-title').text('Novo colaborador');
                $('#modal_new_company input[name="id"]').val('');
                $('#modal_new_company input[type="text"]').val('');
                $('#modal_new_company input[type="checkbox"]').prop('checked', false);
                $('#modal_new_company select').val(0).trigger('change');
                $('#modal_new_company').modal({
                    backdrop: 'static',
                    keyboard: false,
                    show: true
                }).on('hidden.bs.modal', function () {
                    reload_companys_table();
                });
            }).on('click', '.send-form-modal_new_company', function (e) {
                e.preventDefault();
                let $btn = $(this), form = $(this).closest('form'), modal = $(this).closest('.modal');
                $btn.addClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                if (form.valid()) {
                    sendForm($btn, form, modal);
                } else {
                    $btn.removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                }
            }).on('click', '.send-form', function (e) {
                e.preventDefault();
                let $btn = $(this), form = $(this).closest('form'), modal = $(this).closest('.modal');
                $btn.addClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                if (form.valid()) {
                    sendForm($btn, form, modal);
                } else {
                    $btn.removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                }
            }).on('click', 'a.user_options', function () {
                let action = $(this).data('action');
                switch (action) {

                    case 'see':
                        op_see(line_data_pickup);
                        break;

                    case 'lock':
                        op_lock(line_data_pickup);
                        break;

                    case 'password':
                        op_password(line_data_pickup);
                        break;

                    case 'edit':
                        op_edit(line_data_pickup);
                        break;

                    case 'softdelete':
                        op_delete(line_data_pickup, 'Eliminar usúario', '<p>Deseja continuar?</p>', 'softdelete', "/" + language + '/admin/users/ajax/deleteUser', 'reload_companys_table');
                        break;

                    case 'recycle':
                        op_delete(line_data_pickup, 'Recuperar usúario', '<p>Deseja continuar?</p>', 'recycle', "/" + language + '/admin/users/ajax/deleteUser', 'reload_companys_table');
                        break;

                    default:
                        return false;
                }
            });


            /** Functions *****************************************************************************/


            /**
             * Function to Send Data from form DataTable
             *
             */
            function sendForm($btn, form) {
                let action = form.attr('action'), formdata = new FormData(form[0]);
                setTimeout(function () {
                    $.ajax({
                        type: "POST",
                        url: action,
                        data: formdata,
                        async: false,
                        success: function (data) {
                            if (data.status === 1 || data.status === 15) {
                                location.reload();
                            }
                            if (data.not) {
                                toastr[data.not.tipo](data.not.mensaje, data.not.titulo);
                            }
                            $btn.removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                        },
                        error: function () {
                            $btn.removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                }, 1000);
            }

            /**
             * Function to Send Data from form DataTable
             *
             */
            function sendFormDataTable($btn, form, modal) {
                let callback = modal.data('callback'), action = form.attr('action'), formdata = new FormData(form[0]);
                setTimeout(function () {
                    $.ajax({
                        type: "POST",
                        url: action,
                        data: formdata,
                        async: false,
                        success: function (data) {
                            if (data.status === 1 || data.status === 15) {
                                modal.modal('hide');
                            }
                            if (callback) {
                                eval(callback + '()');
                            }
                            if (data.not) {
                                toastr[data.not.tipo](data.not.mensaje, data.not.titulo);
                            }
                            $btn.removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                        },
                        error: function () {
                            $btn.removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                }, 1000);
            }

            function reload_companys_table() {
                campany_table.ajax.reload(null, false);
            }

            function op_see(data) {
                let url = window.location.href, id = data.id;
                window.open(url + '/' + id, '_self');
            }

            function op_lock(data) {
                $('#modal_lock input[name="id"]').val(data.id);
                if (data.active_o) {
                    $('#modal_lock .modal-title').text('Desativar colaborador');
                    $('#modal_lock .modal-body').html('<p>Se desativar o colaborador, esse já não poderá utilizar a aplicação. Deseja continuar?</p>');
                } else {
                    $('#modal_lock .modal-title').text('Ativar colaborador');
                    $('#modal_lock .modal-body').html('<p>Se ativar o colaborador, esse poderá utilizar a aplicação. Deseja continuar?</p>');
                }
                $('#modal_lock').modal('show');
            }

            function op_password(data) {
                $('#modal_email_password input[name="id"]').val(data.id);
                $('#modal_email_password .modal-body').html('<p>Deseja enviar um email para o usuario redefinir sua senha?</p>');
                $('#modal_email_password').modal('show');
            }

            function op_edit(data) {
                validation('user', false);
                validator.resetForm();
                $('#modal_new_company #mail').hide();
                $('#modal_new_company .modal-title').text('Editar colaborador');
                $('#modal_new_company input[name="id"]').val(data.id);
                $('#modal_new_company input[name="name"]').val(data.name);
                $('#modal_new_company input[name="last_name"]').val(data.last_name);
                $('#modal_new_company input[name="fix_phone"]').val(data.fix_phone);
                $('#modal_new_company input[name="mobile_phone"]').val(data.mobile_phone);
                $('#modal_new_company input[name="identification_number"]').val(data.identification_number);
                $('#modal_new_company input[name="social_number"]').val(data.social_number);
                $('#modal_new_company select[name="role_employee"]').val(data.role_id).trigger('change');
                $('#modal_new_company .send-form-modal_new_company').removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                $('#modal_new_company').modal({
                    backdrop: 'static',
                    keyboard: false,
                    show: true
                }).on('hidden.bs.modal', function () {
                    reload_companys_table();
                });
            }

            function op_delete(data, title, mensage, options, action, callback) {
                $('#modal_delete form').attr('action', action);
                $('#modal_delete input[name="id"]').val(data.id);
                $('#modal_delete input[name="trashed"]').val(options);
                $('#modal_delete .modal-title').text(title);
                $('#modal_delete .modal-body').html(mensage);
                $('#modal_delete').data('callback', callback).modal('show');
            }

            function validation(form_name, rule) {

                if (form_name === 'company') {
                    validator = $("#form_new_user").validate({
                        // define validation rules
                        rules: {
                            business_name: {
                                required: true,
                                minlength: 1
                            },
                            trading_name: {
                                required: true,
                                minlength: 1
                            },
                            email: {
                                required: rule,
                                email: rule,
                                minlength: rule ? 10 : 0,
                            },
                            federal_tax_number: {
                                required: true,
                                minlength: 1
                            },
                            state_tax_number: {
                                required: true,
                                minlength: 1
                            },
                            mobile_phone: {
                                required: true,
                                minlength: 1
                            },
                        },
                        //display error alert on form submit
                        invalidHandler: function (event) {
                            swal.fire({
                                "title": "",
                                "html": "Existem alguns erros no formulário. <br />Por favor corrigi-los.",
                                "type": "error",
                                "confirmButtonClass": "btn btn-brand",
                                "onClose": function (e) {
                                    //console.log('on close event fired!');
                                }
                            });

                            event.preventDefault();
                        },
                        submitHandler: function () {
                            return true;
                        }
                    });
                }
            }
        });
    </script>


@endsection