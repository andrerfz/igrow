<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8"/>
    <title>{{ config('app.name', 'Restmar') }} | {{  Auth::user()->cliente->nombre }}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>

    <style>

        @page {
            /*size: a4 portrait;*/
            margin-bottom: 5px;
        }

        body {

            font-size: 14px;
        }

        td {

            border-bottom: 1px solid #ddd;
            margin: 5px;
        }

        td.align {
            text-align: right;
            padding-right: 5px;
        }

        table {

            border-color: gray;
            border-spacing: 0;
        }

        ul {

            list-style-type: none;
        }

        .page_break {
            page-break-before: always;
        }

        .rotateFirst {

            font-size: 8px !important;
            left: -480px;
            position: relative;
            text-align: left;
            top: 155px;
            width: 900px;

            /* Safari */
            -webkit-transform: rotate(-90deg);

            /* Firefox */
            -moz-transform: rotate(-90deg);

            /* IE */
            -ms-transform: rotate(-90deg);

            /* Opera */
            -o-transform: rotate(-90deg);
        }

        .rotateLastones {

            font-size: 8px !important;
            left: -480px;
            position: relative;
            text-align: left;
            top: 345px;
            width: 900px;

            /* Safari */
            -webkit-transform: rotate(-90deg);

            /* Firefox */
            -moz-transform: rotate(-90deg);

            /* IE */
            -ms-transform: rotate(-90deg);

            /* Opera */
            -o-transform: rotate(-90deg);
        }

        .rotateLast {

            font-size: 8px !important;
            left: -1010px;
            position: relative;
            text-align: left;
            top: 345px;
            width: 900px;

            /* Safari */
            -webkit-transform: rotate(-90deg);

            /* Firefox */
            -moz-transform: rotate(-90deg);

            /* IE */
            -ms-transform: rotate(-90deg);

            /* Opera */
            -o-transform: rotate(-90deg);
        }

    </style>

</head>


<body>

<div style="width: 190mm;">

    <script type="text/php">

        if ( isset($pdf) ) {
            $x = 530;
            $y = 10;
            $text = "Página {PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("helvetica", "");
            $size = 9;
            $color = array(0, 0, 0);
            $word_space = 0.0;  //  default
            $char_space = 0.0;  //  default
            $angle = 0.0;   //  default
            $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
        }



    </script>

    <div style="height: 105px;">
        <div style="float: left; width: 40%; height: 100px; text-align:left; overflow: hidden;">
            @if ($base64)
                <img src="{{$base64}}" alt="" style="height: 100px;">
            @endif
        </div>
        <div style="float: left; width: 25%; text-align: center; padding-top: 15px;">
            <span style="font-size: 36px; font-weight: bold;">Factura</span>
        </div>
        <div style="float: left; width: 35%;">
            <ul style="font-size: 11px!important;">
                <li style="font-size: 16px!important; text-align: right;">
                    Fecha: {{date('d/m/Y', strtotime($factura->created_at))}}</li>
                <li style="font-size: 16px!important; text-align: right;">
                    Hora: {{date('H:i:s', strtotime($factura->created_at))}}</li>
                <li style="font-size: 16px!important; text-align: right;">Factura Nº: {{$factura->is_proveedor?$factura->num_referencia:$factura->num_facturas?:$factura->id}}</li>
                <li style="font-size: 12px!important; text-align: right;">Facturado
                    por: {{$factura->usuario->full_name}}</li>
            </ul>
        </div>
    </div>
    <div style="height: 105px; margin-top: 0;">
        <div style="float: left; margin-left: -30px; width: 50%;">
            <ul style="font-size: 11px!important;">
                <li><strong>{{ strtoupper($proveedor->nombre) }}</strong></li>
                <li><strong>CIF</strong> {{ $proveedor->cif }}</li>
                <li>{{ $proveedor->direccion }}</li>
                <li>{{ strtoupper($proveedor->cp .' '. $proveedor->municipio .' '. $proveedor->provincia .' '. $proveedor->pais) }}</li>
                <li><span title="Telefono">T:</span> {{ $proveedor->telefono }} <span
                            title="Movil">M:</span> {{ $proveedor->movil }}</li>
                <li><span title="Email">Em@il:</span> {{ $proveedor->email }}</li>
            </ul>
        </div>
        <div style="float: right; margin-right: 10px; width: 50%;">
            <ul style="font-size: 11px!important;">
                @if($factura->local)
                    <li style="text-align: right;"><strong> {{ strtoupper($factura->local->nombre) }}</strong></li>
                    <li style="text-align: right;"><strong>CIF</strong> {{$factura->local->cif}}</li>
                    <li style="text-align: right;">{{ strtoupper($factura->local->cp .' '. $factura->local->municipio)}}</li>
                    <li style="text-align: right;">{{ strtoupper($factura->local->provincia .' '. $factura->local->pais)}}</li>
                @endif
            </ul>
        </div>
    </div>

    <!-- First Page -->
    @php $a = 1 @endphp
    @if ($pages >= 0)
        <div style="margin-top: -10px;">
            <div class="rotateFirst">
                <span style="width: 100%;">
                Documento emitido por {{ strtoupper($proveedor->nombre .' - '. $proveedor->cp .' '. $proveedor->municipio .' '. $proveedor->provincia .' '. $proveedor->pais)}};
                domicilio social: {{ strtoupper($proveedor->cp .' '. $proveedor->municipio .' '. $proveedor->provincia .' '. $proveedor->pais) }}.
                Inscrita en el Registro Mercantil de ... - {{ $proveedor->cif }}
                </span>
            </div>
            <table style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; width: 100%;">
                <thead>
                <tr>
                    <th width="20%" height="30px" style="text-align: left;">Número del albarán</th>
                    <th width="20%" style="text-align: left">Fecha del albarán</th>
                    <th width="20%" style="text-align: left">Fecha del pedido</th>
                    <th width="15%" style="text-align: right">Subtotal</th>
                    <th width="15%" style="text-align: right">IVA</th>
                    <th width="15%" style="text-align: right">Total</th>
                </tr>
                </thead>
                <tbody>
                @foreach(array_slice($lineasAlbaran['data'],0,24) as $td)
                    <tr>
                        <th width="20%" height="20px" style="text-align: left;">{{$td['id']}}</th>
                        <th width="20%" style="text-align: left">{{$td['fecha']}}</th>
                        <th width="20%" style="text-align: left">{{$td['fecha_pedido']}}</th>
                        <th width="15%" style="text-align: right">{{$td['subtotal']}}</th>
                        <th width="15%" style="text-align: right">{{$td['iva']}}</th>
                        <th width="15%" style="text-align: right">{{$td['importe_iva']}}</th>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
    <!-- Other Pages -->
    @if ($pages >= 1)
        @php
            $firstposition = 24;
            $lastposition = 31;
        @endphp
        @for ($i = 1; $i <= $pages; $i++)
            @if ($i === $pages)
                @php $lastposition = (key(array_slice($lineasAlbaran['data'],-1,1,true))+1); @endphp
                @break;
            @endif
            <div style="margin-top: 10px; width: 100%;">
                <div class="rotateLastones">
                        <span style="width: 100%;">
                            Documento emitido por {{ $proveedor->nombre }} - {{ $proveedor->cp .' '. $proveedor->municipio .' '. $proveedor->provincia .' '. $proveedor->pais}};
                            domicilio social: {{ $proveedor->cp .' '. $proveedor->municipio .' '. $proveedor->provincia .' '. $proveedor->pais}}.
                            Inscrita en el Registro Mercantil de ... - {{ $proveedor->cif }}
                        </span>
                </div>
                <table style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; width: 100%;">
                    <thead>
                    <tr>
                        <th width="20%" height="30px" style="text-align: left;">Número del albarán</th>
                        <th width="20%" style="text-align: left">Fecha del albarán</th>
                        <th width="20%" style="text-align: left">Fecha del pedido</th>
                        <th width="15%" style="text-align: right">Subtotal</th>
                        <th width="15%" style="text-align: right">IVA</th>
                        <th width="15%" style="text-align: right">Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(array_slice($lineasAlbaran['data'],$firstposition,$lastposition,true) as $td)
                        <tr>
                            <th width="20%" height="20px" style="text-align: left;">{{$td['id']}}</th>
                            <th width="20%" style="text-align: left">{{$td['fecha']}}</th>
                            <th width="20%" style="text-align: left">{{$td['fecha_pedido']}}</th>
                            <th width="15%" style="text-align: right">{{$td['subtotal']}}</th>
                            <th width="15%" style="text-align: right">{{$td['iva']}}</th>
                            <th width="15%" style="text-align: right">{{$td['importe_iva']}}</th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @if ($i < $pages)
                @php $firstposition += 31 @endphp
            @endif
        @endfor
    @endif

    <div style="margin-left: 530px; margin-top: 10px">
        @if ($pages === 0 && $lineas > 24)
            <div class="rotateLast">
                <span style="width: 100%;">
                    Documento emitido por {{ strtoupper($proveedor->nombre .' - '. $proveedor->cp .' '. $proveedor->municipio .' '. $proveedor->provincia .' '. $proveedor->pais)}};
                    domicilio social: {{ strtoupper($proveedor->cp .' '. $proveedor->municipio .' '. $proveedor->provincia .' '. $proveedor->pais) }}.
                    Inscrita en el Registro Mercantil de ... - {{ $proveedor->cif }}
                </span>
            </div>
        @endif
    </div>
    <div style="margin-left: 0;">
        <div style="float: left; width: 50%; margin-top: 10px;">
            @if ($lineasFactura['recargo_equivalencia'])
            <table style="width: 100%;">
                <thead>
                <tr>
                    <th width="35%" height="20px" style="text-align: left;">B. Imponible</th>
                    <th width="60%" style="text-align: left">Tipo IVA</th>
                    <th width="15%" style="text-align: left">IVA</th>
                    <th width="15%" style="text-align: left">RE</th>
                </tr>
                </thead>
                <tbody>
                @if ($lineasFactura['importe_iva4'] > 0)
                    <tr>
                        <td nowrap height="20px" style="text-align: left;">{{number_format((float)$lineasFactura['importe_iva4'], 2, ',', '')}}</td>
                        <td nowrap style="text-align: left;">Superreducido (4% + 0,5%)</td>
                        <td nowrap style="text-align: left;">{{number_format((float)$lineasFactura['iva4'], 2, ',', '')}}</td>
                        <td nowrap style="text-align: left;">{{number_format((float)$lineasFactura['req4'], 2, ',', '')}}</td>
                    </tr>
                @endif
                @if ($lineasFactura['importe_iva10'] > 0)
                    <tr>
                        <td nowrap height="20px" style="text-align: left;">{{number_format((float)$lineasFactura['importe_iva10'], 2, ',', '')}}</td>
                        <td nowrap style="text-align: left;">Reducido (10% + 1,4%)</td>
                        <td nowrap style="text-align: left;">{{number_format((float)$lineasFactura['iva10'], 2, ',', '')}}</td>
                        <td nowrap style="text-align: left;">{{number_format((float)$lineasFactura['req10'], 2, ',', '')}}</td>
                    </tr>
                @endif
                @if ($lineasFactura['importe_iva21'] > 0)
                    <tr>
                        <td nowrap height="20px" style="text-align: left;">{{number_format((float)$lineasFactura['importe_iva21'], 2, ',', '')}}</td>
                        <td nowrap style="text-align: left;">General (21% + 5,2%)</td>
                        <td nowrap style="text-align: left;">{{number_format((float)$lineasFactura['iva21'], 2, ',', '')}}</td>
                        <td nowrap style="text-align: left;">{{number_format((float)$lineasFactura['req21'], 2, ',', '')}}</td>
                    </tr>
                @endif
                </tbody>
            </table>
            @endif
            <br/>
            @if (count($lineasFactura['impuestos']) > 0)
                <table style="width: 100%;">
                    <thead>
                    <tr>
                        <th width="33%" height="20px" style="text-align: left;">Impuesto</th>
                        <th width="33%" style="text-align: left">Base de cálculo</th>
                        <th width="33%" style="text-align: right">Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($lineasFactura['impuestos'] as $key => $impuestos)
                        <tr>
                            <td nowrap height="20px" style="text-align: left;">{{$key}}</td>
                            <td nowrap style="text-align: center;">@if($impuestos['cantidad'] > 0) {{number_format((float)$impuestos['cantidad'], 2, ',', '')}} @elseif($impuestos['porcentaje'] > 0) {{$impuestos['porcentaje'].'%'}} @endif</td>
                            <td nowrap style="text-align: right;">{{number_format((float)$impuestos['importe_impuesto'], 3, ',', '')}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
        <div style="float: right; width: 20%;">
            <table style="width: 100%; font-size: 14px!important;">
                <tbody>
                <tr>
                    <td nowrap height="20px" style="text-align: left;"><strong>Pactado: </strong></td>
                    <td nowrap style="text-align: right;">{{number_format((float)$lineasFactura['pactado'], 2, ',', '')}}</td>
                </tr>
                <tr>
                    <td nowrap height="20px" style="text-align: left;"><strong>Descuento: </strong></td>
                    <td nowrap style="text-align: right;">{{number_format((float)$lineasFactura['descuentototal'], 2, ',', '')}}</td>
                </tr>
                <tr>
                    <td nowrap height="20px" style="text-align: left;"><strong>Subtotal:</strong></td>
                    <td nowrap style="text-align: right;">{{ number_format($factura->importe, 2, ',', '.') }}</td>
                </tr>
                @if ($lineasFactura['iva4'] > 0 && !($lineasFactura['req4'] > 0))
                    <tr>
                        <td nowrap height="20px" style="text-align: left;"><strong>IVA 4%: </strong></td>
                        <td nowrap style="text-align: right;">{{number_format((float)$lineasFactura['iva4'], 2, ',', '')}}</td>
                    </tr>
                @endif
                @if ($lineasFactura['iva10'] > 0 && !($lineasFactura['req10'] > 0))
                    <tr>
                        <td nowrap height="20px" style="text-align: left;"><strong>IVA 10%: </strong></td>
                        <td nowrap style="text-align: right;">{{number_format((float)$lineasFactura['iva10'], 2, ',', '')}}</td>
                    </tr>
                @endif
                @if ($lineasFactura['iva21'] > 0 && !($lineasFactura['req21'] > 0))
                    <tr>
                        <td nowrap height="20px" style="text-align: left;"><strong>IVA 21%: </strong></td>
                        <td nowrap style="text-align: right;">{{number_format((float)$lineasFactura['iva21'], 2, ',', '')}}</td>
                    </tr>
                @endif
                @if ($lineasFactura['total_recargo'] > 0 && $lineasFactura['recargo_equivalencia'])
                    <tr>
                        <td nowrap height="20px" style="text-align: left;"><strong>R. equivalencia </strong></td>
                        <td nowrap style="text-align: right;">{{number_format((float)$lineasFactura['total_recargo'], 2, ',', '')}}</td>
                    </tr>
                @endif
                @if ($lineasFactura['total_iva'] > 0 && $lineasFactura['recargo_equivalencia'])
                    <tr>
                        <td nowrap height="20px" style="text-align: left;"><strong>Total IVA: </strong></td>
                        <td nowrap style="text-align: right;">{{number_format((float)$lineasFactura['total_iva'], 2, ',', '')}}</td>
                    </tr>
                @endif
                @if ($lineasFactura['importe_impuesto'] > 0)
                    <tr>
                        <td nowrap height="20px" style="text-align: left;"><strong>Impuestos: </strong></td>
                        <td nowrap style="text-align: right;">{{number_format((float)$lineasFactura['importe_impuesto'], 3, ',', '')}}</td>
                    </tr>
                @endif
                <tr style="background-color: #c0dff4; width: 120px;">
                    <td nowrap height="20px" style="text-align: left;"><strong>Total: </strong></td>
                    <td nowrap style="text-align: right;">{{$lineasFactura['preciototal']}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

</body>
</html>