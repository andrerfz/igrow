<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8"/>
    <title>{{ config('app.name', 'Restmar') }} | {{  Auth::user()->cliente->nombre }}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>

    <style>

        @page {
            /*size: a4 portrait;*/
            margin-bottom: 5px;
        }

        body {

            font-size: 14px;
        }

        td {

            border-bottom: 1px solid #ddd;
            margin: 5px;
        }

        td.align {
            text-align: right;
            padding-right: 5px;
        }

        table {

            border-color: gray;
            border-spacing: 0;
        }

        ul {

            list-style-type: none;
        }

        .rotateFirst {

            font-size: 8px !important;
            left: -480px;
            position: relative;
            text-align: left;
            top: 155px;
            width: 900px;

            /* Safari */
            -webkit-transform: rotate(-90deg);

            /* Firefox */
            -moz-transform: rotate(-90deg);

            /* IE */
            -ms-transform: rotate(-90deg);

            /* Opera */
            -o-transform: rotate(-90deg);
        }

        .rotateLastones {

            font-size: 8px !important;
            left: -480px;
            position: relative;
            text-align: left;
            top: 345px;
            width: 900px;

            /* Safari */
            -webkit-transform: rotate(-90deg);

            /* Firefox */
            -moz-transform: rotate(-90deg);

            /* IE */
            -ms-transform: rotate(-90deg);

            /* Opera */
            -o-transform: rotate(-90deg);
        }

        .rotateLast {

            font-size: 8px !important;
            left: -1010px;
            position: relative;
            text-align: left;
            top: 345px;
            width: 900px;

            /* Safari */
            -webkit-transform: rotate(-90deg);

            /* Firefox */
            -moz-transform: rotate(-90deg);

            /* IE */
            -ms-transform: rotate(-90deg);

            /* Opera */
            -o-transform: rotate(-90deg);
        }

    </style>

</head>


<body>

<div style="width: 190mm;">

    <script type="text/php">

        if ( isset($pdf) ) {
            $x = 545;
            $y = 10;
            $text = "Página {PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("helvetica", "");
            $size = 9;
            $color = array(0, 0, 0);
            $word_space = 0.0;  //  default
            $char_space = 0.0;  //  default
            $angle = 0.0;   //  default
            $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
        }



    </script>

    <div style="height: 105px;">
        <div style="float: left; width: 40%; height: 100px; text-align:left; overflow: hidden;">
            @if ($base64)
                <img src="{{$base64}}" alt="" style="height: 100px;">
            @endif
        </div>
        <div style="float: left; width: 25%; text-align: center; padding-top: 15px;">
            <span style="font-size: 36px; font-weight: bold;">Albarán</span>
        </div>
        <div style="float: left; width: 35%;">
            <ul style="font-size: 11px!important;">
                <li style="font-size: 16px!important; text-align: right;">
                    Fecha: {{$pedidoToAlbaran->pedido ? date('d/m/Y', strtotime($pedidoToAlbaran->pedido->created_at)) : ''}} </li>
                <li style="font-size: 16px!important; text-align: right;">
                    Hora: {{$pedidoToAlbaran->pedido ? date('H:i:s', strtotime($pedidoToAlbaran->pedido->created_at)) : ''}}</li>
                <li style="font-size: 16px!important; text-align: right;">Albaran Nº: {{$albaran->id}}</li>
                <li style="font-size: 12px!important; text-align: right;">Pedido por: {{$pedidoToAlbaran->pedido ? $pedidoToAlbaran->pedido->usuario->full_name : ''}}</li>
            </ul>
        </div>
    </div>
    <div style="height: 105px; margin-top: 0;">
        <div style="float: left; margin-left: -30px; width: 50%;">
            <ul style="font-size: 11px!important;">
                <li><strong>{{ strtoupper($proveedor->nombre) }}</strong></li>
                <li><strong>CIF</strong> {{ $proveedor->cif }}</li>
                <li>{{ $proveedor->direccion }}</li>
                <li>{{ strtoupper($proveedor->cp .' '. $proveedor->municipio .' '. $proveedor->provincia .' '. $proveedor->pais) }}</li>
                <li><span title="Telefono">T:</span> {{ $proveedor->telefono }} <span
                            title="Movil">M:</span> {{ $proveedor->movil }}</li>
                <li><span title="Email">Em@il:</span> {{ $proveedor->email }}</li>
            </ul>
        </div>
        <div style="float: right; margin-right: 10px; width: 50%;">
            <ul style="font-size: 11px!important;">
                <li style="text-align: right;"><strong> {{ strtoupper($albaran->local->nombre) }}</strong></li>
                <li style="text-align: right;"><strong>CIF</strong> {{$albaran->local->cif}}</li>
                <li style="text-align: right;">{{ strtoupper($albaran->local->cp .' '. $albaran->local->municipio)}}</li>
                <li style="text-align: right;">{{ strtoupper($albaran->local->provincia .' '. $albaran->local->pais)}}</li>
            </ul>
        </div>
    </div>
    <!-- First Page -->
    @php $a = 1 @endphp
    @if ($pages >= 0)
        <div style="margin-top: -10px;">
            <div class="rotateFirst">
                <span style="width: 100%;">
                Documento emitido por {{ strtoupper($proveedor->nombre .' - '. $proveedor->cp .' '. $proveedor->municipio .' '. $proveedor->provincia .' '. $proveedor->pais)}};
                domicilio social: {{ strtoupper($proveedor->cp .' '. $proveedor->municipio .' '. $proveedor->provincia .' '. $proveedor->pais) }}.
                Inscrita en el Registro Mercantil de ... - {{ $proveedor->cif }}
                </span>
            </div>
            <table style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; width: 100%;">
                <thead>
                <tr>
                    <th width="12,5%" height="35px" style="text-align: left;">Pedido</th>
                    <th width="25%" style="text-align: left">Producto</th>
                    <th width="12,0%" style="text-align: right">Ubicación</th>
                    <th width="12,0%" style="text-align: right">Cantidad</th>
                    <th width="14,5%" style="text-align: right">Precio Unitario</th>
                    <th width="12,0%" style="text-align: right">IVA</th>
                    <th width="12,0%" style="text-align: right">Importe</th>
                </tr>
                </thead>
                <tbody>
                @foreach(array_slice($lineasAlbaranes['data'],0,24) as $td)
                    <tr>
                        <td height="25px" style="text-align: left">{{$td['id_pedido']}}</td>
                        <td style="text-align: left;">{{$td['producto_nombre']}}</td>
                        <td class="align">{{$td['ubicacion']}}</td>
                        <td class="align">{{$td['cantidad']}}</td>
                        <td class="align">{{$td['precio_pactado_print']['ver']}}</td>
                        <td class="align">{{$td['iva']}}</td>
                        <td class="align">{{$td['precio_total']['ver']}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
<!-- Other Pages -->
    @if ($pages >= 1)
        @php
            $firstposition = 24;
            $lastposition = 31;
        @endphp
        @for ($i = 1; $i <= $pages; $i++)
            @if ($i == $pages)
                @php $lastposition = (key(array_slice($lineasAlbaranes['data'],-1,1,true))+1); @endphp
                @break;
            @endif
            <div style="margin-top: 10px; width: 100%;">
                <div class="rotateLastones">
                        <span style="width: 100%;">
                            Documento emitido por {{ $proveedor->nombre }} - {{ $proveedor->cp .' '. $proveedor->municipio .' '. $proveedor->provincia .' '. $proveedor->pais}};
                            domicilio social: {{ $proveedor->cp .' '. $proveedor->municipio .' '. $proveedor->provincia .' '. $proveedor->pais}}.
                            Inscrita en el Registro Mercantil de ... - {{ $proveedor->cif }}
                        </span>
                </div>
                <table style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; width: 100%;">
                    <thead>
                    <tr>
                        <th width="12,5%" height="35px" style="text-align: left;">Pedido</th>
                        <th width="25%" style="text-align: left">Producto</th>
                        <th width="12,0%" style="text-align: right">Ubicación</th>
                        <th width="12,0%" style="text-align: right">Cantidad</th>
                        <th width="14,5%" style="text-align: right">Precio Unitario</th>
                        <th width="12,0%" style="text-align: right">IVA</th>
                        <th width="12,0%" style="text-align: right">Importe</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(array_slice($lineasAlbaranes['data'],$firstposition,$lastposition,true) as $td)
                        <tr>
                            <td height="25px" style="text-align: left">{{$td['id_pedido']}}</td>
                            <td style="text-align: left;">{{$td['producto_nombre']}}</td>
                            <td class="align">{{$td['ubicacion']}}</td>
                            <td class="align">{{$td['cantidad']}}</td>
                            <td class="align">{{$td['precio_pactado_print']['ver']}}</td>
                            <td class="align">{{$td['iva']}}</td>
                            <td class="align">{{$td['precio_total']['ver']}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @if ($i < $pages)
                @php $firstposition += 31 @endphp
            @endif
        @endfor
    @endif
    <div style="margin-left: 530px; margin-top: 10px">
        @if ($pages === 0 && $lineas > 24)
            <div class="rotateLast">
                    <span style="width: 100%;">
                        Documento emitido por {{ strtoupper($proveedor->nombre .' - '. $proveedor->cp .' '. $proveedor->municipio .' '. $proveedor->provincia .' '. $proveedor->pais)}};
                        domicilio social: {{ strtoupper($proveedor->cp .' '. $proveedor->municipio .' '. $proveedor->provincia .' '. $proveedor->pais) }}.
                        Inscrita en el Registro Mercantil de ... - {{ $proveedor->cif }}
                    </span>
            </div>
        @endif
    </div>
    <div style="margin-left: 0;">
        <div style="float: left; width: 40%; margin-top: 10px;">
            @if (count($lineasAlbaranes['impuestos']) > 0)
            <table style="width: 100%;">
                <thead>
                <tr>
                    <th width="33%" height="20px" style="text-align: left;">Impuesto</th>
                    <th width="33%" style="text-align: left">Base de cálculo</th>
                    <th width="33%" style="text-align: right">Total</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($lineasAlbaranes['impuestos'] as $key => $impuestos)
                    <tr>
                        <td nowrap height="20px" style="text-align: left;">{{$key}}</td>
                        <td nowrap style="text-align: center;">@if($impuestos['cantidad'] > 0) {{number_format((float)$impuestos['cantidad'], 2, ',', '')}} @elseif($impuestos['porcentaje'] > 0) {{$impuestos['porcentaje'].'%'}} @endif</td>
                        <td nowrap style="text-align: right;">{{number_format((float)$impuestos['importe_impuesto'], 3, ',', '')}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endif
            <br/>
            @if ($lineasAlbaranes['firma'])
                <table id="tabla_firma" style="width: 80%;">
                    <thead>
                    <tr style="border-color: white;">
                        <th style="text-align: left;">Firma del recibo</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="border-color: white;">
                            <img src="{{$lineasAlbaranes['repartidor_firma']}}" style="height: 60px; width: 180px; float: left; margin-top: 10px;"/>
                        </td>
                    </tr>
                    </tbody>
                </table>
            @endif
        </div>
        <div style="float: right; width: 20%;">
            <table style="width: 100%; font-size: 14px!important;">
                <tbody>
                <tr>
                    <td nowrap height="20px" style="text-align: left;"><strong>Descuento: </strong></td>
                    <td nowrap style="text-align: right;">{{number_format((float)$lineasAlbaranes['descuentototal'], 2, ',', '')}}</td>
                </tr>
                <tr>
                    <td nowrap height="20px" style="text-align: left;"><strong>Subtotal:</strong></td>
                    <td nowrap style="text-align: right;">{{ number_format($albaran->importe, 2, ',', '.') }}</td>
                </tr>
                @if ($lineasAlbaranes['precioiva'][4] > 0)
                    <tr>
                        <td nowrap height="20px" style="text-align: left;"><strong>IVA 4%: </strong></td>
                        <td nowrap style="text-align: right;">{{number_format((float)$lineasAlbaranes['precioiva'][4], 2, ',', '')}}</td>
                    </tr>
                @endif
                @if ($lineasAlbaranes['precioiva'][10] > 0)
                    <tr>
                        <td nowrap height="20px" style="text-align: left;"><strong>IVA 10%: </strong></td>
                        <td nowrap style="text-align: right;">{{number_format((float)$lineasAlbaranes['precioiva'][10], 2, ',', '')}}</td>
                    </tr>
                @endif
                @if ($lineasAlbaranes['precioiva'][21] > 0)
                    <tr>
                        <td nowrap height="20px" style="text-align: left;"><strong>IVA 21%: </strong></td>
                        <td nowrap style="text-align: right;">{{number_format((float)$lineasAlbaranes['precioiva'][21], 2, ',', '')}}</td>
                    </tr>
                @endif
                @if ($lineasAlbaranes['importe_impuesto'] > 0)
                    <tr>
                        <td nowrap height="20px" style="text-align: left;"><strong>Impuestos: </strong></td>
                        <td nowrap style="text-align: right;">{{number_format((float)$lineasAlbaranes['importe_impuesto'], 3, ',', '')}}</td>
                    </tr>
                @endif
                <tr style="background-color: #c0dff4; width: 120px;">
                    <td nowrap height="20px" style="text-align: left;"><strong>Total: </strong></td>
                    <td nowrap style="text-align: right;">{{$lineasAlbaranes['preciototal']}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

</body>
</html>