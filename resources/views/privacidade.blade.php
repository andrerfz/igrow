
@section('privacidade')

    <div id="privacidade" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true" style="background-color: rgba(0, 0, 0, 0.84);">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Política de Privacidade</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="kt-scroll ps" data-scroll="true" data-height="400" style="height: 400px; overflow: hidden;">

                        <div class="kt-portlet">
                            <div class="kt-portlet__body">
                                <ul class="nav nav-tabs  nav-tabs-line" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#cookies_tab">Política de Privacidade</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="cookies_tab" role="tabpanel">

                                        <p>Se você está aqui é porque você quer saber mais sobre as obrigações e direitos que lhe correspondem como um usuário deste site, Studex Brasil (Regional Acre): https://brincosstudexacre.com.br/. Nosso dever é informar e o seu é estar devidamente informado.</p>

                                        <p>Esta Política de Privacidade é para informá-lo com total transparência sobre a finalidade deste site e tudo o que afeta os dados que nos forneça, bem como as obrigações e direitos que lhe correspondem.

                                        <p>Para começar, você deve saber que este site se adapta a normativa vigente em matéria de proteção de dados, o que afeta os dados pessoais que nos forneça com o seu consentimento expresso e os cookies que utilizamos para que este site funcione corretamente e possa desenvolver sua atividade.

                                        <p>Especificamente, este site se ajusta ao cumprimento das seguintes normas:

                                        <li>
                                            <ul>Lei geral de proteção de dados pessoais (projeto de lei aprovado recentemente, baseado na lei Europeia)</ul>
                                            <ul>Lei Brasileira n.º 12.965, de 2014</ul>
                                            <ul>RGPD (Regulamento (UE) 2016/679 do Parlamento Europeu e do Conselho, de 27 de abril de 2016, relativo à proteção das pessoas físicas) que é a nova legislação da União Europeia que unifica a regulamentação do tratamento dos dados pessoais nos diferentes países da UE.</ul>
                                        </li>

                                        <p>DADOS DE IDENTIFICAÇÃO</p>

                                        <li>
                                            <ul>A seguir estão os dados do responsável titular de este site:</ul>
                                            <ul>Nome/Razão Social: Solange Fernandez Vidal</ul>
                                            <ul>CPF/CNPJ: 89999711200</ul>
                                            <ul>Endereço: Rua Gaivota II, 154. Bairro Ouricuri, Rio Branco, Acre.</ul>
                                            <ul>Atividade do site: Representação comercial</ul>
                                            <ul>E-mail: solange.fvidal@gmail.com</ul>
                                        </li>
                                        
                                        <p>Os dados pessoais que nos fornecer, sempre com o seu consentimento expresso, serão guardados e tratados para os fins previstos a seguir descritos nesta Política de Privacidade, até o momento em que nos solicite a remoção.</p>

                                        <p>Informamos que esta Política de Privacidade pode ser alterada a qualquer momento, com a finalidade de adaptá-la a novidades legislativas ou mudanças em nossas atividades, sendo vigente a cada momento a que está publicada no site. Em caso de alteração você será notificado antes da sua aplicação.</p>

                                        <p>CONDIÇÕES DE USO

                                        <p>Você deve saber, para sua tranquilidade, que sempre será solicitado o seu consentimento expresso para obter seus dados com a respectiva finalidade especificada em cada caso, o que implica que, em caso de dar esse consentimento, você leu e concorda com esta Política de Privacidade.</p>

                                        <p>No momento em que acesse e utiliza esta página web, está assumindo a sua condição de usuário com seus correspondentes direitos e obrigações.</p>

                                        <p>Se você é maior de 13 anos, poderá registar-se como utilizador deste site sem o prévio consentimento de seus pais ou tutores.
                                        Se você é menor de 13 anos, necessitará o consentimento de seus pais ou tutores para o tratamento dos seus dados pessoais.
                                        Nunca solicito informações pessoais, a menos que realmente seja necessário para prestar os serviços que ofereço nesse website.
                                        Nunca compartilho informações pessoais dos meus usuários com ninguém, exceto para cumprir com a lei ou em caso que tenha a sua autorização expressa.
                                        Nunca utilizo os seus dados pessoais com uma finalidade diferente da expressa nesta política de privacidade.</p>

                                        <p>REGISTRO E FINALIDADE DOS SEUS DADOS</p>

                                        <p>Em função do formulário ou seção que acesse será solicitado exclusivamente os dados necessários com as finalidades descritas a seguir. Em todo momento, você deve dar o seu consentimento expresso, quando lhe solicitamos informações pessoais para as seguintes finalidades:</p>

                                        <li>
                                            <ul>As finalidades concretas que particularmente são indicadas em cada uma das páginas ou seções onde aparece o formulário de registro ou contato eletrônico.</ul>
                                            <ul>Com carácter geral, para atender os seus pedidos, comentários, perguntas ou qualquer tipo de pedido que realize como pessoa através de qualquer uma das formas de contato que coloco à sua disposição.</ul>
                                            <ul>Para obter informações sobre consultas, pedidos, atividades, produtos, novidades e/ou serviços; via e-mail, fax, WhatsApp, Skype, telefone fornecido, sms e mms.</ul>
                                            <ul>Para enviar comunicações comerciais ou publicitárias através de qualquer outro meio eletrônico ou físico, que possibilite a realização de comunicações.</ul>
                                            <ul>Essas comunicações, serão sempre relacionadas com os nossos produtos, serviços, novidades e promoções, bem como aqueles produtos ou serviços que possamos considerar do seu interesse e que possam oferecer colaboradores, empresas ou "parceiros" com os quais mantemos acordos de promoção ou de colaboração comercial.</ul>
                                        </li>

                                        <p>Sendo assim, garantimos que estes terceiros nunca terão acesso aos seus dados pessoais, com as exceções indicadas abaixo, sendo em todo o caso essas comunicações realizadas por parte de Solange Fernandez Vidal, como titular da web.</p>
                                        <p>Este site mostra produtos de afiliado de terceiros.</p>
                                        <p>Isto quer dizer que, ao clicar em "Compre Agora" ou em determinados links similares, você será redirecionado para a página onde se ofertam produtos.</p>
                                        <p>Neste caso, você deve saber que somente proporcionamos e facilitamos os links para as páginas e/ou plataformas de terceiros, onde podem ser adquiridos os produtos que mostramos, no intuito de facilitar a pesquisa e fácil aquisição dos mesmos.</p>
                                        <p>Estas páginas interligadas e pertencem a terceiros, não foram revisadas nem estão sobre controle, nem recomendação por minha parte, por isso que, em nenhum caso, Solange Fernandez Vidal será considerado responsável pelo conteúdo desses sites, nem das responsabilidades derivadas da sua utilização em todos os aspectos, nem mesmo por medidas tomadas relacionadas à privacidade do usuário, tratamento de seus dados de caráter pessoal ou outras que possam ser estabelecidas.</p>
                                        <p>Por tudo isso, recomendo a leitura cuidadosa e antecipada de todas as condições de uso, condições de compra, políticas de privacidade, termos de uso e/ou similares desses sites antes de proceder à aquisição de produtos ou utilização dos sites.</p>

                                        <p>EXATIDÃO E VERACIDADE DOS DADOS</p>

                                        <p>Como usuário, você é o único responsável pela veracidade e alteração dos dados que enviar a Studex Brasil (Regional Acre), exonerando-me de qualquer responsabilidade a respeito.</p>
                                        <p>Quer dizer, a você cabe garantir e responder em qualquer caso, a exatidão, vigência e autenticidade dos dados pessoais fornecidos, e se comprometer a manter esses dados devidamente atualizados.</p>
                                        <p>De acordo ao expressado nesta Política de Privacidade, você concorda em fornecer informação completa e correta no formulário de contato ou assinatura.</p>

                                        <p>CANCELAMENTO DE INSCRIÇÃO E O DIREITO DE REVOGAÇÃO</p>

                                        <p>Como titular dos dados que nos tenha fornecido, pode exercer em qualquer momento os seus direitos de acesso, retificação, cancelamento e oposição, enviando-nos um e-mail para solange.fvidal@gmail.com anexando uma fotocópia do seu documento de identidade como prova válida.</p>
                                        <p>Para solicitar seus dados também pode fazer diretamente através desse link: https://brincosstudexacre.com.br/politica-privacidade.</p>

                                        <p>Igualmente, você poderá cancelar a qualquer momento para deixar de receber a nosso newsletter ou qualquer outra comunicação comercial, diretamente a partir do mesmo endereço de e-mail que você recebeu ou enviando-nos um e-mail para solange.fvidal@gmail.com</p>

                                        <p>ACESSO A DADOS POR PARTE DE TERCEIROS</p>

                                        <p>Para poder prestar serviços estritamente necessários para o funcionamento e desenvolvimento das atividades deste site, informamos que compartilhamos dados com os seguintes prestadores de serviço, sob as suas correspondentes condições de privacidade.</p>

                                        <p>Você pode ter a tranquilidade total de que estes terceiros não poderão utilizar essas informações para qualquer outro fim que não esteja especificamente regulado em nossas relações com eles, nos termos da legislação aplicável em matéria de proteção de dados de caráter pessoal.</p>

                                        <p>Este site está hospedado em https://www.digitalocean.com/ com a marca comercial Digital Ocean, que oferece serviços de hospedagem para que você possa acessar e navegar pelo nosso site. Pode consultar a política de privacidade e outros aspectos legais dessa empresa no seguinte link: https://www.digitalocean.com/legal/.</p>

                                        <p>Nosso site utiliza servidores publicitários com o fim de facilitar os conteúdos comerciais que verá em nossas páginas. Tais serviços publicitários utilizam cookies, que permitem adaptar os conteúdos publicitários para os perfis demográficos dos utilizadores:</p>

                                        <p>Redes Sociais</p>

                                        <p>As cookies de redes sociais podem ser armazenadas no seu navegador enquanto você navega pelo blog, por exemplo, quando você usa o botão de compartilhar um artigo do Studex Brasil (Regional Acre) em alguma rede social.</p>
                                        <p>As implicações de privacidade serão em função de cada rede social e dependem da configuração de privacidade que tenha nessa rede. Em nenhum caso, nem eu Solange Fernandez Vidal como o responsável por este blog, nem os anunciantes, podemos obter informações de identificação pessoal de cookies.</p>
                                        <p>Para saber mais sobre as cookies de redes sociais visite nossa Política De Cookies.</p>

                                        <p>Google Analytics:</p>

                                        <p>O Google Analytics é um serviço de análise web fornecido pela Google, Inc., uma companhia de Delaware cujo escritório principal está em 1600 Amphitheatre Parkway, Mountain View (California), CA 94043, Estados Unidos ("Google").
                                        Google Analytics utiliza "cookies", que são arquivos de texto localizados no seu computador, para ajudar o website a analisar o uso que os usuários fazem do site.
                                        A informação gerada pelo cookie sobre a sua utilização do website (incluindo o seu endereço IP) será diretamente transmitida e arquivada por Google. Google usará esta informação por minha conta, com o propósito de seguir a pista do seu uso do website, compilando relatórios sobre a atividade do website e fornecendo outros serviços relativos a atividade do website e utilização da Internet.
                                        Google pode igualmente transferir esta informação para terceiros, sempre que exigido por lei ou quando estes terceiros processem a informação através do Google. O Google não associará o seu endereço IP com qualquer outro dado mantido.</p>

                                        <p>Google Adsense:

                                        <p>O Google Adsense utiliza cookies para exibir anúncios personalizados neste site. Você pode desativar o uso do cookie DART através do anúncio do Google ou acessando diretamente este link https://adssettings.google.com/authenticated.
                                        Usamos empresas de publicidade de terceiros para veicular anúncios quando visita o nosso website. É possível que estas empresas utilizem a informação que recebem de suas visitas a este e a outros websites (sem incluir o seu nome, endereço, endereço de e-mail ou número de telefone) para lhe fornecer anúncios sobre produtos e serviços que lhe são de interesse.
                                        Ao usar este site, você concorda com o processamento de dados pelo Google na forma e para os fins indicados.
                                        Se quiser saber mais sobre o uso de cookies e sobre as práticas de recolha de informação e os processos de aceitação ou rejeição, consulte a nossa Política De Cookies.</p>

                                        <p>E-Mails Comerciais</p>

                                        <p>Eu Solange Fernandez Vidal quero lhe confirmar que não realizo práticas de SPAM, quer dizer que não envio e-mails comerciais que não foram devidamente identificados, solicitados e permitidos pelo Usuário.
                                        Em cada um dos formulários que existem no blog, o usuário tem a possibilidade de dar ou não seu consentimento expresso para receber minha Newsletter.</p>

                                        <p>MEDIDAS DE SEGURANÇA</p>

                                        <p>Como titular do site, Solange Fernandez Vidal adotou todas as medidas técnicas e organizacionais necessárias para garantir a segurança e integridade dos dados de carácter pessoal que trate, bem como para evitar a sua alteração, perda, e/ou acesso por parte de terceiros não autorizados.
                                        Este blog inclui um certificado SSL, trata-se de um protocolo de segurança que faz com que seus dados viajem de forma íntegra e segura, ou seja, a transmissão de dados entre um servidor e um usuário da web é totalmente criptografada ou encriptada.
                                        De todas as formas, deve estar ciente de que as medidas de segurança dos sistemas informáticos na Internet não são inteiramente confiáveis, por isso no site https://brincosstudexacre.com.br/, também não posso garantir completamente a inexistência de vírus ou outros elementos que possam produzir alterações nos sistemas informáticos (software e hardware) do Utilizador ou em seus documentos eletrônicos e arquivos que contém.
                                        Em caso de algum problema de fuga de dados me comprometo a avisar imediatamente as autoridades e a você leitor.</p>

                                        <p>Para mais informações pode consultar a nossa <a href="#" id="open_cookies"><span>Política de Cookies</span></a>.<p>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px; height: 200px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 72px;"></div></div></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    @if (Cookie::get('studex_acre_cookie_consent') == null)
                        <button type="button" class="btn btn-warning cookie-consent__modal">Aceitar</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection