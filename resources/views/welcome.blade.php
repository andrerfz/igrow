@include('cookieConsent::index')

@include('cookie')
@include('privacidade')

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

    <head>
        <meta charset="utf-8"/>
        <title>{{ config('app.name', 'Studex Acre') }}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--begin:: Global Mandatory Vendors -->
        <link href="{{ asset('assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet" type="text/css"/>
        <!--end:: Global Mandatory Vendors -->

        <link href="{{ asset('assets/vendors/MiApp/bootstrap/css/bootstrap.min.css') }}"  rel='stylesheet' type='text/css'>
        <link href="{{ asset('assets/vendors/general/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/vendors/MiApp/css/ionicons.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/vendors/MiApp/css/owl.carousel.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/vendors/MiApp/css/magnific-popup.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/vendors/general/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/vendors/custom/vendors/line-awesome/css/line-awesome.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/vendors/general/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css"/>

        <!--begin::Global Theme Styles(used by all pages) -->
        <link href="{{ asset('assets/css/demo1/style.bundle.css') }}" rel="stylesheet" type="text/css"/>
        <!--end::Global Theme Styles -->

        <!--begin::Local Theme Style -->
        <link href="{{ asset('assets/vendors/MiApp/css/style.css') }}?v=00004" rel="stylesheet" type="text/css"/>
        <!--end::Local Theme Styles -->

    </head>

    <body>


        @yield('cookie')

        @yield('privacidade')


        <div class="page-borders">
            <div class="left"></div>
            <div class="right"></div>
        </div>


        <a href="#" class="menu-btn">
            <span class="lines">
                <span class="l1"></span>
                <span class="l2"></span>
                <span class="l3"></span>
            </span>
        </a>


        <nav class="menu">
            <ul>
                <li>
                    <a href="#inicio">
                        Início
                    </a>
                </li>
                <li>
                    <a href="#classicos">
                        Clássicos
                    </a>
                </li>
                <li>
                    <a href="#caracteristica-1">
                        Características
                    </a>
                </li>
                <li>
                    <a href="#novidades">
                        Novidades
                    </a>
                </li>
                <li>
                    <a href="#faq">
                        Dúvidas?
                    </a>
                </li>
                <li>
                    <a href="#contato">
                        Contate-nos
                    </a>
                </li>
                @guest
                    <li>
                        <a> | </a>
                    </li>
                    <li>
                        <a href="{{ route('login', app()->getLocale()) }}">Minha conta</a>
                    </li>
                @endguest
            </ul>
        </nav>


        <div id="preloader">
            <div class="loader">
                <img src="{{url('assets/vendors/MiApp/img/loader.gif')}}" alt>
            </div>
        </div>


        <div id="main-wrapper">


            <section id="inicio" class="section intro-section">
                <div class="container">
                    <div class="row intro-cols">
                        <div class="col-md-6 col-md-push-6">
                            <div class="phone">
                                <div class="phone-img">
                                    <img src="{{url('img/display_216.jpg')}}" alt>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-md-pull-6">
                            <div class="v-align">
                                <div class="inner">
                                    <div class="intro-text">
                                        <h1>
                                            Studex
                                        </h1>
                                        <h2>
                                            Regional Acre
                                        </h2>
                                        <div style="margin-top: 55px; margin-bottom: 55px;">
                                            <p>
                                                A Studex é a maior fabricante de semi-jóias para perfuração e lider en perfuração de orelhas do mundo!
                                            </p>
                                            <p>
                                                Fabricado e desenvolvido em Los Angeles, California (EUA).
                                            </p>
                                            <p>
                                                Somos representante da região norte do Brasil, atuando em todo o estado do Acre há mais de 15 anos.
                                            </p>
                                        </div>
                                        <p><i class="fab fa-whatsapp-square" aria-hidden="true"> (68) - 99971-1308 </i> <i class="fa fa-mobile" aria-hidden="true"> (68) - 99971-1308 </i> <i class="fa fa-phone" aria-hidden="true"> (68) - 3228-6411 </i><p>
                                        <p><i class="fab fa-weixin" aria-hidden="true"> Solange F. Vidal</i> </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section id="classicos" class="section features-section">
                <div class="container">
                    <div class="phone-img">
                        <img src="{{url('img/studex_classic.jpg')}}" alt>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="features-text">
                                <h2>Os clássicos que amamos</h2>
                                <p>
                                    Um clássico que não sai de moda!
                                </p>
                                <p>
                                    Nossa linha de brincos classic sempre disponível em sua farmácia mais próxima.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row features-row">
                        <div class="col-md-4 col-sm-6">
                            <div class="feature">
                                <div class="icon">
                                    <i class="ion-ios-heart-outline"></i>
                                </div>
                                <div class="content">
                                    <h4>Os mais pedidos</h4>
                                    <p>
                                        Studex classic é a receita de sucesso. Venda garantida e alta procura, a linha classic é garantia de sucesso em sua farmácia.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="feature">
                                <div class="icon">
                                    <i class="ion-ios-loop"></i>
                                </div>
                                <div class="content">
                                    <h4>Sempre temos novidades</h4>
                                    <p>
                                        Há 20 anos se reinventando, procuramos evoluir nosso design sem perder a toque clássico. Sempre tem novidade te esperando.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="feature">
                                <div class="icon">
                                    <i class="ion-ios-pie-outline"></i>
                                </div>
                                <div class="content">
                                    <h4>Melhores vendas</h4>
                                    <p>
                                        Venda garantida e margem imbatível, garanttimos uma ótima receita com nossos produtos classic.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section id="caracteristica-1" class="section features-2-section bg-lightgray">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="phones order-alt">
                                <img class="front" src="{{url('img/system_75.jpg')}}" alt>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="screen-info-text">
                                <h2>Ainda mais poderoso</h2>
                                <p>Apresentamos <strong><span style="font-size: 24px;">System 75</span></strong>, nossa linha de produtos alta gama. Com tecnologia exclusiva de aplicação manual, sem necessidade de instrumentos. Totalmente descartável.</p>
                                <a href="#video" class="btn-minimal">
                                    Ver mais sobre
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section id="features-3" class="section features-4-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 features-col text-right">
                            <div class="col-feature">
                                <div class="icon">
                                    <i class="ion-ios-heart-outline"></i>
                                </div>
                                <div class="content">
                                    <h4>Design apaixonante!</h4>
                                    <p>
                                        Os 100% aço cirurgico que nunca saem de moda!
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="features-phone">
                                <img style="height: 500px" src="{{url('img/display_system_75.jpg')}}" alt>
                            </div>
                        </div>
                        <div class="col-md-4 features-col">
                            <div class="col-feature">
                                <div class="icon">
                                    <i class="ion-ios-color-wand-outline"></i>
                                </div>
                                <div class="content">
                                    <h4>Funciona como mágica</h4>
                                    <p>
                                        O sistema mais modeno, fácil e seguro de aplicar.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section id="caracteristica-2" class="section features-2-section bg-lightgray">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-push-6">
                            <div class="phones">
                                <img class="back2" src="{{url('img/instruments_S75.jpg')}}" alt>
                                <img class="back2" src="{{url('img/instruments_system_75_multi.jpg')}}" alt>
                                <img class="back2" src="{{url('img/instruments_universal_general.jpg')}}" alt>
                                <img class="back2" src="{{url('img/instruments_universal.jpg')}}" alt>
                            </div>
                        </div>
                        <div class="col-md-6 col-md-pull-6">
                            <div class="screen-info-text">
                                <h2>Simples & Estéril</h2>
                                <p>O melhor sistema já criado, dispensa qualquer contato e manipulação da jóia durante a aplicação.</p>
                                <a href="#video" class="btn-minimal">
                                    Ver mais sobre
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section id="novidades" class="section screenshots-section">
                <div class="container">
                    <div class="top-section-header">
                        <h2>Novidades</h2>
                        <p>Conheça nossos novos produtos. Sempre uma novidade para você.</p>
                        <p>Você pode mandar uma <strong><a href="#contato" style="text-decoration : none; color : #000000;">menssagem</a></strong> perguntando por eles.</p>
                    </div>
                    <ul class="screenshots-slider">
                        <li>
                            <div class="inner">
                                <img src="{{url('img/treinamentos/treinamento_1.jpg')}}" alt>
                                <div class="overlay">
                                    <a href="{{url('img/treinamentos/treinamento_1.jpg')}}" class="view-btn">
                                        <i class="ion-ios-plus-empty"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="inner">
                                <img src="{{url('img/novidades/novidades_1.jpg')}}" alt>
                                <div class="overlay">
                                    <a href="{{url('img/novidades/novidades_1.jpg')}}" class="view-btn">
                                        <i class="ion-ios-plus-empty"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="inner">
                                <img src="{{url('img/novidades/novidades_2.jpg')}}" alt>
                                <div class="overlay">
                                    <a href="{{url('img/novidades/novidades_2.jpg')}}" class="view-btn">
                                        <i class="ion-ios-plus-empty"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="inner">
                                <img src="{{url('img/novidades/novidades_3.jpg')}}" alt>
                                <div class="overlay">
                                    <a href="{{url('img/novidades/novidades_3.jpg')}}" class="view-btn">
                                        <i class="ion-ios-plus-empty"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="inner">
                                <img src="{{url('img/novidades/novidades_4.jpg')}}" alt>
                                <div class="overlay">
                                    <a href="{{url('img/novidades/novidades_4.jpg')}}" class="view-btn">
                                        <i class="ion-ios-plus-empty"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="inner">
                                <img src="{{url('img/novidades/novidades_5.jpg')}}" alt>
                                <div class="overlay">
                                    <a href="{{url('img/novidades/novidades_5.jpg')}}" class="view-btn">
                                        <i class="ion-ios-plus-empty"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="inner">
                                <img src="{{url('img/novidades/novidades_6.jpg')}}" alt>
                                <div class="overlay">
                                    <a href="{{url('img/novidades/novidades_6.jpg')}}" class="view-btn">
                                        <i class="ion-ios-plus-empty"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="inner">
                                <img src="{{url('img/novidades/novidades_7.jpg')}}" alt>
                                <div class="overlay">
                                    <a href="{{url('img/novidades/novidades_7.jpg')}}" class="view-btn">
                                        <i class="ion-ios-plus-empty"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="inner">
                                <img src="{{url('img/novidades/novidades_8.jpg')}}" alt>
                                <div class="overlay">
                                    <a href="{{url('img/novidades/novidades_8.jpg')}}" class="view-btn">
                                        <i class="ion-ios-plus-empty"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="inner">
                                <img src="{{url('img/novidades/novidades_9.jpg')}}" alt>
                                <div class="overlay">
                                    <a href="{{url('img/novidades/novidades_9.jpg')}}" class="view-btn">
                                        <i class="ion-ios-plus-empty"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="inner">
                                <img src="{{url('img/novidades/novidades_10.jpg')}}" alt>
                                <div class="overlay">
                                    <a href="{{url('img/novidades/novidades_10.jpg')}}" class="view-btn">
                                        <i class="ion-ios-plus-empty"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="inner">
                                <img src="{{url('img/novidades/novidades_11.jpg')}}" alt>
                                <div class="overlay">
                                    <a href="{{url('img/novidades/novidades_11.jpg')}}" class="view-btn">
                                        <i class="ion-ios-plus-empty"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="inner">
                                <img src="{{url('img/novidades/novidades_12.jpg')}}" alt>
                                <div class="overlay">
                                    <a href="{{url('img/novidades/novidades_12.jpg')}}" class="view-btn">
                                        <i class="ion-ios-plus-empty"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>


            <section id='video' class='section video-section bg-lightgray' >
                <div class='container' >
                    <div class='row' >
                        <div class='col-md-4' >
                            <div class='col-section-header' >
                                <h2>Aprenda mais sobre nosso sistema com esse vídeo instrutivo</h2>
                                <p>
                                    Simples, fácil e seguro.
                                </p>
                            </div>
                        </div>
                        <div class='col-md-8' >
                            <div class='video-container' >
                                <iframe src="https://www.youtube.com/embed/yUk4OinJ-Ak?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section id="pricing" class="section pricing-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="col-section-header">
                                <h2>Escolha a linha de produtos que mais combina com seu seu perfil</h2>
                                <p>
                                    Em breve poderá fazer compras online. No momento estamos atendendo por whatsapp e por telefone.
                                    Contacto-nos.
                                </p>
                                <a href="#contato" class="btn-minimal">
                                    Entre em contato
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="p-table">
                                <div class="header">
                                    <h4>Clássicos</h4>
                                    <div class="produtos">
                                        <img src="{{url('img/display_216.jpg')}}" alt>
                                    </div>
                                </div>
                                <a href="" class="btn-minimal">
                                    Fazer Login
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="p-table">
                                <div class="header">
                                    <h4>System 75</h4>
                                    <div class="produtos">
                                        <img src="{{url('img/display_system_75.jpg')}}" alt>
                                    </div>
                                    <div class="price">
                                    </div>
                                </div>
                                <a href="" class="btn-minimal">
                                    Fazer Login
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section id="clients" class="section clients-section bg-lightgray">
                <div class="container">
                    <div class="top-section-header">
                        <h2>Nossos Clientes</h2>
                        <p>Dê uma olhada em nossos clientes e veja o que eles tem a dizer de nossos produtos.</p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="clients">
                                <li>
                                    <img src="{{url('img/clientes/drograria_londrina.jpg')}}" alt>
                                </li>
                                <li>
                                    <img src="{{url('img/clientes/recol_farma.png')}}" alt>
                                </li>
                                <li>
                                    <img src="{{url('img/clientes/drogarias_araujo.jpg')}}" alt>
                                </li>
                                <li>
                                    <img src="{{url('img/clientes/recol_farma.png')}}" alt>
                                </li>
                                <li>
                                    <img src="{{url('img/clientes/drogarias_araujo.jpg')}}" alt>
                                </li>
                                <li>
                                    <img src="{{url('img/clientes/drograria_londrina.jpg')}}" alt>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <div class="testimonials-slider">
                                <div class="testimonial">
                                    <div class="icon">
                                        <i class="ion-quote"></i>
                                    </div>
                                    <div class="content">
                                        <p>
                                            Os brincos Studex são venda certa e ademas, não há nada igual a Studex System 75.
                                        </p>
                                        <p>
                                            Solange é muito atenciosa e pontual, sempre oferendo os melhores preços e serviços pós venda.
                                        </p>
                                    </div>
                                    <div class="author">
                                        <h4>José</h4>
                                        <span>Gerente @ Drograrias Ceará</span>
                                    </div>
                                </div>
                                <div class="testimonial">
                                    <div class="icon">
                                        <i class="ion-quote"></i>
                                    </div>
                                    <div class="content">
                                        <p>
                                            Solange é nossa parceira há mais de uma decada, oferendo o melhor suporte pós venda.
                                        </p>
                                        <p>
                                            Produto da melhor qualidade, de venda imbatível, com a melhor segurança em aplicação.
                                        </p>
                                    </div>
                                    <div class="author">
                                        <h4>Maria</h4>
                                        <span>Gerente @ Drograrias Globo</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section id="faq" class="section faq-section">
                <div class="container">
                    <div class="top-section-header">
                        <h2>Perguntas mais frequentes</h2>
                        <p>As mais frequentes dúvidas estão aqui.</p>
                    </div>
                    <div class="col-md-6">
                        <div class="faq">
                            <h4>É permitido furar orelhas em farmácias e drogarias?</h4>
                            <p>
                                SIM. De acordo com as normas da ANVISA, RDC nº 44, seção II e III, e artigos 78 a 81, as Farmácias e Drogarias estão autorizadas a perfurar Lóbulos Auriculares para colocação de brincos desde que seja feito com aparelho específico para esse fim e utilize o próprio brinco como material perfurante, como estabelecido na referida norma.
                            </p>
                        </div>
                        <div class="faq">
                            <h4>A Studex tem Cadastro na Anvisa dos produtos vendidos no Mercado?</h4>
                            <p>
                                SIM. Todos os nossos produtos atendem as Leis Federais de Saúde, Nacionais e Internacionais, estando devidamente regulamentados na ANVISA, FDA (FOOD & DRUG ADMINISRATION) e E.C (EUROPEAN CONFEDERATION).
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="faq">
                            <h4>Posso permanecer utilizando os brincos Studex após a cicatrização dos Lóbulos Auriculares?</h4>
                            <p>
                                SIM. Os brincos Studex são fabricados em Aço Inox Cirúrgicos que passam por um processo de esterilização por óxido de etileno que garante a máxima higiene e proteção. Além disso, nossos brincos são livres de níquel, o que reforça seu caráter Antialégico, podendo ser utilizado como uma semi-jóia.
                            </p>
                        </div>
                        <div class="faq">
                            <h4>Qual o tempo necessário para permanecer com o brinco Studex após a perfuração do Lóbulo Auricular?</h4>
                            <p>
                                No mínimo 6 (seis) semanas, de preferências até certificar-se que os lóbulos estejam completamente cicatrizados, evitando dessa forma o risco de inflamação na parte em que ocorreu a perfuração.
                            </p>
                        </div>
                    </div>
                </div>
            </section>


            <section id="subscribe" class="section subscribe-section" style="background-color: black;">
                <div class="container">
                    <div class="section-header">
                        <h2>Inscreva-se para receber novidades</h2>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="subscribe-form">
                                <input type="text" class="subscribe-input" placeholder="Seu E-mail" title="Seu E-mail" name="subscribe-input">
                                <a href="#" class="subscribe-btn">Inscreva-se</a>
                            </div>
                            <p class="hate-spam">Não se preocupe, não compartilhamos suas informações pessoais com anunciantes ou parceiros.</p>
                        </div>
                    </div>
                </div>
            </section>


            <section id="contato" class="section contact-section">
                <div class="container">
                    <div class="top-section-header">
                        <h2>Contate-nos</h2>
                        <p>Sinta-se a vontade para nos contactar e retirar quaisquer dúvidas</p>
                    </div>
                    <form class="contact-form" id="contact-form" data-toggle="validator" method="post" action="mail.php">
                        <div id="contact-form-result"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Nome" name="nome" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email" name="email" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Assunto" name="assunto" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Menssagem" name="menssagem" required></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group text-center">
                                    <button class="btn-minimal btn-block" type="submit">Enviar mensagem</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>

        <footer>
            <div class='container text-center' >
                <p>{{ date('Y') }}&nbsp;&copy;&nbsp; Brincos Studex | Versão {{trim(exec('git log --pretty="%h" -n1 HEAD'))}}</p>
                <p><a href="#" id="open_cookies"><span>Política de Cookies</span></a>&nbsp;&nbsp;&nbsp;<a href="#" id="open_privacidade"><span>Política de Privacidade</span></a></p>
            </div>
        </footer>

        </div>

        <script src="{{ asset('assets/vendors/MiApp/js/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendors/general/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendors/general/sticky-js/dist/sticky.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendors/MiApp/js/jquery.fitvids.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendors/MiApp/js/owl.carousel.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendors/MiApp/js/jquery.magnific-popup.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendors/MiApp/js/validator.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendors/general/toastr/build/toastr.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendors/MiApp/js/script.js') }}" type="text/javascript"></script>

        <!--begin::Global Theme Bundle(used by all pages) -->
        <script src="{{ asset('assets/js/demo1/scripts.bundle.js') }}" type="text/javascript"></script>
        <!--end::Global Theme Bundle -->

        <script>


            var KTAppOptions = {
                "colors": {
                    "state": {
                        "brand": "#5d78ff",
                        "dark": "#282a3c",
                        "light": "#ffffff",
                        "primary": "#5867dd",
                        "success": "#34bfa3",
                        "info": "#36a3f7",
                        "warning": "#ffb822",
                        "danger": "#fd3995"
                    },
                    "base": {
                        "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                        "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                    }
                }
            };


            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "showDuration": "1",
                "hideDuration": "1",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };


            $(document).ready( function() {

                $.fn.modal.Constructor.Default.backdrop = false; // changes default for the modal plugin's `backdrop` option to false
                $.fn.modal.Constructor.Default.keyboard = false; // changes default for the modal plugin's `keyboard` option to false

                $(document).on('click', '#open_cookies', function () {

                    $('#cookies').modal('show');
                    $('#privacidade').modal('hide');
                });

                $(document).on('click', '#open_privacidade', function () {

                    $('#privacidade').modal('show');
                    $('#cookies').modal('hide');
                });

                $('.cookie-consent__modal').on('click', function () {

                    laravelCookieConsent.consentWithCookies();

                    $('#cookies').modal('hide');
                    $('#privacidade').modal('hide');
                })

            });

        </script>

    </body>
</html>