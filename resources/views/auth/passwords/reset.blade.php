@extends('layouts.app')

@section('extra_meta')

    <!--begin::Page Custom Styles(used by this page) -->
    <link href="{{ asset('assets/css/demo1/pages/login/login-5.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Page Custom Styles -->

@endsection

@section('content')


    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--ver kt-grid--root">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v5 kt-login--signin" id="kt_login">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile" style="background-image: url( {{ url('assets/media//bg/bg-3.jpg') }} );">
                <div class="kt-login__left">
                    <div class="kt-login__wrapper">
                        <div class="kt-login__content">
                            <a class="kt-login__logo" href="#">
                                <img src="{{ url('img/logo.png') }}">
                            </a>
                            <h3 class="kt-login__title">Painel Administrativo </h3>
                            <span class="kt-login__desc">Bem vindo, crie uma conta ou acesse a sua se já possui.</span>
                            <div class="kt-login__actions">
                                @if (Route::has('register'))
                                    <button href="{{ route('register', app()->getLocale()) }}" type="button" id="kt_login_signup" class="btn btn-outline-brand btn-pill">Criar uma conta</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-login__divider">
                    <div></div>
                </div>
                <div class="kt-login__right">
                    <div class="kt-login__wrapper">
                        <div class="kt-login__signin">
                            <div class="kt-login__head">
                                <h3 class="kt-login__title">Redefinir sua senha</h3>
                            </div>
                            <div class="kt-login__form">
                                <form class="kt-form" method="POST" action="{{ route('password.update', app()->getLocale()) }}">
                                    @csrf

                                    <input type="hidden" name="token" value="{{ $token }}">

                                    <div class="form-group">

                                        <input class="form-control @error('email') is-invalid @enderror" type="email" name="email" value="{{ $email ?? old('email') }}" placeholder="E-mail" required autocomplete="email" autofocus>

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                    </div>
                                    <div class="form-group">

                                        <input class="form-control @error('password') is-invalid @enderror" type="password" placeholder="Senha" name="password" required autocomplete="new-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                        <input class="form-control" name="password_confirmation" type="password" placeholder="Confirmar senha" required autocomplete="new-password">

                                    </div>
                                    <div class="kt-login__actions">
                                        <button id="kt_login_signin_submit" type="submit" class="btn btn-brand btn-pill btn-elevate">Redefinir senha
                                            @if(false){{ __('Reset Password') }}@endif
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- end:: Page -->

    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                    "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                }
            }
        };
    </script>


@endsection

@section('extra_js')

    <!--begin::Page Scripts(used by this page) -->
    <script src="{{ asset('assets/js/demo1/pages/login/login-general.js') }}" type="text/javascript"></script>

@endsection
