@extends('layouts.app')

@section('extra_meta')

    <link href="{{ asset('assets/css/demo1/pages/login/login-5.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .kt-header--fixed_1 {
            top: 0 !important;
            right: 0 !important;
            left: initial !important;
            float: right !important;
            position: absolute !important;
            border-bottom: none !important;
        }
        .kt-login.kt-login--v5 .kt-login__right .kt-login__wrapper .kt-login__extra .kt-checkbox {
            font-size: 0.7rem !important;
        }
    </style>

@endsection

@section('content')


    <!-- begin:: Page -->
    <div class="kt-header kt-grid__item kt-header--fixed_1">
        <div class="kt-header__topbar">
            <div class="kt-header__topbar-item kt-header__topbar-item--langs">
                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                    <span class="kt-header__topbar-icon">
                        <img class="" src="{{Config::get('app.locale') == 'en' ? asset('assets/media/flags/020-flag.svg') : (Config::get('app.locale') == 'es' ? asset('assets/media/flags/016-spain.svg') : asset('assets/media/flags/011-brazil.svg'))}}" alt="" />
                    </span>
                </div>
                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround">
                    <ul class="kt-nav kt-margin-t-10 kt-margin-b-10">
                        <li class="kt-nav__item @if(Config::get('app.locale') == 'en') kt-nav__item--active @endif">
                            <a href="/en/register" class="kt-nav__link">
                                <span class="kt-nav__link-icon">
                                    <img src="{{asset('assets/media/flags/020-flag.svg')}}" alt="" />
                                </span>
                                <span class="kt-nav__link-text">USA</span>
                            </a>
                        </li>
                        <li class="kt-nav__item @if(Config::get('app.locale') == 'es') kt-nav__item--active @endif">
                            <a href="/es/register" class="kt-nav__link">
                                <span class="kt-nav__link-icon">
                                    <img src="{{asset('assets/media/flags/016-spain.svg')}}" alt="" />
                                </span>
                                <span class="kt-nav__link-text">Spain</span>
                            </a>
                        </li>
                        <li class="kt-nav__item @if(Config::get('app.locale') == 'pt-BR') kt-nav__item--active @endif">
                            <a href="/pt-BR/register" class="kt-nav__link">
                                <span class="kt-nav__link-icon">
                                    <img src="{{asset('assets/media/flags/011-brazil.svg')}}" alt="" />
                                </span>
                                <span class="kt-nav__link-text">Brasil</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-grid kt-grid--ver kt-grid--root">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v5 kt-login--signin" id="kt_login">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile" style="background-image: url( {{ url('assets/media//bg/bg-3.jpg') }} );">
                <div class="kt-login__left">
                    <div class="kt-login__wrapper">
                        <div class="kt-login__content col-lg-12 col-md-12 sol-sm-12">
                            <a class="kt-login__logo" href="{{url('/')}}">
                                <img src="{{ url('img/logo.png') }}">
                            </a>
                            <h3 class="kt-login__title">@if(Config::get('app.locale') == 'en') Administration Panel @elseif(Config::get('app.locale') == 'es') Panel de control @elseif(Config::get('app.locale') == 'pt-BR') Painel Administrativo @endif </h3>
                        </div>
                    </div>
                </div>
                <div class="kt-login__divider">
                    <div></div>
                </div>
                <div class="kt-login__right">
                    <div class="kt-login__wrapper">
                        <div class="kt-login__signin">
                            <div class="kt-login__head">
                                <h3 class="kt-login__title">@if(Config::get('app.locale') == 'en') Sign Up @elseif(Config::get('app.locale') == 'es') Registrarte @elseif(Config::get('app.locale') == 'pt-BR') Abra uma conta @endif </h3>
                                <div class="kt-login__desc">@if(Config::get('app.locale') == 'en') It’s quick and easy. @elseif(Config::get('app.locale') == 'es') Es rápido y fácil. @elseif(Config::get('app.locale') == 'pt-BR') É rápido e fácil. @endif</div>
                            </div>
                            <div class="kt-login__form">
                                <form class="kt-form" method="POST" action="{{ route('register', app()->getLocale()) }}">
                                        @csrf
                                    <h4 class="kt-login__title" style="font-size: 1.2rem !important; text-align: left;">@if(Config::get('app.locale') == 'en') User: @elseif(Config::get('app.locale') == 'es') Usuario: @elseif(Config::get('app.locale') == 'pt-BR') Usuário: @endif</h4>
                                    <div class="form-group">

                                        <input class="form-control @error('name') is-invalid @enderror" type="text" placeholder="{{ __('Name') }}" name="name" value="{{ old('name') }}" autofocus autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                        <input class="form-control @error('last_name') is-invalid @enderror" type="text" placeholder="{{ __('Last Name') }}" name="last_name" value="{{ old('last_name') }}" autofocus autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
                                        @error('last_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                        <input class="form-control @error('email') is-invalid @enderror" type="email" placeholder="{{ __('E-Mail Address') }}" name="email" value="{{ old('email') }}" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                        <input class="form-control @error('password') is-invalid @enderror" type="password" placeholder="{{ __('Password') }}" name="password" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                        <input class="form-control" name="password_confirmation" type="password"  placeholder="{{ __('Confirm Password') }}" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">

                                    </div>

                                    <h4 class="kt-login__title" style="font-size: 1.2rem !important; text-align: left; margin-top: 50px;">@if(Config::get('app.locale') == 'en') Company: @elseif(Config::get('app.locale') == 'es') Empresa: @elseif(Config::get('app.locale') == 'pt-BR') Empresa: @endif </h4>

                                    <div class="form-group">

                                        <input class="form-control @error('company_register') is-invalid @enderror" type="text" placeholder="@if(Config::get('app.locale') == 'en') Company Tax Number @elseif(Config::get('app.locale') == 'es') CIF @elseif(Config::get('app.locale') == 'pt-BR') CNPJ @endif" name="company_register" value="{{ old('company_register') }}">
                                        @error('company_register')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                        <input class="form-control @error('company_name') is-invalid @enderror" type="text" placeholder="@if(Config::get('app.locale') == 'en') Company name @elseif(Config::get('app.locale') == 'es') Nombre comercial @elseif(Config::get('app.locale') == 'pt-BR') Nombre da empresa @endif" name="company_name" value="{{ old('company_name') }}">
                                        @error('company_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                        <input class="form-control @error('phone') is-invalid @enderror" type="text" placeholder="@if(Config::get('app.locale') == 'en') Phone @elseif(Config::get('app.locale') == 'es') Telefono @elseif(Config::get('app.locale') == 'pt-BR') Telefone @endif" name="phone" value="{{ old('phone') }}">
                                        @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                        <input class="form-control @error('town') is-invalid @enderror" type="text" placeholder="@if(Config::get('app.locale') == 'en') Town @elseif(Config::get('app.locale') == 'es') Municipio @elseif(Config::get('app.locale') == 'pt-BR') Município @endif" name="town" value="{{ old('town') }}">
                                        @error('town')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                        <div class="row kt-login__extra">
                                            <div class="col kt-align-left">
                                                <label class="kt-checkbox">
                                                    <input type="checkbox" name="terms"> {{ __('Terms') }}
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                        @error('terms')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror

                                        <div class="row kt-login__extra">
                                            <div class="col kt-align-left">
                                                <label class="kt-checkbox">
                                                    <input type="checkbox" name="privacy"> {{ __('Privacy Policy') }}
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                        @error('privacy')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                                    <div class="kt-login__actions">
                                        <button id="kt_login_signin_submit" type="submit" class="btn btn-brand btn-pill btn-elevate">{{ __('Register') }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- end:: Page -->

    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                    "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                }
            }
        };
    </script>


@endsection

@section('extra_js')

    <!--begin::Page Scripts(used by this page) -->
    <script src="{{ asset('assets/js/demo1/pages/login/login-general.js') }}" type="text/javascript"></script>

@endsection