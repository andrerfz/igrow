
<!-- begin::Page loader -->
<div class="kt-page-loader kt-page-loader--logo">
    <img alt="Logo" src="{{ url('img/logo.png') }}"/>
    <div class="kt-spinner kt-spinner--danger"></div>
</div>
<!-- end::Page Loader -->
