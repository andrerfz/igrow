<ul class="kt-nav kt-margin-t-10 kt-margin-b-10">
    <li class="kt-nav__item @if(Config::get('app.locale') == 'en') kt-nav__item--active @endif">
        <a href="#" class="kt-nav__link">
            <span class="kt-nav__link-icon">
                <img src="{{asset('assets/media/flags/020-flag.svg')}}" alt="" />
            </span>
            <span class="kt-nav__link-text">English</span> 
        </a>
    </li>
    <li class="kt-nav__item @if(Config::get('app.locale') == 'es') kt-nav__item--active @endif">
        <a href="#" class="kt-nav__link">
            <span class="kt-nav__link-icon">
                <img src="{{asset('assets/media/flags/016-spain.svg')}}" alt="" />
            </span>
            <span class="kt-nav__link-text">Spanish</span> 
        </a>
    </li>
    <li class="kt-nav__item @if(Config::get('app.locale') == 'pt-BR') kt-nav__item--active @endif">
        <a href="#" class="kt-nav__link">
            <span class="kt-nav__link-icon">
                <img src="{{asset('assets/media/flags/011-brazil.svg')}}" alt="" />
            </span>
            <span class="kt-nav__link-text">Português</span>
        </a>
    </li>
</ul>