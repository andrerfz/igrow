
<!-- begin:: Header Topbar -->
<div class="kt-header__topbar">

    @include('layouts.partials.topbar-search')
    @include('layouts.partials.topbar-notifications')
    @include('layouts.partials.topbar-quick-actions')
    @include('layouts.partials.topbar-my-cart')
    @include('layouts.partials.topbar-quick-panel')
    @include('layouts.partials.topbar-languages')
    @include('layouts.partials.topbar-user')
    
</div>
<!-- end:: Header Topbar -->