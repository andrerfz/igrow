@extends('layouts.app')

@section('extra_meta')

    <!--begin::Page Custom Styles(used by this page) -->
    <link href="{{ asset('assets/css/demo1/pages/login/login-5.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Page Custom Styles -->

@endsection

@section('content')


    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--ver kt-grid--root">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v5 kt-login--signin" id="kt_login">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile" style="background-image: url( {{ url('assets/media//bg/bg-3.jpg') }} );">
                <div class="kt-login__left">
                    <div class="kt-login__wrapper">
                        <div class="kt-login__content col-lg-12 col-md-12 sol-sm-12">
                            <a class="kt-login__logo" href="#">
                                <img src="{{ url('img/logo.png') }}">
                            </a>
                            <h3 class="kt-login__title">@if(Config::get('app.locale') == 'en') Administration Panel @elseif(Config::get('app.locale') == 'es') Panel de control @elseif(Config::get('app.locale') == 'pt-BR') Painel Administrativo @endif </h3>
                            <span class="kt-login__desc">@if(Config::get('app.locale') == 'en') Restrict access @elseif(Config::get('app.locale') == 'es') Acceso restringido. @elseif(Config::get('app.locale') == 'pt-BR') Acesso restrito. @endif</span>
                            <div class="kt-login__actions">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-login__divider">
                    <div></div>
                </div>
                <div class="kt-login__right">
                    <div class="kt-login__wrapper">
                        <div class="kt-login__signin">
                            <div class="kt-login__head">
                                <h3 class="kt-login__title">@if(Config::get('app.locale') == 'en') Access your account @elseif(Config::get('app.locale') == 'es') Acessar sua conta @elseif(Config::get('app.locale') == 'pt-BR') Acessar sua conta @endif</h3>
                            </div>
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <p>{{ $error }}</p>
                                    @endforeach
                                </div>
                            @endif
                            <div class="kt-login__form">
                                <form class="kt-form" method="POST" action="{{ route('admin.login', app()->getLocale()) }}">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <input class="form-control @error('email') is-invalid @enderror" type="email" placeholder="E-mail" name="email" autocomplete="off" required autofocus>

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control form-control-last" type="Password" placeholder="@if(Config::get('app.locale') == 'en') Password @elseif(Config::get('app.locale') == 'es') Contraseña @elseif(Config::get('app.locale') == 'pt-BR') Senha @endif" name="password">
                                    </div>
                                    <div class="row kt-login__extra">
                                        <div class="col kt-align-left">
                                        </div>
                                        <div class="col kt-align-right">
                                            <a href="{{ url('/password/reset') }}" id="kt_login_forgot" class="kt-link">@if(Config::get('app.locale') == 'en') Forgot Password? @elseif(Config::get('app.locale') == 'es') ¿Has olvidado la contraseña? @elseif(Config::get('app.locale') == 'pt-BR') Esqueceu a senha? @endif</a>
                                        </div>
                                    </div>
                                    <div class="kt-login__actions">
                                        <button id="kt_login_signin_submit" type="submit" class="btn btn-brand btn-pill btn-elevate"> @if(Config::get('app.locale') == 'en') Log In @elseif(Config::get('app.locale') == 'es') Acceder @elseif(Config::get('app.locale') == 'pt-BR') Entrar @endif
                                            @if(false){{ __('Login') }}@endif
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="kt-login__forgot">
                            <div class="kt-login__head">
                                <h3 class="kt-login__title">@if(Config::get('app.locale') == 'en') Forgot Password? @elseif(Config::get('app.locale') == 'es') ¿Has olvidado la contraseña? @elseif(Config::get('app.locale') == 'pt-BR') Esqueceu a senha? @endif</h3>
                                <div class="kt-login__desc">@if(Config::get('app.locale') == 'en') Please enter your email to search for your account: @elseif(Config::get('app.locale') == 'es') Introduce el correo electrónico para buscar tu cuenta: @elseif(Config::get('app.locale') == 'pt-BR') Escreva seu e-mail para recurerar sua conta: @endif</div>
                            </div>
                            <div class="kt-login__form">
                                <form class="kt-form" method="POST" action="{{ route('password.email', app()->getLocale()) }}">
                                    @csrf
                                    <div class="form-group">
                                        <!--input class="form-control" type="text" placeholder="Email" name="email" id="kt_email" autocomplete="off"-->
                                        <input id="email" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    </div>
                                    <div class="kt-login__actions">
                                        <button id="kt_login_forgot_submit" type="submit" class="btn btn-brand btn-pill btn-elevate">@if(Config::get('app.locale') == 'en') Search @elseif(Config::get('app.locale') == 'es') Buscar @elseif(Config::get('app.locale') == 'pt-BR') Recuperar @endif</button>
                                        <button id="kt_login_forgot_cancel" class="btn btn-outline-brand btn-pill">@if(Config::get('app.locale') == 'en') Cancel @elseif(Config::get('app.locale') == 'es') Cancelar @elseif(Config::get('app.locale') == 'pt-BR') Cancelar @endif</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- end:: Page -->

    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                    "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                }
            }
        };
    </script>


@endsection

@section('extra_js')

    <!--begin::Page Scripts(used by this page) -->
    <script src="{{ asset('assets/js/demo1/pages/login/login-general.js') }}" type="text/javascript"></script>

@endsection