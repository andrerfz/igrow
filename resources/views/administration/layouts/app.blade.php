<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8"/>
    <title>{{ config('app.name', 'Studex Acre') }}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    @yield('extra_meta')

    <!--begin::Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Fonts -->

    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <!--end::Page Vendors Styles -->

    <!--begin:: Global Mandatory Vendors -->
    <link href="{{ asset('assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet" type="text/css"/>
    <!--end:: Global Mandatory Vendors -->

    <!--begin:: Global Optional Vendors -->
    <link href="{{ asset('assets/vendors/general/tether/dist/css/tether.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/nouislider/distribute/nouislider.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/dropzone/dist/dropzone.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/summernote/dist/summernote.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/animate.css/animate.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/morris.js/morris.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/sweetalert2/dist/sweetalert2.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/socicon/css/socicon.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/custom/jquery-ui/jquery-ui.bundle.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/custom/vendors/line-awesome/css/line-awesome.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/custom/vendors/flaticon/flaticon.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/custom/vendors/flaticon2/flaticon.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css"/>
    <!--end:: Global Optional Vendors -->

    @yield('extra_css')

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{ asset('assets/css/demo1/style.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="{{ asset('assets/css/demo1/skins/header/base/light.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/demo1/skins/header/menu/light.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/demo1/skins/brand/dark.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/demo1/skins/aside/dark.css') }}" rel="stylesheet" type="text/css"/>

    <link href="{{ mix('css/app.css') }}?v=00001" rel="stylesheet" type="text/css"/>

    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">

    <script>
        window.Laravel = {csrfToken: '{{ csrf_token() }}'};
    </script>

</head>


<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading @if(Session::get('sideBar', true)) {{'kt-aside--minimize'}} @endif">

    @include('administration.layouts.partials.layout-page-loader')

    @if(Auth::check())
    <!-- begin:: Page -->

    @include('administration.layouts.partials.header-base-mobile')

    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

            @include('administration.layouts.partials.aside-base')

            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                @include('administration.layouts.partials.header-base')

                <div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                    @yield('fix_head')

                    @yield('content')

                </div>

                @yield('footer')

                @include('administration.layouts.partials.footer-base')

            </div>

        </div>
    </div>

    @include('administration.layouts.partials.offcanvas-search')
    @include('administration.layouts.partials.offcanvas-notifications')
    @include('administration.layouts.partials.offcanvas-quick-actions')
    @if(false)@include('administration.layouts.partials.offcanvas-user')@endif
    @include('administration.layouts.partials.layout-quick-panel')
    @include('administration.layouts.partials.layout-scrolltop')
    @if(false)@include('administration.layouts.partials.layout-toolbar')@endif
    @if(false)@include('administration.layouts.partials.layout-demo-panel')@endif
    @include('administration.layouts.partials.layout-chat')

    @else

        @yield('content')

    @endif


    <!--begin:: Global Mandatory Vendors -->
    <script src="{{ asset('assets/vendors/general/jquery/dist/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/popper.js/dist/umd/popper.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/js-cookie/src/js.cookie.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/moment/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/sticky-js/dist/sticky.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/wnumb/wNumb.js') }}" type="text/javascript"></script>
    <!--end:: Global Mandatory Vendors -->

    <!--begin:: Global Optional Vendors -->
    <script src="{{ asset('assets/vendors/general/jquery-form/dist/jquery.form.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/jquery-ui/jquery-ui.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/block-ui/jquery.blockUI.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/js/vendors/bootstrap-datepicker.init.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/js/vendors/bootstrap-timepicker.init.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap-daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap-maxlength/src/bootstrap-maxlength.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/js/vendors/bootstrap-switch.init.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/select2/dist/js/select2.full.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/ion-rangeslider/js/ion.rangeSlider.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/typeahead.js/dist/typeahead.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/handlebars/dist/handlebars.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/inputmask/dist/jquery.inputmask.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/nouislider/distribute/nouislider.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/owl.carousel/dist/owl.carousel.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/autosize/dist/autosize.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/clipboard/dist/clipboard.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/dropzone/dist/dropzone.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/summernote/dist/summernote.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/markdown/lib/markdown.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/js/vendors/bootstrap-markdown.init.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap-notify/bootstrap-notify.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/js/vendors/bootstrap-notify.init.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/jquery-validation/dist/jquery.validate.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/jquery-validation/dist/additional-methods.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/js/vendors/jquery-validation.init.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/toastr/build/toastr.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/raphael/raphael.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/morris.js/morris.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/chart.js/dist/Chart.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/vendors/jquery-idletimer/idle-timer.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/waypoints/lib/jquery.waypoints.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/counterup/jquery.counterup.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/es6-promise-polyfill/promise.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/sweetalert2/dist/sweetalert2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/js/vendors/sweetalert2.init.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/jquery.repeater/src/lib.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/jquery.repeater/src/jquery.input.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/jquery.repeater/src/repeater.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/dompurify/dist/purify.js') }}" type="text/javascript"></script>
    <!--end:: Global Optional Vendors -->

    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="{{ asset('assets/js/demo1/scripts.bundle.js') }}" type="text/javascript"></script>
    <!--end::Global Theme Bundle -->

    <!--begin::Page Vendors(used by this page) -->
    <script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
    <!--script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM') }}" type="text/javascript"></script-->
    <script src="{{ asset('assets/vendors/custom/gmaps/gmaps.js') }}" type="text/javascript"></script>
    <!--end::Page Vendors -->

    <!--begin::Page Scripts(used by this page) -->
    <script src="{{ asset('assets/js/demo1/pages/dashboard.js') }}" type="text/javascript"></script>
    <!--end::Page Scripts -->


    <!--script src="{{ mix('js/app.js') }}" type="text/javascript"></script-->


    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>

        /******** VARIABLES *******/

        let json_search = null,
            select2 = $('.select2').select2(),
            dataTableLanguage = '//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json',
            KTAppOptions = {
                "colors": {
                    "state": {
                        "brand": "#5d78ff",
                        "dark": "#282a3c",
                        "light": "#ffffff",
                        "primary": "#5867dd",
                        "success": "#34bfa3",
                        "info": "#36a3f7",
                        "warning": "#ffb822",
                        "danger": "#fd3995"
                    },
                    "base": {
                        "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                        "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                    }
                }
            },
            inputMask = function () {
                let handleInputMasks = function () {
                    $(".mask_date").inputmask("d/m/y", {autoUnmask: !0}),
                        $(".mask_date1").inputmask("d/m/y", {placeholder: "*"}),
                        $(".mask_date2").inputmask("d/m/y", {placeholder: "dd/mm/yyyy"}),
                        $(".mask_phone_usa").inputmask("mask", {mask: "(999) 999-9999"}),
                        $(".mask_phone_spain").inputmask("mask", {mask: "(999) 99-99-99"}),
                        $(".mask_phone_brazil_fix").inputmask("mask", {mask: "(99) 9999-9999"}),
                        $(".mask_phone_brazil_mobile").inputmask("mask", {mask: "(99) 99999-9999"}),
                        $(".mask_tin").inputmask({mask: "99-9999999", placeholder: ""}),
                        $(".mask_number").inputmask({mask: "9", repeat: 15, greedy: !1}),
                        $(".mask_decimal").inputmask("decimal", {
                            digits: 3,
                            radixPoint: ",",
                            groupSeparator: ".",
                            groupSize: 3,
                            autoGroup: true,
                            autoUnmask: false,
                            unmaskAsNumber: false,
                            removeMaskOnSubmit: false
                        }).focus(function () {
                            $(this).select();
                        }),
                        $(".mask_decimal2").inputmask("decimal", {
                            digits: 2,
                            radixPoint: ",",
                            groupSeparator: ".",
                            groupSize: 3,
                            autoGroup: true,
                            autoUnmask: false,
                            unmaskAsNumber: false,
                            removeMaskOnSubmit: false
                        }).focus(function () {
                            $(this).select();
                        }),
                        $(".mask_currency").inputmask("999.999,99", {numericInput: !0, showMaskOnFocus: !1}),
                        $(".mask_currency2").inputmask("999.999.99", {
                            numericInput: !0,
                            rightAlignNumerics: !1,
                            greedy: !1
                        }),
                        $(".mask_ssn").inputmask("999-99-9999", {placeholder: " "}),
                        $(".mask_ssn_brazil").inputmask("999.99999.99-9", {placeholder: " "}),
                        $(".mask_ipv4").inputmask({"mask": "999.999.999.999"})
                };
                return {
                    init: function () {
                        handleInputMasks();
                    }
                }
            }();

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "showDuration": "1",
            "hideDuration": "1",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        jQuery.validator.addMethod(
            "notEqualTo",
            function(elementValue,element,param) {
                return elementValue != param;
            },
            "This field is required."
        );



        /******** EVENTS *******/



        $(document).ready(function () {
            inputMask.init();
            $(document).on('focus', '.select2.select2-container', function (e) {
                let isOriginalEvent = e.originalEvent, isSingleSelect = $(this).find(".select2-selection--single").length > 0;
                if (isOriginalEvent && isSingleSelect) {
                    $(this).siblings('select:enabled').select2('open');
                }
            }).on('select2:select', '#company_filter', function (e) {
                let data = e.params.data;
                /** This set company filter on Session **/
                $.ajax({
                    url: '/{{$language}}/admin/ajax/companyFilter',
                    dataType: "json",
                    type: 'POST',
                    data: {
                        company_filter: data.id,
                        _token: window.Laravel.csrfToken
                    }
                });
            }).on('click', '.kt-aside__brand-aside-toggler', function () {
                /** This set sideBar on Session **/
                $.ajax({
                    url: '/{{$language}}/admin/ajax/sideBarSession',
                    dataType: "json",
                    type: 'POST',
                    data: {
                        _token: window.Laravel.csrfToken
                    }
                });
            });
        });



        /******** FUNCTIONS *******/



        /**
         * Function to retrieve Json to Jquery autocomplete
         *

         function fieldSearchAutocomplete() {

            $.ajax({
                url: '/admin/usuarios/ajax/fieldSearchAutocomplete',
                type: 'POST',
                data: {
                    _token: window.Laravel.csrfToken
                },
                success: function (data) {

                    json_search = data;
                },
                error: function (message) {
                    json_search = [];
                }
            });
        }

         fieldSearchAutocomplete();

         $('input#search_input').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/admin/usuarios/ajax/fieldSearchAutocomplete',
                    type: 'POST',
                    data: {
                        term: request.term,
                        _token: window.Laravel.csrfToken
                    },
                    success: function (data) {

                        response($.ui.autocomplete.filter(data, request.term));
                    }
                });
            },
            //function( request, response ) {
            //response( $.ui.autocomplete.filter(json_search, request.term) )
            //},
            minLength: 3,
            select: function (event, ui) {
                window.location.href = ui.item.value;
            },
            focus: function (event, ui) {
                event.preventDefault();
            },
            appendTo: "#search_results",

        }).bind("autocompleteselect", function (event, ui) {

            window.location.href = ui.item.value;

        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<div>" + item.icon + ' ' + item.label + "</div>")
                .appendTo(ul);
        };

         **/

    </script>
    <!-- end::Global Config -->


    @yield('extra_js')

</body>
</html>
