
<!-- begin:: Header Topbar -->
<div class="kt-header__topbar">

    @include('administration.layouts.partials.topbar-search')
    @include('administration.layouts.partials.topbar-notifications')
    @include('administration.layouts.partials.topbar-quick-actions')
    @include('administration.layouts.partials.topbar-my-cart')
    @include('administration.layouts.partials.topbar-quick-panel')
    @include('administration.layouts.partials.topbar-languages')
    @include('administration.layouts.partials.topbar-user')
    
</div>
<!-- end:: Header Topbar -->