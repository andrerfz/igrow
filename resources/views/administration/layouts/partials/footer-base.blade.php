
<!-- begin:: Footer -->
<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
    <div class="kt-container kt-container--fluid">
        <div class="kt-footer__copyright">{{ date('Y') }}&nbsp;&copy;&nbsp; Brincos Studex | Versão {{trim(exec('git log --pretty="%h" -n1 HEAD'))}}</div>
        <div class="kt-footer__menu">
            <a href="#" target="_blank" class="kt-footer__menu-link kt-link">Sobre nós</a>
            <a href="#" target="_blank" class="kt-footer__menu-link kt-link">Equipe</a>
            <a href="#" target="_blank" class="kt-footer__menu-link kt-link">Contato</a>
        </div>
    </div>
</div>
<!-- end:: Footer -->
