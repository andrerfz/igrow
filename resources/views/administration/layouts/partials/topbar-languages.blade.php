
<!--begin: Language bar -->
<div class="kt-header__topbar-item kt-header__topbar-item--langs">
    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
        <span class="kt-header__topbar-icon">
            <img class="" src="{{asset(App::isLocale('en') ? 'assets/media/flags/020-flag.svg' : (App::isLocale('en') ? 'assets/media/flags/016-spain.svg' : 'assets/media/flags/011-brazil.svg'))}}" alt="" />
        </span>
    </div>
    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround">
        @include('layouts.partials.dropdown-languages')
    </div>
</div>
<!--end: Language bar -->