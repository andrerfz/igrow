
<!-- begin:: Aside Menu -->
<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
    <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
        <ul class="kt-menu__nav ">
            <li class="kt-menu__item {{route('admin.dashboard', app()->getLocale()) === url()->current() ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                <a href="{{route('admin.dashboard', app()->getLocale())}}" class="kt-menu__link ">
                    <span class="kt-menu__link-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon id="Bound" points="0 0 24 0 24 24 0 24"/>
                                <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" id="Shape" fill="#000000" fill-rule="nonzero"/>
                                <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" id="Path" fill="#000000" opacity="0.3"/>
                            </g>
                        </svg>
                    </span>
                    <span class="kt-menu__link-text">@if(auth()->user()->language->name == 'en') Dashboard @elseif(auth()->user()->language->name == 'es') Informe general @elseif(auth()->user()->language->name == 'pt-BR') Relatório Geral @endif</span>
                </a>
            </li>

            <li class="kt-menu__section">
                <i class="kt-menu__section-icon flaticon-more-v2"></i>
            </li>

            <li class="kt-menu__item {{route('admin.companys', app()->getLocale()) === url()->current() ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                <a href="{{route('admin.companys', app()->getLocale())}}" class="kt-menu__link ">
                    <span class="kt-menu__link-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect id="bound" x="0" y="0" width="24" height="24"/>
                                <path d="M13.5,21 L13.5,18 C13.5,17.4477153 13.0522847,17 12.5,17 L11.5,17 C10.9477153,17 10.5,17.4477153 10.5,18 L10.5,21 L5,21 L5,4 C5,2.8954305 5.8954305,2 7,2 L17,2 C18.1045695,2 19,2.8954305 19,4 L19,21 L13.5,21 Z M9,4 C8.44771525,4 8,4.44771525 8,5 L8,6 C8,6.55228475 8.44771525,7 9,7 L10,7 C10.5522847,7 11,6.55228475 11,6 L11,5 C11,4.44771525 10.5522847,4 10,4 L9,4 Z M14,4 C13.4477153,4 13,4.44771525 13,5 L13,6 C13,6.55228475 13.4477153,7 14,7 L15,7 C15.5522847,7 16,6.55228475 16,6 L16,5 C16,4.44771525 15.5522847,4 15,4 L14,4 Z M9,8 C8.44771525,8 8,8.44771525 8,9 L8,10 C8,10.5522847 8.44771525,11 9,11 L10,11 C10.5522847,11 11,10.5522847 11,10 L11,9 C11,8.44771525 10.5522847,8 10,8 L9,8 Z M9,12 C8.44771525,12 8,12.4477153 8,13 L8,14 C8,14.5522847 8.44771525,15 9,15 L10,15 C10.5522847,15 11,14.5522847 11,14 L11,13 C11,12.4477153 10.5522847,12 10,12 L9,12 Z M14,12 C13.4477153,12 13,12.4477153 13,13 L13,14 C13,14.5522847 13.4477153,15 14,15 L15,15 C15.5522847,15 16,14.5522847 16,14 L16,13 C16,12.4477153 15.5522847,12 15,12 L14,12 Z" id="Combined-Shape" fill="#000000"/>
                                <rect id="Rectangle-Copy-2" fill="#FFFFFF" x="13" y="8" width="3" height="3" rx="1"/>
                                <path d="M4,21 L20,21 C20.5522847,21 21,21.4477153 21,22 L21,22.4 C21,22.7313708 20.7313708,23 20.4,23 L3.6,23 C3.26862915,23 3,22.7313708 3,22.4 L3,22 C3,21.4477153 3.44771525,21 4,21 Z" id="Rectangle-2" fill="#000000" opacity="0.3"/>
                            </g>
                        </svg>
                    </span>
                    <span class="kt-menu__link-text">@if(auth()->user()->language->name == 'en') Company's @elseif(auth()->user()->language->name == 'es') Empresas @elseif(auth()->user()->language->name == 'pt-BR') Empresas @endif</span>
                </a>
            </li>


            <li class="kt-menu__section">
                <i class="kt-menu__section-icon flaticon-more-v2"></i>
            </li>

            <li class="kt-menu__item {{route('admin.users', app()->getLocale()) === url()->current() ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                <a href="{{route('admin.users', app()->getLocale())}}" class="kt-menu__link ">
                    <span class="kt-menu__link-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon id="Shape" points="0 0 24 0 24 24 0 24"/>
                                <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" id="Combined-Shape" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" id="Combined-Shape" fill="#000000" fill-rule="nonzero"/>
                            </g>
                        </svg>
                    </span>
                    <span class="kt-menu__link-text">@if(auth()->user()->language->name == 'en') Users @elseif(auth()->user()->language->name == 'es') Usuarios @elseif(auth()->user()->language->name == 'pt-BR') Usúarios @endif</span>
                </a>
            </li>

            <li class="kt-menu__section">
                <i class="kt-menu__section-icon flaticon-more-v2"></i>
            </li>

            <li class="kt-menu__item {{route('admin.roles', app()->getLocale()) === url()->current() ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                <a href="{{route('admin.roles', app()->getLocale())}}" class="kt-menu__link ">
                    <span class="kt-menu__link-icon">
                        <i class="flaticon-map"></i>
                    </span>
                    <span class="kt-menu__link-text">@if(auth()->user()->language->name == 'en') Role @elseif(auth()->user()->language->name == 'es') Role @elseif(auth()->user()->language->name == 'pt-BR') Cargos @endif</span>
                </a>
            </li>

            <li class="kt-menu__section ">
                <i class="kt-menu__section-icon flaticon-more-v2"></i>
            </li>

            <li class="kt-menu__item kt-menu__item--submenu
                {{
                    (route('app.users', app()->getLocale()) === url()->current() || request()->is('admin/users/employee/*')) ? 'kt-menu__item--open' : '' ||
                    (route('app.roles', app()->getLocale()) === url()->current() || request()->is('admin/users/roles/*')) ? 'kt-menu__item--open' : '' ||
                    (route('app.company_profile', app()->getLocale()) === url()->current() || request()->is('admin/configuration/company_profile/*')) ? 'kt-menu__item--open' : ''
                }}
                    " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                <a href="javascript:" class="kt-menu__link kt-menu__toggle">
                    <span class="kt-menu__link-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect id="bound" x="0" y="0" width="24" height="24"/>
                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" id="Combined-Shape" fill="#000000"/>
                            </g>
                        </svg>
                    </span>
                    <span class="kt-menu__link-text">@if(auth()->user()->language->name == 'en') Setup @elseif(auth()->user()->language->name == 'es') Ajustes @elseif(auth()->user()->language->name == 'pt-BR') Ajustes @endif</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="kt-menu__submenu">
                    <span class="kt-menu__arrow"></span>
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true">
                            <span class="kt-menu__link">
                                <span class="kt-menu__link-text">>@if(auth()->user()->language->name == 'en') Configurations @elseif(auth()->user()->language->name == 'es') Configuración @elseif(auth()->user()->language->name == 'pt-BR') Configurações @endif</span>
                            </span>
                        </li>
                        <li class="kt-menu__item {{route('admin.dashboard', app()->getLocale()) === url()->current() ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                            <a href="{{route('admin.dashboard', app()->getLocale())}}" class="kt-menu__link ">
                                <span class="kt-menu__link-icon">
                                    <i class="flaticon-apps"></i>
                                </span>
                                <span class="kt-menu__link-text">@if(auth()->user()->language->name == 'en') Applications @elseif(auth()->user()->language->name == 'es') Apps @elseif(auth()->user()->language->name == 'pt-BR') Apps @endif</span>
                            </a>
                        </li>
                        <li class="kt-menu__item {{route('admin.dashboard', app()->getLocale()) === url()->current() ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                            <a href="{{route('admin.dashboard', app()->getLocale())}}" class="kt-menu__link ">
                                <span class="kt-menu__link-icon">
                                    <i class="flaticon-lock"></i>
                                </span>
                                <span class="kt-menu__link-text">@if(auth()->user()->language->name == 'en') Permisions @elseif(auth()->user()->language->name == 'es') Permisos @elseif(auth()->user()->language->name == 'pt-BR') Permissões @endif</span>
                            </a>
                        </li>
                        <li class="kt-menu__item {{(route('app.company_profile', app()->getLocale()) === url()->current() || request()->is('admin/configuration/company_profile/*')) ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                            <a href="{{route('app.company_profile', app()->getLocale())}}" class="kt-menu__link ">
                                <span class="kt-menu__link-icon">
                                    <i class="flaticon-speech-bubble-1"></i>
                                </span>
                                <span class="kt-menu__link-text">@if(auth()->user()->language->name == 'en') Language @elseif(auth()->user()->language->name == 'es') Lenguajes @elseif(auth()->user()->language->name == 'pt-BR') Linguagens @endif</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- end:: Aside Menu -->