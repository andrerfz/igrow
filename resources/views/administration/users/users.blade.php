@extends('administration.layouts.app')

@section('extra_css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>

    <style>

        .selection {
            width: 100%;
        }
        .select2-container {
            min-width: 150px;
            display: inline-flex;
            height: 32px !important;
            padding-top: 0;
            padding-bottom: 0;
            margin-top: 0.25rem;
            margin-bottom: 0.25rem;
        }

        .select2-selection {
            display: inline-flex;
            height: 32px !important;
            padding-top: 0;
            padding-bottom: 0;
            margin-bottom: 0.25rem;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 1.2 !important;
        }

    </style>
@endsection

@section('fix_head')

    <div class="kt-subheader kt-grid__item" id="kt_subheader">
        <div class="kt-container kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">@if($en == 'en') Users @elseif($es == 'es') Usuarios @elseif($ptBR== 'pt-BR') Usúarios @endif</h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <a href="javascript:" id="new_employee" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10"> Adicionar novo </a>
                <!--
                <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
                    <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
                    <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span>
                        <i class="flaticon2-search-1"></i></span>
                    </span>
                </div>
                -->
            </div>
            <div class="kt-subheader__toolbar">
                <div style="padding-right: 15px;" data-toggle="kt-tooltip" data-placement="left" data-original-title="@if($en == 'en') Select the company @elseif($es == 'es') Seleccionar la empresa @elseif($ptBR== 'pt-BR') Selecionar a empresa @endif">
                    <select class="btn form-control kt-input select2" id="company_filter" style="width: 100%;">
                            <option value="0">@if($en == 'en') All companys @elseif($es == 'es') Todas empresas @elseif($ptBR== 'pt-BR') Todas empresas @endif</option>
                        @foreach($companys as $company)
                            <option value="{{$company->id}}" {{Session::get('company_filter') == $company->id ? 'selected' : ''}}> {{$company->trading_name && $company->business_name ? $company->trading_name .' ('. $company->business_name . ')' : ($company->trading_name ?: $company->business_name)}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <!--
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">-->

                    <!--
                    <a href="#" class="btn kt-subheader__btn-primary">
                        Actions &nbsp;
                    </a>
                    -->

                    <!--
                    <div class="dropdown dropdown-inline" data-toggle-="kt-tooltip" title="Quick actions" data-placement="left">
                        <a href="#" class="btn btn-icon"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon id="Shape" points="0 0 24 0 24 24 0 24"/>
                                    <path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" id="Combined-Shape" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                    <path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" id="Combined-Shape" fill="#000000"/>
                                </g>
                            </svg>
                             -->
                            <!--<i class="flaticon2-plus"></i>-->

                        <!--
                        </a>
                        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right"> -->
                            <!--begin::Nav-->
                            <!--
                            <ul class="kt-nav">
                               <li class="kt-nav__item">
                                   <a href="#" class="kt-nav__link"> <i class="kt-nav__link-icon flaticon2-plus"></i> <span class="kt-nav__link-text">Colaborador</span> </a>
                               </li>
                           </ul>
                           -->
                           <!--end::Nav-->
                        <!--
                        </div>
                    </div>
                </div>
            </div>
            -->
        </div>
    </div>

@endsection

@section('content')

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-user"></i>
                </span>
                <h3 class="kt-portlet__head-title">Filtros de busca</h3>
            </div>
            <!--
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <div class="dropdown dropdown-inline">
                            <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="la la-download"></i> Exportar
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__section kt-nav__section--first">
                                        <span class="kt-nav__section-text">Escolha uma opção</span>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-print"></i>
                                            <span class="kt-nav__link-text">Print</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-copy"></i>
                                            <span class="kt-nav__link-text">Copy</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                            <span class="kt-nav__link-text">Excel</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-text-o"></i>
                                            <span class="kt-nav__link-text">CSV</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                            <span class="kt-nav__link-text">PDF</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        &nbsp
                    </div>
                </div>
            </div>-->
        </div>
        <div class="kt-portlet__body">

            <div class="row">
                <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                    <label>Cargo:</label>
                    <select class="form-control kt-input select2" id="role_filter">
                        <option value="0">Todos</option>
                        @foreach($roles as $role)
                            <option value="{{$role->id}}"> {{$role->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                    <label>Estado:</label>
                    <select class="form-control kt-input select2" id="state_filter">
                        <option value="0">Todos</option>
                        <option value="1">Ativos</option>
                        <option value="2">Inativos</option>
                    </select>
                </div>
                <div class="col-lg-1 kt-margin-b-10-tablet-and-mobile">
                    <label>Apagados:</label>
                    <span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--warning">
                        <label>
                            <input type="checkbox" id="recycle_filter">
                            <span></span>
                        </label>
                    </span>
                </div>
            </div>

            <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>
            <table class="table table-striped- table-bordered table-hover table-checkable" id="users_table">
                <thead>
                <tr>
                    <th>Foto</th>
                    <th>Nome</th>
                    <th>Cargo</th>
                    <th>Telefone</th>
                    <th>Email</th>
                    <th>Identidade</th>
                    <th>PIS</th>
                    <th>Estado</th>
                    <th>Ações</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>

    <div id="modal_new_user" class="modal fade" data-callback="reload_users_table" tabindex="" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form action="/{{$language}}/administration/users/ajax/setUser" class="form" id="form_new_user">
                <input type="hidden" name="id">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ModalLabel"></h5>
                        <button type="reset" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="kt-portlet__body">
                            <div class="kt-section">
                                <h3 class="kt-section__title">Informações básicas:</h3>
                                <div class="kt-section__content">
                                    <div class="form-group row">
                                        <div class="col-lg-4 col-md-4 col-sm-12">
                                            <label class="form-control-label">* Nome:</label>
                                            <input type="text" name="name" class="form-control" placeholder="Nome do colaborador">
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-12">
                                            <label class="form-control-label">* Sobrenome:</label>
                                            <input type="text" name="last_name" class="form-control" placeholder="Sobrenome do colaborador">
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="mail">
                                        <div class="col-lg-12">
                                            <label class="form-control-label">* Email:</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="email" placeholder="Email do colaborador">
                                                <div class="input-group-append"><a href="#" class="btn btn-brand btn-icon">@</a></div>
                                            </div>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <label class="form-control-label">Telefone fixo:</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control mask_phone_brazil_fix" name="fix_phone" placeholder="Telefone fixo">
                                                <div class="input-group-append"><a href="#" class="btn btn-brand btn-icon"><i class="la la-phone"></i></a></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <label class="form-control-label">*Telefone móvel:</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control mask_phone_brazil_mobile" name="mobile_phone" placeholder="Telefone móvel">
                                                <div class="input-group-append"><a href="#" class="btn btn-brand btn-icon"><i class="la la-mobile-phone"></i></a></div>
                                            </div>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <label class="form-control-label">* Número de Identidade:</label>
                                            <input type="text" class="form-control mask_number" name="identification_number" placeholder="Número de Identidade">
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <label class="form-control-label">* Seguro Social:</label>
                                            <input type="text" class="form-control mask_ssn_brazil" name="social_number" placeholder="Seguro social">
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--border-dashed kt-separator--space-xl"></div>
                            <div class="kt-section">
                                <h3 class="kt-section__title">Informações Avançadas:</h3>
                                <div class="form-group row">
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <label class="form-control-label">* Cargo:</label>
                                        <div class="input-group">
                                            <select class="form-control kt-input select2" name="role_employee" style="width: 100%">
                                                <option value="0">Todos</option>
                                                @foreach($roles as $role_)
                                                    <option value="{{$role_->id}}"> {{$role_->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary send-form-modal_new_employee">Salvar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="modal_new_picture" class="modal fade" data-callback="" tabindex="" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <form action="/{{$language}}/administration/users/ajax/setPicture" class="form">
                <input type="hidden" name="id">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ModalLabel">Inserir imagem</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="file" id="file">
                            <label class="custom-file-label" for="file">Escolher arquivo</label>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary send-form">Enviar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="modal_email_password" class="modal fade" data-callback="" tabindex="" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <form action="/{{$language}}/administration/users/ajax/recoveryAccount" class="form">
                <input type="hidden" name="id">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ModalLabel">Enviar email para redefinir senha.</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary send-form">Enviar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="modal_lock" class="modal fade" data-callback="reload_users_table" tabindex="" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <form action="/{{$language}}/administration/users/ajax/toggleActive" class="form">
                <input type="hidden" name="id">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ModalLabel"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary send-form">Enviar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="modal_delete" class="modal fade" data-callback="" tabindex="" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <form action="" class="form">
                <input type="hidden" name="id">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ModalLabel"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary send-form" data-loading-text="Apagando...">Apagar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection


@section('extra_js')

    <!--begin::Page Vendors(used by this page) -->
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/demo1/pages/crud/datatables/advanced/column-rendering.js') }}" type="text/javascript"></script>
    <!--end::Page Scripts -->

    <script>
        $(document).ready(function () {

            /** Global variables **************************************************************************/

            let line_data_pickup = null, /* Variable to guard the dataTable line values */
                validator = null, /* Variable to guard form validation rules */
                language = '{{ $language }}', /* Variable to guard user language */
                /** Users Table - Retrieve lines from table users to Datatable's. **/
                ajax_users_table = {
                    url: "/" + language + "/administration/users/ajax/getUsers",
                    type: "POST",
                    data: function (d) {
                        d.state_filter = $('#state_filter').val();
                        d.recycle_filter = $('#recycle_filter').is(':checked');
                        d.role_id = $('#role_filter').val();
                        d._token = window.Laravel.csrfToken;
                    }
                },
                users_table = $('#users_table').DataTable({
                    "language": {
                        "url": dataTableLanguage
                    },
                    ajax: ajax_users_table,
                    responsive: false,
                    pagingType: 'full_numbers',
                    paging: true,
                    columns: [
                        {
                            data: "picture",
                            sClass: "insertFile va-middle padding-foto padding-top-bottom"
                        },
                        {
                            data: "full_name",
                            sClass: "va-middle"
                        },
                        {
                            data: "role",
                            sClass: "va-middle"
                        },
                        {
                            data: "mobile_phone",
                            sClass: "va-middle"
                        },
                        {
                            data: "email",
                            sClass: "va-middle",
                            render: function (data, type, full, meta) {
                                return '<a class="kt-link" href="mailto:' + data + '">' + data + '</a>';
                            },
                        },
                        {
                            data: "identification_number",
                            sClass: "va-middle"
                        },
                        {
                            data: "social_number",
                            sClass: "va-middle"
                        },
                        {
                            data: "active",
                            sClass: "va-middle"
                        },
                        {
                            data: "actions",
                            sClass: "va-middle align-center"
                        },
                    ],
                    columnDefs: [
                        {targets: -1, orderable: false, width: '5%'},
                        {targets: -2, orderable: true, width: '1%'},
                        {targets: 0, orderable: false, width: '1%'},
                    ],
                    order: [1, 'asc'],

                }).on('click', 'span.linkActive', function () {
                    let tr = $(this).parent().parent(), id = users_table.row(tr).data().id;
                    $.ajax({
                        type: 'POST',
                        url: "/" + language + '/administration/users/employee/ajax/toggleActive',
                        data: {
                            id: id,
                            _token: window.Laravel.csrfToken
                        },
                        success: function (data) {
                            reload_users_table();
                            if (data.not) {
                                toastr[data.not.tipo](data.not.mensaje, data.not.titulo);
                            }
                        }
                    });
                }).on('click', 'span.dropdown', function () {
                    let tr = $(this).parent().parent().parent();
                    line_data_pickup = users_table.row(tr).data();
                }).on('click', 'td div a', function () {
                    let tr = $(this).parent().parent().parent();
                    line_data_pickup = users_table.row(tr).data();
                }).on('click', 'td.insertFile', function () {
                    let tr = $(this).parent(), data = users_table.row(tr).data();
                    if (data.id != undefined) {
                        $('#modal_new_picture input[name="id"]').val(data.id);
                        $('#modal_new_picture input[type="file"]').val('');
                        $('#modal_new_picture').modal({
                            backdrop: 'static',
                            keyboard: false,
                            show: true
                        }).on('hidden.bs.modal', function () {
                            reload_users_table();
                        });
                    }
                });


            /** Events *****************************************************************************/


            $(document).on('select2:select', '#role_filter', function () {
                reload_users_table();
            }).on('select2:select', '#state_filter', function () {
                reload_users_table();
            }).on('change', '#recycle_filter', function () {
                reload_users_table();
            }).on('click', '#new_employee', function () {
                validation('user', true);
                validator.resetForm();
                $('#modal_new_user #mail').show();
                $('#modal_new_user .send-form-modal_new_employee').removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                $('#modal_new_user .modal-title').text('Novo colaborador');
                $('#modal_new_user input[name="id"]').val('');
                $('#modal_new_user input[type="text"]').val('');
                $('#modal_new_user input[type="checkbox"]').prop('checked', false);
                $('#modal_new_user select').val(0).trigger('change');
                $('#modal_new_user').modal({
                    backdrop: 'static',
                    keyboard: false,
                    show: true
                }).on('hidden.bs.modal', function () {
                    reload_users_table();
                });
            }).on('click', '.send-form-modal_new_employee', function (e) {
                e.preventDefault();
                let $btn = $(this), form = $(this).closest('form'), modal = $(this).closest('.modal');
                $btn.addClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                if (form.valid()) {
                    sendForm($btn, form, modal);
                } else {
                    $btn.removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                }
            }).on('click', '.send-form', function (e) {
                e.preventDefault();
                let $btn = $(this), form = $(this).closest('form'), modal = $(this).closest('.modal');
                $btn.addClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                if (form.valid()) {
                    sendForm($btn, form, modal);
                } else {
                    $btn.removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                }
            }).on('click', 'a.user_options', function () {
                let action = $(this).data('action');
                switch (action) {

                    case 'see':
                        op_see(line_data_pickup);
                        break;

                    case 'lock':
                        op_lock(line_data_pickup);
                        break;

                    case 'password':
                        op_password(line_data_pickup);
                        break;

                    case 'edit':
                        op_edit(line_data_pickup);
                        break;

                    case 'softdelete':
                        op_delete(line_data_pickup, 'Eliminar usúario', '<p>Deseja continuar?</p>', 'softdelete', "/" + language + '/administration/users/ajax/deleteUser', 'reload_users_table');
                        break;

                    case 'recycle':
                        op_delete(line_data_pickup, 'Recuperar usúario', '<p>Deseja continuar?</p>', 'recycle', "/" + language + '/administration/users/ajax/deleteUser', 'reload_users_table');
                        break;

                    default:
                        return false;
                }
            });


            /** Functions *****************************************************************************/

            /**
             * Function to Send Data from form DataTable
             *
             */
            function sendForm($btn, form, modal) {
                let callback = modal.data('callback'), action = form.attr('action'), formdata = new FormData(form[0]);
                setTimeout(function () {
                    $.ajax({
                        type: "POST",
                        url: action,
                        data: formdata,
                        async: false,
                        success: function (data) {
                            if (data.status === 1 || data.status === 15) {
                                modal.modal('hide');
                            }
                            if (callback) {
                                eval(callback + '()');
                            }
                            if (data.not) {
                                toastr[data.not.tipo](data.not.mensaje, data.not.titulo);
                            }
                            $btn.removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                        },
                        error: function () {
                            $btn.removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                }, 1000);
            }

            function reload_users_table() {
                users_table.ajax.reload(null, false);
            }

            function op_see(data) {
                let url = window.location.href, id = data.id;
                window.open(url + '/' + id, '_self');
            }

            function op_lock(data) {
                $('#modal_lock input[name="id"]').val(data.id);
                if (data.active_o) {
                    $('#modal_lock .modal-title').text('Desativar colaborador');
                    $('#modal_lock .modal-body').html('<p>Se desativar o colaborador, esse já não poderá utilizar a aplicação. Deseja continuar?</p>');
                } else {
                    $('#modal_lock .modal-title').text('Ativar colaborador');
                    $('#modal_lock .modal-body').html('<p>Se ativar o colaborador, esse poderá utilizar a aplicação. Deseja continuar?</p>');
                }
                $('#modal_lock').modal('show');
            }

            function op_password(data) {
                $('#modal_email_password input[name="id"]').val(data.id);
                $('#modal_email_password .modal-body').html('<p>Deseja enviar um email para o usuario redefinir sua senha?.</p>');
                $('#modal_email_password').modal('show');
            }

            function op_edit(data) {
                validation('user', false);
                validator.resetForm();
                $('#modal_new_user #mail').hide();
                $('#modal_new_user .modal-title').text('Editar colaborador');
                $('#modal_new_user input[name="id"]').val(data.id);
                $('#modal_new_user input[name="name"]').val(data.name);
                $('#modal_new_user input[name="last_name"]').val(data.last_name);
                $('#modal_new_user input[name="fix_phone"]').val(data.fix_phone);
                $('#modal_new_user input[name="mobile_phone"]').val(data.mobile_phone);
                $('#modal_new_user input[name="identification_number"]').val(data.identification_number);
                $('#modal_new_user input[name="social_number"]').val(data.social_number);
                $('#modal_new_user select[name="role_employee"]').val(data.role_id).trigger('change');
                $('#modal_new_user .send-form-modal_new_employee').removeClass('kt-spinner kt-spinner kt-spinner--md kt-spinner--light');
                $('#modal_new_user').modal({
                    backdrop: 'static',
                    keyboard: false,
                    show: true
                }).on('hidden.bs.modal', function () {
                    reload_users_table();
                });
            }

            function op_delete(data, title, mensage, options, action, callback) {
                $('#modal_delete form').attr('action', action);
                $('#modal_delete input[name="id"]').val(data.id);
                $('#modal_delete input[name="trashed"]').val(options);
                $('#modal_delete .modal-title').text(title);
                $('#modal_delete .modal-body').html(mensage);
                $('#modal_delete').data('callback', callback).modal('show');
            }

            function validation(form_name, rule) {
                if (form_name === 'user') {
                    validator = $("#form_new_user").validate({
                        // define validation rules
                        rules: {
                            name: {
                                required: true,
                                minlength: 1
                            },
                            last_name: {
                                required: true,
                                minlength: 1
                            },
                            email: {
                                required: rule,
                                email: rule,
                                minlength: rule ? 10 : 0,
                            },
                            identification_number: {
                                required: true,
                                minlength: 1
                            },
                            social_number: {
                                required: true,
                                minlength: 1
                            },
                            mobile_phone: {
                                required: true,
                                minlength: 1
                            },
                            role_employee: {
                                required: true,
                                notEqualTo: 0
                            },
                        },

                        //display error alert on form submit
                        invalidHandler: function (event) {
                            swal.fire({
                                "title": "",
                                "html": "Existem alguns erros no formulário. <br />Por favor corrigi-los.",
                                "type": "error",
                                "confirmButtonClass": "btn btn-brand",
                                "onClose": function (e) {
                                    //console.log('on close event fired!');
                                }
                            });
                            event.preventDefault();
                        },
                        submitHandler: function () {
                            return true;
                        }
                    });
                }
            }
        });
    </script>

@endsection