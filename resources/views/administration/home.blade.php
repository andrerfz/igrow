@extends('administration.layouts.app')

@section('fix_head')

    <div class="kt-subheader kt-grid__item" id="kt_subheader">
        <div class="kt-container kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    @if(Auth::user()->language->name == 'en') Dashboard @elseif(Auth::user()->language->name == 'es') Informe general @elseif(Auth::user()->language->name == 'pt-BR') Relatório Geral @endif
                </h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc">{{Auth::user()->company->business_name}}</span>
                <a href="#" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
                    Adcionar novo widget
                </a>
                <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
                    <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
                    <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span><i class="flaticon2-search-1"></i></span>
                </span>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div style="padding-right: 15px;" data-toggle="kt-tooltip" data-placement="left" data-original-title="@if(Auth::user()->language->name == 'en') Select the company @elseif(Auth::user()->language->name == 'es') Seleccionar la empresa @elseif(Auth::user()->language->name == 'pt-BR') Selecionar a empresa @endif">
                    <select class="btn form-control kt-input select2" id="company_filter" style="width: 100%;">
                        <option value="0">@if(Auth::user()->language->name == 'en') All companys @elseif(Auth::user()->language->name == 'es') Todas empresas @elseif(Auth::user()->language->name == 'pt-BR') Todas empresas @endif</option>
                        @foreach($companys as $company)
                            <option value="{{$company->id}}" {{Session::get('company_filter') == $company->id ? 'selected' : ''}}> {{$company->trading_name && $company->business_name ? $company->trading_name .' ('. $company->business_name . ')' : ($company->trading_name ?: $company->business_name)}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="kt-subheader__wrapper">
                    <a href="#" class="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip" title="" data-placement="left" data-original-title="Select dashboard daterange">
                        <span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title"></span>&nbsp;
                        <span class="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date"></span>
                        <i class="flaticon2-calendar-1"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('content')

    @include('administration.layouts.partials.dashboard')

@endsection
