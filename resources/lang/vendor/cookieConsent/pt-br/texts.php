<?php

return [
    'message' => '
    <div class="col-xs-12 col-md-12 col-lg-12" style="text-align: left">
        <p>Sua privacidade é importante.</p>
        <p>Cookies e tecnologias semelhantes são usados ​​em nossos sites para personalizar conteúdo e anúncios, </p>
        <p>fornecer e aprimorar recursos de produtos e para analisar o tráfego em nossos sites pelo Google, nossos parceiros de negócios e autores.</p>
        <p>Ao continuar a usar nossos sites e serviços, você concorda com o uso desses cookies e tecnologias semelhantes. 
            <a href="#" id="open_cookies"><span>Click para saber mais.</span></a>
        </p>
    </div>',
    'agree' => 'Aceitar',
];
