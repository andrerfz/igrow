<?php

/*
|--------------------------------------------------------------------------
| Administration Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', static function () {
    return redirect(app()->getLocale() . '/login');
});


Route::group(array('middleware' => ['set.locale']), static function () {


    Route::pattern('subdomain2', config('app.admin'));
    Route::pattern('subdomain1', config('app.dashboard'));
    Route::pattern('domain', config('app.domain'));


    Route::domain(config('app.admin') . config('app.dashboard') . config('app.domain'))->group(static function () {


        Route::group(array('prefix' => '{locale}', 'where' => ['locale' => '[A-Za-z\-]+']), static function () {


            // Authentication Routes...
            Route::get('login', 'Auth\LoginController@showAdministrationLoginForm')->name('admin.login');
            Route::post('login', 'Auth\LoginController@login');
            Route::post('logout', 'Auth\LoginController@logout')->name('admin.logout');


            Route::group(array('prefix' => '/administration', 'middleware' => ['auth', 'auth.permissions']), static function () {


                //Retrieve the Dashboard.
                Route::group(array('prefix' => '/'), static function () {
                    Route::get('/', array('as' => 'admin.dashboard', 'uses' => 'HomeController@indexAdministration'));
                    Route::post('/ajax/sideBarSession', array('uses' => 'HomeController@sideBarSession'));
                    Route::post('/ajax/companyFilter', array('uses' => 'HomeController@companyFilter'));
                    Route::post('ajax/getPostCode', array('uses' => 'ConfigurationsController@getPostCode'));
                });


                //Retrieve the company settings.
                Route::group(array('prefix' => '/company'), static function () {
                    Route::get('/companys/', array('as' => 'admin.companys', 'uses' => 'CompanyController@indexAdministration'));
                    Route::post('ajax/getCompanysAdministration', array('uses' => 'CompanyController@getCompanysAdministration'));
                    Route::post('/ajax/setCompanyProfileAdministration', array('uses' => 'CompanyController@setCompanyBranch'));
                    Route::post('/ajax/deleteCompanyBranchAdministration', array('uses' => 'CompanyController@deleteCompanyBranch'));
                    Route::post('/ajax/toggleCompanyBranchAdministration', array('uses' => 'CompanyController@toggleCompanyBranch'));
                });


                //Retrieve the user pages rotes.
                Route::group(array('prefix' => '/users'), static function () {
                    Route::get('/employee/', array('as' => 'admin.users', 'uses' => 'UserController@indexSuper'));
                    Route::get('/employee/{id}', array('as' => 'admin.user', 'uses' => 'UserController@indexSuperDetail'));
                    Route::get('/roles', array('as' => 'admin.roles', 'uses' => 'RolesController@indexSuper'));
                    Route::post('/ajax/getUsers', array('uses' => 'UserController@getUsers'));
                    Route::post('/ajax/getRoles', array('uses' => 'RolesController@getRoles'));
                    Route::post('/ajax/getCustomers', array('uses' => 'UserController@getCustomers'));
                    Route::post('/ajax/getUserPermissions', array('uses' => 'UserController@getUserPermissions'));
                    Route::post('/ajax/setUser', array('uses' => 'UserController@setUser'));
                    Route::post('/ajax/setUserPermission', array('uses' => 'UserController@setUserPermission'));
                    Route::post('/ajax/setRole', array('uses' => 'RolesController@setRole'));
                    Route::post('/ajax/setPicture', array('uses' => 'UserController@setPicture'));
                    Route::post('/ajax/deleteUser', array('uses' => 'UserController@deleteUser'));
                    Route::post('/ajax/deleteRole', array('uses' => 'RolesController@deleteRole'));
                    Route::post('/employee/ajax/toggleActive', array('uses' => 'UserController@toggleActive'));
                    Route::post('/role/ajax/toggleActive', array('uses' => 'RolesController@toggleActive'));
                    Route::post('/ajax/recoveryAccount', array('uses' => 'UserController@recoveryAccount'));
                });

            });

        });

    });

});