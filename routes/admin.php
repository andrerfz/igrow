<?php

/*
|--------------------------------------------------------------------------
| Back office Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', static function () {
    return redirect(app()->getLocale() . '/login');
});


Route::group(array('middleware' => ['set.locale']), static function () {


    Route::pattern('subdomain', config('app.dashboard'));
    Route::pattern('domain', config('app.domain'));


    Route::domain(config('app.dashboard') . config('app.domain'))->group(static function () {


        Route::group(array('prefix' => '{locale}', 'where' => ['locale' => '[A-Za-z\-]+']), static function () {


            Auth::routes();


            Route::group(array('prefix' => '/admin', 'middleware' => ['auth', 'auth.permissions']), static function () {


                //Retrieve the Dashboard.
                Route::group(array('prefix' => '/'), static function () {

                    Route::get('/', array('as' => 'app.dashboard', 'uses' => 'HomeController@index'));
                    Route::post('/ajax/sideBarSession', array('uses' => 'HomeController@sideBarSession'));
                    Route::post('/ajax/companyFilter', array('uses' => 'HomeController@companyFilter'));
                });


                //Retrieve the company settings.
                Route::group(array('prefix' => '/company'), static function () {
                    Route::get('/company_profile/', array('as' => 'app.company_profile', 'uses' => 'CompanyController@index'));
                    Route::post('ajax/getCompanys', array('uses' => 'CompanyController@getCompanys'));
                    Route::post('/ajax/setCompanyProfile', array('uses' => 'CompanyController@setCompanyProfile'));
                    Route::post('/ajax/deleteCompanyBranch', array('uses' => 'CompanyController@deleteCompanyBranch'));
                    Route::post('/ajax/toggleCompanyBranch', array('uses' => 'CompanyController@toggleCompanyBranch'));
                });


                //Retrieve the user pages configuration.
                Route::group(array('prefix' => '/users'), static function () {
                    Route::get('/employee/', array('as' => 'app.users', 'uses' => 'UserController@index'));
                    Route::get('/employee/{id?}', array('as' => 'app.user', 'uses' => 'UserController@indexDetail'));
                    Route::get('/roles', array('as' => 'app.roles', 'uses' => 'RolesController@index'));
                    Route::post('/ajax/getUsers', array('uses' => 'UserController@getUsers'));
                    Route::post('/ajax/getRoles', array('uses' => 'RolesController@getRoles'));
                    Route::post('/ajax/getCustomers', array('uses' => 'UserController@getCustomers'));
                    Route::post('/ajax/getUserPermissions', array('uses' => 'UserController@getUserPermissions'));
                    Route::post('/ajax/setUser', array('uses' => 'UserController@setUser'));
                    Route::post('/ajax/setUserPermission', array('uses' => 'UserController@setUserPermission'));
                    Route::post('/ajax/setRole', array('uses' => 'RolesController@setRole'));
                    Route::post('/ajax/setPicture', array('uses' => 'UserController@setPicture'));
                    Route::post('/ajax/deleteUser', array('uses' => 'UserController@deleteUser'));
                    Route::post('/ajax/deleteRole', array('uses' => 'RolesController@deleteRole'));
                    Route::post('/ajax/deleteUserPermissions', array('uses' => 'UserController@deleteUserPermissions'));
                    Route::post('/employee/ajax/toggleActive', array('uses' => 'UserController@toggleActive'));
                    Route::post('/role/ajax/toggleActive', array('uses' => 'RolesController@toggleActive'));
                    Route::post('/ajax/recoveryAccount', array('uses' => 'UserController@recoveryAccount'));
                });

            });

        });

    });

});