<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlatformToLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('platform_to_language', function (Blueprint $table) {

            $table->bigInteger('platform_id')->unsigned()->nullable();
            $table->bigInteger('lang_id')->unsigned()->nullable();

            $table->string('name')->nullable();

            $table->foreign('platform_id')->references('id')->on('platform');
            $table->foreign('lang_id')->references('id')->on('languages');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('platform_to_language');
    }
}
