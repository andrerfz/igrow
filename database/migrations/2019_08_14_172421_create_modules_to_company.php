<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesToCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules_to_company', function (Blueprint $table) {

            $table->bigInteger('module_id')->unsigned()->nullable();
            $table->bigInteger('company_id')->unsigned()->nullable();

            $table->date('expire_date')->nullable();

            $table->foreign('module_id')->references('id')->on('modules');
            $table->foreign('company_id')->references('id')->on('company');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules_to_company');
    }
}
