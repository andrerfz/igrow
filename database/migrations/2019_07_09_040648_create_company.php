<?php


use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateCompany extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void|int
     */
    public function up()
    {
        Schema::create('company', static function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('hash_id')->nullable();

            $table->bigInteger('company_id')->unsigned()->nullable();
            $table->bigInteger('external_id')->unsigned()->nullable(); //Company external ID

            $table->string('logo')->nullable();

            $table->string('business_name')->nullable();
            $table->string('trading_name')->nullable();

            $table->string('federal_tax_number')->nullable();
            $table->string('state_tax_number')->nullable();

            $table->string('direction')->nullable();
            $table->string('town')->nullable();
            $table->string('state')->nullable();
            $table->string('province')->nullable();
            $table->string('country')->nullable();
            $table->string('postcode')->nullable();

            $table->string('contact')->nullable();
            $table->string('mobile')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('web')->nullable();

            $table->boolean('branch')->default(false);
            $table->boolean('retail')->default(false);
            $table->boolean('wholesale')->default(false);

            $table->boolean('external')->default(false); //Company external created by using the API

            $table->boolean('active')->default(true);
            $table->boolean('locked')->default(false);

            $table->boolean('terms')->default(false);
            $table->boolean('form_register')->default(false); //True If the company was created at the form register

            $table->bigInteger('max_users')->unsigned()->nullable();
            $table->bigInteger('max_company')->unsigned()->nullable();

            $table->foreign('company_id')->references('id')->on('company');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void|int
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}
