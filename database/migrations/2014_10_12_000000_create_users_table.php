<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->bigInteger('lang_id')->unsigned()->nullable();
            $table->bigInteger('company_id')->unsigned()->nullable();
            $table->bigInteger('place_id')->unsigned()->nullable();
            $table->bigInteger('role_id')->unsigned()->nullable();

            $table->text('plafform_modules_permissions')->nullable();

            $table->string('fiscal_number', 50)->nullable();
            $table->string('identification_number', 50)->nullable();
            $table->string('social_number', 50)->nullable();
            $table->string('driver_number', 50)->nullable();

            $table->string('picture')->nullable();
            $table->string('name')->nullable();
            $table->string('last_name')->nullable();
            $table->text('bio')->nullable();
            $table->string('degrees')->nullable();
            $table->string('school_level')->nullable();

            $table->string('direction')->nullable();
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('fix_phone')->nullable();
            $table->string('mobile_phone')->nullable();

            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');

            $table->string('doc_1')->nullable();
            $table->string('doc_2')->nullable();
            $table->string('doc_3')->nullable();
            $table->string('doc_4')->nullable();
            $table->string('doc_5')->nullable();
            $table->string('doc_6')->nullable();
            $table->string('doc_7')->nullable();
            $table->string('doc_8')->nullable();
            $table->string('doc_9')->nullable();
            $table->string('doc_10')->nullable();

            $table->boolean('active')->default(true);
            $table->boolean('locked')->default(false);

            $table->boolean('privacy')->default(false);
            $table->boolean('terms')->default(false);

            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
