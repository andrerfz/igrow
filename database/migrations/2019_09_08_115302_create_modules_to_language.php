<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesToLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules_to_language', function (Blueprint $table) {

            $table->bigInteger('module_id')->unsigned()->nullable();
            $table->bigInteger('lang_id')->unsigned()->nullable();

            $table->string('name')->nullable();

            $table->foreign('module_id')->references('id')->on('modules');
            $table->foreign('lang_id')->references('id')->on('languages');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules_to_language');
    }
}
