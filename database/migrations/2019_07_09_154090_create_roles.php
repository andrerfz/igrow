<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('lang_id')->unsigned()->nullable();
            $table->bigInteger('permission_id')->unsigned()->nullable();
            $table->bigInteger('company_id')->unsigned()->nullable();

            $table->string('name', 100);
            $table->string('description')->nullable();

            $table->boolean('locked')->default(false);
            $table->boolean('active')->default(true);

            $table->foreign('lang_id')->references('id')->on('languages');
            $table->foreign('permission_id')->references('id')->on('permissions');
            $table->foreign('company_id')->references('id')->on('company');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}