<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('platform_id')->unsigned()->nullable();

            $table->string('name', 100);
            $table->string('description')->nullable();

            $table->boolean('locked')->default(true);
            $table->boolean('active')->default(true);

            $table->foreign('platform_id')->references('id')->on('platform');

            $table->softDeletes();
            $table->timestamps();
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
    }
}
