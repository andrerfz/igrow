<?php

use App\Permissions;
use Illuminate\Database\Seeder;


class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*********** US-US ****************************************************************/

        Permissions::create([
            'lang_id'    => '1',
            'name'    => 'Manager',
            'view'      => true,
            'create'    => true,
            'update'    => true,
            'delete'    => true,
            'full'      => true,
            'locked'    => true
        ]);

        Permissions::create([
            'lang_id'    => '1',
            'name'    => 'Manager Contribute',
            'view'      => true,
            'create'    => true,
            'update'    => true,
            'delete'    => false,
            'full'      => true,
            'locked'    => true
        ]);

        Permissions::create([
            'lang_id'    => '1',
            'name'    => 'Manager View Only',
            'view'      => true,
            'create'    => false,
            'update'    => false,
            'delete'    => false,
            'full'      => true,
            'locked'    => true
        ]);

        Permissions::create([
            'lang_id'    => '1',
            'name'    => 'Admin',
            'view'      => true,
            'create'    => true,
            'update'    => true,
            'delete'    => true,
            'full'      => false,
            'locked'    => true
        ]);

        Permissions::create([
            'lang_id'    => '1',
            'name'    => 'Contribute Restricted delete',
            'view'      => true,
            'create'    => true,
            'update'    => true,
            'delete'    => false,
            'full'      => false,
            'locked'    => true
        ]);

        Permissions::create([
            'lang_id'    => '1',
            'name'    => 'Contribute Restricted Read',
            'view'      => true,
            'create'    => false,
            'update'    => false,
            'delete'    => false,
            'full'      => false,
            'locked'    => true
        ]);

        Permissions::create([
            'lang_id'    => '1',
            'name'    => 'Not Contribute',
            'view'      => false,
            'create'    => false,
            'update'    => false,
            'delete'    => false,
            'full'      => false,
            'locked'    => true
        ]);

        /*********** ES-ES ****************************************************************/

        Permissions::create([
            'lang_id'    => '2',
            'name'    => 'Gerente',
            'view'      => true,
            'create'    => true,
            'update'    => true,
            'delete'    => true,
            'full'      => true,
            'locked'    => true
        ]);

        Permissions::create([
            'lang_id'    => '2',
            'name'    => 'Gerente Restrito Borrar',
            'view'      => true,
            'create'    => true,
            'update'    => true,
            'delete'    => false,
            'full'      => true,
            'locked'    => true
        ]);

        Permissions::create([
            'lang_id'    => '2',
            'name'    => 'Gerente Solamente Ver',
            'view'      => true,
            'create'    => false,
            'update'    => false,
            'delete'    => false,
            'full'      => true,
            'locked'    => true
        ]);

        Permissions::create([
            'lang_id'    => '2',
            'name'    => 'Empleado Engargado',
            'view'      => true,
            'create'    => true,
            'update'    => true,
            'delete'    => true,
            'full'      => false,
            'locked'    => true
        ]);

        Permissions::create([
            'lang_id'    => '2',
            'name'    => 'Empleado Restrito Borrar',
            'view'      => true,
            'create'    => true,
            'update'    => true,
            'delete'    => false,
            'full'      => false,
            'locked'    => true
        ]);

        Permissions::create([
            'lang_id'    => '2',
            'name'    => 'Empleado Solamente Ver',
            'view'      => true,
            'create'    => false,
            'update'    => false,
            'delete'    => false,
            'full'      => false,
            'locked'    => true
        ]);

        Permissions::create([
            'lang_id'    => '2',
            'name'    => 'Não Empleado',
            'view'      => false,
            'create'    => false,
            'update'    => false,
            'delete'    => false,
            'full'      => false,
            'locked'    => true
        ]);

        /*********** PT-BR ****************************************************************/

        Permissions::create([
            'lang_id'    => '3',
            'name'    => 'Gerente',
            'view'      => true,
            'create'    => true,
            'update'    => true,
            'delete'    => true,
            'full'      => true,
            'locked'    => true
        ]);

        Permissions::create([
            'lang_id'    => '3',
            'name'    => 'Gerente Restrição Apagar',
            'view'      => true,
            'create'    => true,
            'update'    => true,
            'delete'    => false,
            'full'      => true,
            'locked'    => true
        ]);

        Permissions::create([
            'lang_id'    => '3',
            'name'    => 'Gerente Somente Ver',
            'view'      => true,
            'create'    => false,
            'update'    => false,
            'delete'    => false,
            'full'      => true,
            'locked'    => true
        ]);

        Permissions::create([
            'lang_id'    => '3',
            'name'    => 'Colaborador Administrador',
            'view'      => true,
            'create'    => true,
            'update'    => true,
            'delete'    => true,
            'full'      => false,
            'locked'    => true
        ]);

        Permissions::create([
            'lang_id'    => '3',
            'name'    => 'Colaborador Restrição Apagar',
            'view'      => true,
            'create'    => true,
            'update'    => true,
            'delete'    => false,
            'full'      => false,
            'locked'    => true
        ]);

        Permissions::create([
            'lang_id'    => '3',
            'name'    => 'Colaborador Somente Ver',
            'view'      => true,
            'create'    => false,
            'update'    => false,
            'delete'    => false,
            'full'      => false,
            'locked'    => true
        ]);

        Permissions::create([
            'lang_id'    => '3',
            'name'    => 'Não Colaborador',
            'view'      => false,
            'create'    => false,
            'update'    => false,
            'delete'    => false,
            'full'      => false,
            'locked'    => true
        ]);

    }
}
