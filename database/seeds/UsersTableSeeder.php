<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Roles;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*************** ROLES ***************************************************************************/

        /**
         * Creating the roles default
         *ENGLISH
         */
        Roles::create([
            'name' => 'System Manager',
            'lang_id' => 1,
            'permission_id' => 1,
            'locked' => true
        ]);

        Roles::create([
            'name' => 'Manager',
            'lang_id' => 1,
            'permission_id' => 4,
            'locked' => true
        ]);

        Roles::create([
            'name' => 'Employee back office',
            'lang_id' => 1,
            'permission_id' => 5,
            'locked' => true
        ]);

        Roles::create([
            'name' => 'Employee',
            'lang_id' => 1,
            'permission_id' => 6,
            'locked' => true
        ]);

        Roles::create([
            'name' => 'Customer Demo',
            'lang_id' => 1,
            'permission_id' => 6,
            'locked' => true
        ]);

        Roles::create([
            'name' => 'Not Client',
            'lang_id' => 1,
            'permission_id' => 7,
            'locked' => true
        ]);

        Roles::create([
            'name' => 'Partner',
            'lang_id' => 1,
            'permission_id' => 7,
            'locked' => true
        ]);


        /**
         * Creandp los roles por defecto
         * SPANISH
         */
        Roles::create([
            'name' => 'Gerente del Sistema',
            'lang_id' => 2,
            'permission_id' => 8,
            'locked' => true
        ]);

        Roles::create([
            'name' => 'Encargado',
            'lang_id' => 2,
            'permission_id' => 11,
            'locked' => true
        ]);

        Roles::create([
            'name' => 'Empleado Administrativo',
            'lang_id' => 2,
            'permission_id' => 12,
            'locked' => true
        ]);

        Roles::create([
            'name' => 'Empleado',
            'lang_id' => 2,
            'permission_id' => 13,
            'locked' => true
        ]);

        Roles::create([
            'name' => 'Cliente Demo',
            'lang_id' => 2,
            'permission_id' => 13,
            'locked' => true
        ]);

        Roles::create([
            'name' => 'No Cliente',
            'lang_id' => 2,
            'permission_id' => 14,
            'locked' => true
        ]);

        Roles::create([
            'name' => 'Parcero Comercial',
            'lang_id' => 2,
            'permission_id' => 14,
            'locked' => true
        ]);


        /**
         * Criando os cargos por padrão
         * PORTUGUESE
         */
        Roles::create([
            'name' => 'Gerente do Sistema',
            'lang_id' => 3,
            'permission_id' => 15,
            'locked' => true
        ]);

        Roles::create([
            'name' => 'Encarregado',
            'lang_id' => 3,
            'permission_id' => 18,
            'locked' => true
        ]);

        Roles::create([
            'name' => 'Colaborador administrativo',
            'lang_id' => 3,
            'permission_id' => 19,
            'locked' => true
        ]);

        Roles::create([
            'name' => 'Colaborador',
            'lang_id' => 3,
            'permission_id' => 20,
            'locked' => true
        ]);

        Roles::create([
            'name' => 'Cliente Demo',
            'lang_id' => 3,
            'permission_id' => 20,
            'locked' => true
        ]);

        Roles::create([
            'name' => 'Não Cliente',
            'lang_id' => 3,
            'permission_id' => 21,
            'locked' => true
        ]);

        Roles::create([
            'name' => 'Parceiro Comercial',
            'lang_id' => 3,
            'permission_id' => 21,
            'locked' => true
        ]);

        /*************** USERS ***************************************************************************/

        /**
         * Creating the firsts Super Admin users
         *
         */
        User::create([
            'lang_id' => 3,
            'role_id' => 15,
            'company_id' => 1,
            'name' => 'André',
            'last_name' => 'R. Fernandez',
            'email' => 'andre.fz@outlook.com',
            'locked' => true,
            'password' => Hash::make('newMatrix_;:Studex')
        ]);

        User::create([
            'lang_id' => 3,
            'role_id' => 15,
            'company_id' => 1,
            'name' => 'Solange',
            'last_name' => 'Fernandez',
            'email' => 'studex.acre@gmail.com',
            'locked' => true,
            'password' => Hash::make('Sol.:123')
        ]);

        User::create([
            'lang_id' => 3,
            'role_id' => 15,
            'company_id' => 1,
            'name' => 'Antonio',
            'last_name' => 'Rodrigues',
            'email' => 'antonio.vidal@gmail.com',
            'locked' => true,
            'password' => Hash::make('Vidal.:123')
        ]);

        User::create([
            'lang_id' => 3,
            'role_id' => 15,
            'company_id' => 1,
            'name' => 'Fabiane',
            'last_name' => 'Terna Bueno',
            'email' => 'fabianebueno@outlook.com',
            'locked' => true,
            'password' => Hash::make('Fabiane.:123')
        ]);

    }
}
