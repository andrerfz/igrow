<?php

use Illuminate\Database\Seeder;

use App\Modules;
use App\ModulesLanguage;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*************** MODULE VIEW ***************************************************************************/

        /**
         * Creating the company default
         */
        Modules::create([
            'name' => 'global',
            'platform_id' => 1,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'dashboard',
            'platform_id' => 1,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'stock',
            'platform_id' => 1,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'configuration',
            'platform_id' => 1,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'employee',
            'platform_id' => 1,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'role',
            'platform_id' => 1,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'company',
            'platform_id' => 1,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'product',
            'platform_id' => 1,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'buy',
            'platform_id' => 1,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'sell',
            'platform_id' => 1,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'service',
            'platform_id' => 1,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'product_report',
            'platform_id' => 1,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'global',
            'platform_id' => 2,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'dashboard',
            'platform_id' => 2,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'stock',
            'platform_id' => 2,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'configuration',
            'platform_id' => 2,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'employee',
            'platform_id' => 2,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'role',
            'platform_id' => 2,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'company',
            'platform_id' => 2,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'product',
            'platform_id' => 2,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'buy',
            'platform_id' => 2,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'sell',
            'platform_id' => 2,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'service',
            'platform_id' => 2,
            'locked' => true
        ]);

        Modules::create([
            'name' => 'product_report',
            'platform_id' => 2,
            'locked' => true
        ]);

        ModulesLanguage::create([
            'name' => 'Global',
            'module_id' => 1,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Dashboard',
            'module_id' => 2,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Stock',
            'module_id' => 3,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Configuration',
            'module_id' => 4,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Employee',
            'module_id' => 5,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Role',
            'module_id' => 6,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Company\'s',
            'module_id' => 7,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Products',
            'module_id' => 8,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Buy',
            'module_id' => 9,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Sell',
            'module_id' => 10,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Services',
            'module_id' => 11,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Product\'s Reports',
            'module_id' => 12,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Global',
            'module_id' => 13,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Dashboard',
            'module_id' => 14,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Stock',
            'module_id' => 15,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Configuration',
            'module_id' => 16,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Employee',
            'module_id' => 17,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Role',
            'module_id' => 18,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Company\'s',
            'module_id' => 19,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Products',
            'module_id' => 20,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Buy',
            'module_id' => 21,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Sell',
            'module_id' => 22,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Services',
            'module_id' => 23,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Product\'s Reports',
            'module_id' => 24,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Global',
            'module_id' => 1,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Informe General',
            'module_id' => 2,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Stock',
            'module_id' => 3,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Configuración',
            'module_id' => 4,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Empleados',
            'module_id' => 5,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Role',
            'module_id' => 6,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Empresas',
            'module_id' => 7,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Productos',
            'module_id' => 8,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Compras',
            'module_id' => 9,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Ventas',
            'module_id' => 10,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Servicios',
            'module_id' => 11,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Informe de Productos',
            'module_id' => 12,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Global',
            'module_id' => 13,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Informe General',
            'module_id' => 14,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Stock',
            'module_id' => 15,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Configuración',
            'module_id' => 16,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Empleados',
            'module_id' => 17,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Role',
            'module_id' => 18,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Empresas',
            'module_id' => 19,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Productos',
            'module_id' => 20,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Compras',
            'module_id' => 21,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Ventas',
            'module_id' => 22,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Servicios',
            'module_id' => 23,
            'lang_id' => 2
        ]);

        ModulesLanguage::create([
            'name' => 'Informe de Productos',
            'module_id' => 24,
            'lang_id' => 1
        ]);

        ModulesLanguage::create([
            'name' => 'Global',
            'module_id' => 1,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Informe Geral',
            'module_id' => 2,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Estoque',
            'module_id' => 3,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Configurações',
            'module_id' => 4,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Colaboradores',
            'module_id' => 5,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Cargos',
            'module_id' => 6,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Empresas',
            'module_id' => 7,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Produtos',
            'module_id' => 8,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Compras',
            'module_id' => 9,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Vendas',
            'module_id' => 10,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Serviços',
            'module_id' => 11,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Informe de Produtos',
            'module_id' => 12,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Global',
            'module_id' => 13,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Informe Geral',
            'module_id' => 14,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Estoque',
            'module_id' => 15,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Configurações',
            'module_id' => 16,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Colaboradores',
            'module_id' => 17,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Cargos',
            'module_id' => 18,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Empresas',
            'module_id' => 19,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Produtos',
            'module_id' => 20,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Compras',
            'module_id' => 21,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Vendas',
            'module_id' => 22,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Serviços',
            'module_id' => 23,
            'lang_id' => 3
        ]);

        ModulesLanguage::create([
            'name' => 'Informe de Produtos',
            'module_id' => 24,
            'lang_id' => 3
        ]);

    }
}
