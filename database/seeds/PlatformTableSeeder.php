<?php

use Illuminate\Database\Seeder;

use App\Platform;
use App\PlatformLanguage;

class PlatformTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*************** Modules ***************************************************************************/

        /**
         * Creating the modules default
         */
        Platform::create([
            'name' => 'aplication_client',
            'locked' => true
        ]);

        Platform::create([
            'name' => 'backoffice_client',
            'locked' => true
        ]);

        PlatformLanguage::create([
            'name' => 'Aplication',
            'platform_id' => 1,
            'lang_id' => 1
        ]);

        PlatformLanguage::create([
            'name' => 'Aplicación',
            'platform_id' => 1,
            'lang_id' => 2
        ]);

        PlatformLanguage::create([
            'name' => 'Aplicação',
            'platform_id' => 1,
            'lang_id' => 3
        ]);

        PlatformLanguage::create([
            'name' => 'Backoffice',
            'platform_id' => 2,
            'lang_id' => 1
        ]);

        PlatformLanguage::create([
            'name' => 'Back Office',
            'platform_id' => 2,
            'lang_id' => 2
        ]);

        PlatformLanguage::create([
            'name' => 'Oficina de Apoio',
            'platform_id' => 2,
            'lang_id' => 3
        ]);

    }
}
