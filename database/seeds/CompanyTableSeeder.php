<?php

use App\Company;
use Illuminate\Database\Seeder;


class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*************** COMPANY ***************************************************************************/

        /**
         * Creating the company default
         */
        Company::create([
            'business_name' => 'Studex Regional Acre',
            'trading_name' => 'Solange F. Vidal',
            'direction' => 'Rua Gaivota II, 154',
            'town' => 'Rio Branco',
            'state' => 'Acre',
            'country' => 'Brasil',
            'postcode' => '69903206',
            'contact' => 'Solange F. Vidal',
            'mobile' => '68999711309',
            'phone' => '6832286411',
            'email' => 'studex.acre@gmail.com',
            'web' => '',
            'branch' => false,
            'retail' => true,
            'wholesale' => false,
            'locked' => true
        ]);

    }
}
