<?php

use App\Languages;
use Illuminate\Database\Seeder;


class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Languages::create([
            'name'    => 'en',
            'description'    => 'English',
        ]);

        Languages::create([
            'name'    => 'es',
            'description'    => 'Spanish',
        ]);

        Languages::create([
            'name'    => 'pt-BR',
            'description'    => 'Português Brazil',
        ]);

    }
}
