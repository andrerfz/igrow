<?php

namespace App\Http\Controllers;

use App\Albaranes;
use App\AlbaranesLineas;
use App\ApiToken;
use App\AsignacionChecklists;
use App\Checklists;
use App\Clientes;
use App\EstadosAlbaranes;
use App\EstadosFacturas;
use App\EstadosPedidos;
use App\Facturas;
use App\FacturasLineas;
use App\Locales;
use App\Pedidos;
use App\PedidosLineas;
use App\Productos;
use App\ProductosClienteProveedor;
use App\ProductosProveedores;
use App\TpvVentas;
use Illuminate\Http\Request;

use League\Flysystem\Exception;
use Response;

use App\User;
use Notificaciones;
use DB;

class ApiPublicController extends Controller
{


    public function prueba()
    {

        return 'hello';
    }


    /**
     *
     *
     *
     */
    public function sendNotification(Request $request)
    {

        if (isset($request->id_user) && isset($request->message)) {

            $usuario = User::find($request->id_user);

            if ($usuario) {

                Notificaciones::sendNotification($usuario->id, $request->message, 5, null, null);
                $this->responder(1, 'Enviada correctamente', null);
            }

        } else {

            $this->responder(0, 'ERROR: Faltan parametros. id_user y message son obligatorios ', null);
        }
    }


    /**
     * Retrieve a state list order's
     *
     */
    public function getOrderStates($token)
    {

        $verify = $this->checkToken($token);

        if ($verify['check'] && $verify['activo']) {

            $aux = [];

            $estados_pedidos = EstadosPedidos::where('activo', true)->get();

            foreach ($estados_pedidos as $item) {

                $aux[] = $this->parseStates($item);
            }

            return $this->responder(1, 'Coleccion de estados de pedidos en data', $aux);
        }

        return $this->responder(-1, 'Acceso denegado, el token no es valido o está inactivo!', null);
    }


    /**
     * Retrieve a state list of delivery notes
     *
     */
    public function getDeliveryNoteStates($token)
    {

        $verify = $this->checkToken($token);

        if ($verify['check'] && $verify['activo']) {

            $aux = [];

            $estados_albaranes = EstadosAlbaranes::where('activo', true)->get();

            foreach ($estados_albaranes as $item) {

                $aux[] = $this->parseStates($item);
            }

            return $this->responder(1, 'Coleccion de estado de albaranes en data', $aux);
        }

        return $this->responder(-1, 'Acceso denegado, el token no es valido o está inactivo!', null);
    }


    /**
     *  Retrieve a state list of bills
     *
     */
    public function getBillStates($token)
    {

        $verify = $this->checkToken($token);

        if ($verify['check'] && $verify['activo']) {

            $aux = [];

            $estados_facturas = EstadosFacturas::where('activo', true)->get();

            foreach ($estados_facturas as $item) {

                $aux[] = $this->parseStates($item);
            }

            return $this->responder(1, 'Coleccion de estados de facturas en data', $aux);
        }

        return $this->responder(-1, 'Acceso denegado, el token no es valido o está inactivo!', null);
    }


    /**
     *  Retrieve a list of provider users
     *
     */
    public function getProvidersUsers($token)
    {

        $verify = $this->checkToken($token);

        if ($verify['check'] && $verify['activo']) {

            $aux = [];

            $usuarios = User::where(['activo' => true, 'id_proveedor' => $verify['id_proveedor']])->get();

            foreach ($usuarios as $item) {

                $aux[] = $this->parseProvidersUsers($item);
            }

            return $this->responder(1, 'Coleccion de usuarios de proveedores en data', $aux);
        }

        return $this->responder(-1, 'Acceso denegado, el token no es valido o está inactivo!', null);
    }


    /**
     *  Retrieve a list of client products
     *
     */
    public function getProducts($token, $ids_above = null)
    {

        $verify = $this->checkToken($token);

        if ($verify['check'] && $verify['activo']) {

            $aux = [];

            $productos = Productos::where('activo', true)
                ->where('id_cliente', $verify['id_cliente'])
                ->where(function ($q) use ($ids_above) {

                    if ($ids_above) {

                        $q->where('id', '>', $ids_above);
                    }
                })
                ->get();

            foreach ($productos as $item) {

                $aux[] = $this->parseProducto($item);
            }

            return $this->responder(1, 'Coleccion de productos en data', $aux);
        }

        return $this->responder(-1, 'Acceso denegado, el token no es valido o está inactivo!', null);
    }


    /**
     *
     * Save or edit a client product
     *
     */
    public function setProduct($token, Request $request)
    {

        $verify = $this->checkToken($token);

        if ($verify['check'] && $verify['activo']) {

            if (isset($request->nombre) && isset($request->id_tpv) && $request->nombre && $request->id_tpv) {

                $item = null;

                if (isset($request->id) && $request->id) {

                    $item = Productos::whereNotNull('id_tpv')->where(['externo' => true, 'id_cliente' => $verify['id_cliente']])->find($request->id);

                    if ($item) {

                        if (isset($request->id_unidad) && $request->id_unidad) {
                            $item->id_unidad = $request->id_unidad;
                        }

                        if (isset($request->id_familia) && $request->id_familia) {
                            $item->id_familia = $request->id_familia;
                        }

                        $item->nombre = $request->nombre;
                        $item->save();
                    }

                } else {

                    $item = Productos::create([

                        'id_cliente' => $verify['id_cliente'],
                        'id_unidad' => isset($request->id_unidad) ? $request->id_unidad : 999,
                        'id_familia' => isset($request->id_familia) ? $request->id_familia : 999,
                        'id_iva' => isset($request->id_iva) ? $request->id_iva : 2,
                        'id_tpv' => $request->id_tpv,
                        'nombre' => $request->nombre,

                        'compra' => isset($request->compra) ? true : false,
                        'venta' => true,
                        'externo' => true,
                    ]);

                }

                if ($item && $item->id) {

                    return $this->responder(1, 'Guardado correctamente', $this->parseProducto($item));
                }

                return $this->responder(0, 'No se puede editar este producto.', null);


            } else {

                return $this->responder(0, 'Faltan datos para guardar el producto!', null);
            }

        }

        return $this->responder(-1, 'Acceso denegado, el token no es valido o está inactivo!', null);

    }


    /**
     *  Retrieve a list of provider products
     *
     */
    public function getProviderProducts($token, $ids_above = null)
    {

        $verify = $this->checkToken($token);

        if ($verify['check'] && $verify['activo']) {

            $aux = [];
            $provider_productos = ProductosProveedores::where('activo', true)
                ->where('id_proveedor', $verify['id_proveedor'])
                ->where(function ($q) use ($ids_above) {

                    if ($ids_above) {

                        $q->where('id', '>', $ids_above);
                    }
                })
                ->get();

            foreach ($provider_productos as $item) {

                $aux[] = $this->parseProviderProducts($item);
            }

            return $this->responder(1, 'Coleccion de productos del proveedor en data', $aux);
        }

        return $this->responder(-1, 'Acceso denegado, el token no es valido o está inactivo!', null);
    }


    /**
     *
     * Save or edit a provider product
     *
     */
    public function setProviderProduct($token, Request $request)
    {

        $verify = $this->checkToken($token);

        if ($verify['check'] && $verify['activo']) {


            if (isset($request->nombre) && isset($request->id_tpv) && $request->nombre && $request->id_tpv) {

                $item = null;

                if (isset($request->id) && $request->id) {

                    $item = Productos::whereNotNull('id_tpv')->where(['externo' => true, 'id_cliente' => $verify['id_cliente']])->find($request->id);

                    if ($item) {

                        $item->nombre = $request->nombre;

                        if (isset($request->descripcion) && $request->descripcion) {
                            $item->descripcion = $request->descripcion;
                        }
                        if (isset($request->referencia) && $request->referencia) {
                            $item->referencia = $request->referencia;
                        }
                        if (isset($request->cod_barras) && $request->cod_barras) {
                            $item->cod_barras = $request->cod_barras;
                        }
                        if (isset($request->ubicacion) && $request->ubicacion) {
                            $item->ubicacion = $request->ubicacion;
                        }
                        if (isset($request->precio_venta) && $request->precio_venta) {
                            $item->precio_venta = $request->precio_venta;
                        }
                        if (isset($request->precio_compra) && $request->precio_compra) {
                            $item->precio_compra = $request->precio_compra;
                        }
                        if (isset($request->es_peso) && $request->es_peso) {
                            $item->es_peso = $request->es_peso ? true : false;
                        }
                        if (isset($request->formato_cantidad) && $request->formato_cantidad) {
                            $item->formato_cantidad = $request->formato_cantidad;
                        }
                        if (isset($request->formato_cantidad_label) && $request->formato_cantidad_label) {
                            $item->formato_cantidad_label = $request->formato_cantidad_label;
                        }
                        if (isset($request->formato_minimo) && $request->formato_minimo) {
                            $item->formato_minimo = $request->formato_minimo;
                        }
                        if (isset($request->formato_multiplo) && $request->formato_multiplo) {
                            $item->formato_multiplo = $request->formato_multiplo;
                        }
                        if (isset($request->formato_multiplo_label) && $request->formato_multiplo_label) {
                            $item->formato_multiplo_label = $request->formato_multiplo_label;
                        }
                        if (isset($request->activo) && $request->activo) {
                            $item->activo = $request->activo ? true : false;
                        }

                        $item->save();
                    }

                } else {

                    $item = ProductosProveedores::create([

                        'id_proveedor' => $verify['id_proveedor'],
                        'id_unidad' => isset($request->id_unidad) ? $request->id_unidad : 999,
                        'id_familia' => isset($request->id_familia) ? $request->id_familia : 999,
                        'id_iva' => isset($request->id_iva) ? $request->id_iva : 2,

                        'nombre' => isset($request->nombre) ? $request->nombre : '',
                        'descripcion' => isset($request->descripcion) ? $request->descripcion : '',
                        'referencia' => isset($request->referencia) ? $request->referencia : '',
                        'cod_barras' => isset($request->cod_barras) ? $request->cod_barras : '',
                        'ubicacion' => isset($request->ubicacion) ? $request->ubicacion : '',
                        'precio_venta' => isset($request->precio_venta) ? $request->precio_venta : '',
                        'precio_compra' => isset($request->precio_compra) ? $request->precio_compra : '',
                        'es_peso' => isset($request->es_peso) ? true : false,
                        'formato_cantidad' => isset($request->formato_cantidad) ? $request->formato_cantidad : '',
                        'formato_cantidad_label' => isset($request->formato_cantidad_label) ? $request->formato_cantidad_label : '',
                        'formato_minimo' => isset($request->formato_minimo) ? $request->formato_minimo : '',
                        'formato_multiplo' => isset($request->formato_multiplo) ? $request->formato_multiplo : '',
                        'formato_multiplo_label' => isset($request->formato_multiplo_label) ? $request->formato_multiplo_label : '',
                        'activo' => isset($request->activo) ? true : false,

                        'externo' => true,
                    ]);

                }

                if ($item && $item->id) {

                    return $this->responder(1, 'Guardado correctamente', $this->parseProviderProducts($item));
                }

                return $this->responder(0, 'No se puede editar este producto.', null);


            } else {

                return $this->responder(0, 'Faltan datos para guardar el producto!', null);
            }

        }

        return $this->responder(-1, 'Acceso denegado, el token no es valido o está inactivo!', null);

    }


    /**
     *
     * Retrieve a list of stores that belongs to clients
     *
     */
    public function getStores($token, $ids_above = null)
    {

        $verify = $this->checkToken($token);

        if ($verify['check'] && $verify['activo']) {

            $aux = [];

            $locales = Locales::where('activo', true)
                ->where('id_cliente', $verify['id_cliente'])
                ->where(function ($q) use ($ids_above) {

                    if ($ids_above) {

                        $q->where('id', '>', $ids_above);
                    }
                })
                ->get();

            foreach ($locales as $item) {

                $aux[] = $this->parseEstablecimientos($item);
            }

            return $this->responder(1, 'Coleccion de establecimientos en data', $aux);
        }

        return $this->responder(-1, 'Acceso denegado, el token no es valido o está inactivo!', null);
    }


    /**
     *
     * Save or edit a store from a client
     *
     */
    public function setStore($token, Request $request)
    {

        $verify = $this->checkToken($token);

        if ($verify['check'] && $verify['activo']) {

            if (isset($request->nombre) && $request->nombre) {

                $item = null;

                if (isset($request->id) && $request->id) {

                    $item = Locales::where(['externo' => true, 'id_cliente' => $verify['id_cliente']])->find($request->id);

                    if ($item && $item->id) {

                        $item->nombre = $request->nombre;
                        if (isset($request->descripcion) && $request->descripcion) {
                            $item->descripcion = $request->descripcion;
                        }
                        if (isset($request->telefono) && $request->telefono) {
                            $item->telefono = $request->telefono;
                        }
                        if (isset($request->web) && $request->web) {
                            $item->web = $request->web;
                        }
                        if (isset($request->email) && $request->email) {
                            $item->email = $request->email;
                        }
                        if (isset($request->direccion) && $request->direccion) {
                            $item->direccion = $request->direccion;
                        }
                        if (isset($request->direccion_fac) && $request->direccion_fac) {
                            $item->direccion_fac = $request->direccion_fac;
                        }
                        if (isset($request->cp) && $request->cp) {
                            $item->cp = $request->cp;
                        }
                        if (isset($request->cp_fac) && $request->cp_fac) {
                            $item->cp_fac = $request->cp_fac;
                        }
                        if (isset($request->municipio) && $request->municipio) {
                            $item->municipio = $request->municipio;
                        }
                        if (isset($request->municipio_fac) && $request->municipio_fac) {
                            $item->municipio_fac = $request->municipio_fac;
                        }
                        if (isset($request->provincia) && $request->provincia) {
                            $item->provincia = $request->provincia;
                        }
                        if (isset($request->provincia_fac) && $request->provincia_fac) {
                            $item->provincia_fac = $request->provincia_fac;
                        }
                        if (isset($request->activo) && $request->activo) {
                            $item->activo = $request->activo ? true : false;
                        }

                        $item->save();
                    }

                } else {

                    $item = Locales::create([

                        'nombre' => $request->nombre,
                        'descripcion' => $request->descripcion ?: null,
                        'telefono' => $request->telefono ?: null,
                        'web' => $request->web ?: null,
                        'email' => $request->email ?: null,
                        'direccion' => $request->direccion ?: null,
                        'direccion_fac' => $request->direccion_fac ?: null,
                        'cp' => $request->cp ?: null,
                        'cp_fac' => $request->cp_fac ?: null,
                        'municipio' => $request->municipio ?: null,
                        'municipio_fac' => $request->municipio_fac ?: null,
                        'provincia' => $request->provincia ?: null,
                        'provincia_fac' => $request->provincia_fac ?: null,
                        'activo' => $request->activo ?: null,
                        'externo' => true,
                    ]);

                }

                if ($item && $item->id) {

                    return $this->responder(1, 'Guardado correctamente', $this->parseEstablecimientos($item));
                }

                return $this->responder(0, 'No se puede editar este establecimiento.', null);

            } else {

                return $this->responder(0, 'Faltan datos para guardar el establecimiento!', null);
            }

        }

        return $this->responder(-1, 'Acceso denegado, el token no es valido o está inactivo!', null);

    }


    /**
     * Retrieve a list of clients
     *
     */
    public function getClients($token, $ids_above = null)
    {

        $verify = $this->checkToken($token);

        if ($verify['check'] && $verify['activo']) {

            $aux = [];

            $clientes = Clientes::where('activo', true)
                ->where(function ($q) use ($ids_above, $verify) {

                    if ($ids_above) {

                        $q->where('id', '>', $ids_above);
                    }


                    if ($verify['id_cliente']) {

                        $q->where('id', $verify['id_cliente']);
                    }


                    if ($verify['id_proveedor']) {

                        $q->whereHas('proveedores', function ($q) use ($verify) {

                            $q->where('id', $verify['id_proveedor']);
                        });
                    }
                })
                ->get();

            foreach ($clientes as $item) {

                $aux[] = $this->parseClientes($item);
            }

            return $this->responder(1, 'Coleccion de clientes en data', $aux);
        }

        return $this->responder(-1, 'Acceso denegado, el token no es valido o está inactivo!', null);
    }


    /**
     *
     * Save or edit a client
     *
     */
    public function setClient($token, Request $request)
    {

        $verify = $this->checkToken($token);

        if ($verify['check'] && $verify['activo']) {

            if (isset($request->nombre) && $request->nombre) {

                $item = null;

                if (isset($request->id) && $request->id) {

                    $item = Clientes::where(['externo' => true])->find($request->id);

                    if ($item && $item->id) {

                        $item->nombre = $request->nombre;
                        if (isset($request->nombre_comercial) && $request->nombre_comercial) {
                            $item->nombre_comercial = $request->nombre_comercial;
                        }
                        if (isset($request->telefono) && $request->telefono) {
                            $item->telefono = $request->telefono;
                        }
                        if (isset($request->movil) && $request->movil) {
                            $item->movil = $request->movil;
                        }
                        if (isset($request->web) && $request->web) {
                            $item->web = $request->web;
                        }
                        if (isset($request->email) && $request->email) {
                            $item->email = $request->email;
                        }
                        if (isset($request->direccion) && $request->direccion) {
                            $item->direccion = $request->direccion;
                        }
                        if (isset($request->cp) && $request->cp) {
                            $item->cp = $request->cp;
                        }
                        if (isset($request->municipio) && $request->municipio) {
                            $item->municipio = $request->municipio;
                        }
                        if (isset($request->provincia) && $request->provincia) {
                            $item->provincia = $request->provincia;
                        }
                        if (isset($request->pais) && $request->pais) {
                            $item->pais = $request->pais;
                        }
                        if (isset($request->contacto) && $request->contacto) {
                            $item->contacto = $request->contacto;
                        }
                        if (isset($request->activo) && $request->activo) {
                            $item->activo = $request->activo ? true : false;
                        }

                        $item->save();
                    }

                } else {

                    $item = Clientes::create([

                        'nombre' => $request->nombre,
                        'nombre_comercial' => $request->nombre_comercial,
                        'direccion' => $request->direccion,
                        'provincia' => $request->provincia,
                        'municipio' => $request->municipio,
                        'pais' => $request->pais,
                        'cp' => $request->cp,
                        'movil' => $request->movil,
                        'telefono' => $request->telefono,
                        'email' => $request->email,
                        'web' => $request->web,
                        'contacto' => $request->contacto,
                        'activo' => true,
                        'externo' => true,
                    ]);

                }

                if ($item && $item->id) {

                    return $this->responder(1, 'Guardado correctamente', $this->parseClientes($item));
                }

                return $this->responder(0, 'No se puede editar este cliente.', null);

            } else {

                return $this->responder(0, 'Faltan datos para guardar el cliente!', null);
            }

        }

        return $this->responder(-1, 'Acceso denegado, el token no es valido o está inactivo!', null);

    }


    /**
     * Retrieve a list of order filtering for the provider that belongs to the token
     *
     * @param $token (The token belong to a specific client and this client can have one or more store house/establishment)
     *
     * @param $lines_order (Boolean value that's if's true, show all the lines from a order and if's false, not show the lines from a order)
     *
     * @param $ids_above (If a ID is passing as a parameter, the consulting will only show's the order's above that ID)
     *
     * @return array
     *
     */
    public function getOrder($token, $lines_order, $ids_above = null)
    {

        $verify = $this->checkToken($token);

        if ($verify['check'] && $verify['activo']) {

            $aux = [];

            $pedidos = Pedidos::where('id_proveedor', $verify['id_proveedor'])
                ->where(function ($q) use ($ids_above) {

                    if ($ids_above) {

                        $q->where('id', '>', $ids_above);
                    }
                })
                ->when($lines_order, function ($r) {

                    $r->with('lineas');
                })
                ->get();

            foreach ($pedidos as $item) {

                $aux[] = $this->parseOrder($item, $lines_order);
            }

            return $this->responder(1, 'Coleccion de pedidos en data', $aux);
        }

        return $this->responder(-1, 'Acceso denegado, el token no es valido o está inactivo!', null);
    }


    /**
     *
     * Save or edit a order
     *
     * @param $token
     * @param Request $request
     * @return array
     *
     */
    public function setOrder($token, Request $request)
    {

        $verify = $this->checkToken($token);

        if ($verify['check'] && $verify['activo'] && $verify['id_proveedor']) {


            /**
             * Checking parameters
             *
             * @param Request (id_order, id_user, id)
             *
             */
            if (($request->id_store && $request->id_user) || $request->id) {

                $linea = null;

                /**
                 * Edit a Order
                 *
                 * @param Request (id)
                 *
                 */
                if (isset($request->id) && $request->id) {

                    $pedido = Pedidos::where(['externo' => true])->find($request->id);

                    if ($pedido && $pedido->id) {

                        if ($request->id_state_order) {
                            $pedido->id_estado = $request->id_state_order;
                        }

                        $pedido->save();

                        /**
                         * Editing the lines from a order
                         *
                         * @param Request (lines)
                         *
                         */
                        if ($pedido && $pedido->id) {

                            $lines = json_decode($request->lines, true);

                            if (isset($lines) && $lines) {

                                foreach ($lines as $line) {

                                    //Edit products that already have been add
                                    if (isset($line['id']) && $line['id']) {

                                        $pedido_linea = PedidosLineas::where('id_pedido', $pedido->id)->find($line['id']);

                                        if ($pedido_linea && $pedido_linea->id) {

                                            if (isset($line['order_description']) && $line['order_description']) {

                                                $pedido_linea->descripcion_compra = $line->order_description;
                                            }

                                            if (isset($line['discount']) && $line['discount']) {

                                                $descuento = str_replace(',', '.', str_replace('.', '', $line['discount'] ?: 0));

                                                $pedido_linea->descuento = $descuento;
                                            }

                                            if (isset($line['price']) && $line['price']) {

                                                $precio = str_replace(',', '.', str_replace('.', '', $line['price'] ?: 0));

                                                $pedido_linea->precio = $precio;
                                            }

                                            if (isset($line['quantity']) && $line['quantity']) {

                                                $quantity = str_replace(',', '.', str_replace('.', '', $line['quantity'] ?: 0));

                                                $pedido_linea->cantidad = $quantity;
                                            }

                                            if (isset($line['confirmed_quantity']) && $line['confirmed_quantity']) {

                                                $quantity_confirmed = str_replace(',', '.', str_replace('.', '', $line['confirmed_quantity'] ?: 0));

                                                $pedido_linea->cantidad_confirmada = $quantity_confirmed;
                                            }

                                            if (isset($line['cancel_line_order']) && $line['cancel_line_order']) {

                                                $pedido_linea->delete();
                                            }

                                            $pedido_linea->save();

                                        }
                                    } else {

                                        //Add more products to the order
                                        if (isset($line->id_provider_product) && $line->id_provider_product) {

                                            $this->setLineasPedido($pedido->id, $verify, $line, $request);
                                        }
                                    }

                                }
                            } else {

                                return $this->responder(0, 'Las lineas deben estar en formato Json! Verificar las comillas altas.', null);
                            }
                        }
                    }

                } else {


                    /**
                     * Saving the order lines
                     *
                     * @param Request (lines)
                     *
                     */
                    if ($request->lines) {

                        $lines = json_decode($request->lines, true);

                        if (isset($lines) && $lines) {

                            /**
                             * Save a new order
                             *
                             * @param Request (id_user, id_store)
                             *
                             */
                            $pedido = Pedidos::create([

                                'id_usuario' => $request->id_user,
                                'id_local' => $request->id_store,
                                'id_proveedor' => $verify['id_proveedor'],
                                'is_proveedor' => true,
                                'externo' => true,
                            ]);

                            $pedido->save();

                            foreach ($lines as $line) {

                                if (isset($line['id_provider_product']) && $line['id_provider_product']) {

                                    $this->setLineasPedido($pedido->id, $verify, $line, $request);
                                }

                            }
                        } else {

                            return $this->responder(0, 'Las lineas deben estar en formato Json! Verificar las comillas altas.', null);
                        }

                    }

                }

                if ($pedido && $pedido->id) {

                    $pedido = Pedidos::with('lineas')->find($pedido->id);
                    $pedido->save();

                    return $this->responder(1, 'Guardado correctamente', $this->parseOrder($pedido, true));
                }

                return $this->responder(0, 'No se puede editar este pedido.', null);

            } else {

                return $this->responder(0, 'Faltan datos para guardar el pedido!', null);
            }

        }

        return $this->responder(-1, 'Acceso denegado, el token no es valido o está inactivo!', null);

    }


    /**
     * Retrieve a list of delivery notes filtering for the provider that belongs to the token
     *
     * @param $token (The token belong to a specific client and this client can have one or more store house/establishment)
     *
     * @param $lines_order (Boolean value that's if's true, show all the lines from a order and if's false, not show the lines from a order)
     *
     * @param $ids_above (If a ID is passing as a parameter, the consulting will only show's the order's above that ID)
     *
     * @return array
     *
     */
    public function getDeliveryNotes($token, $lines_order, $ids_above = null)
    {

        $verify = $this->checkToken($token);

        if ($verify['check'] && $verify['activo']) {

            $aux = [];

            $albaranes = Albaranes::where('id_proveedor', $verify['id_proveedor'])
                ->where(function ($q) use ($ids_above) {

                    if ($ids_above) {

                        $q->where('id', '>', $ids_above);
                    }
                })
                ->when($lines_order, function ($r) {

                    $r->with('lineas');
                })
                ->get();

            foreach ($albaranes as $item) {

                $aux[] = $this->parseDeliveryNote($item, $lines_order);
            }

            return $this->responder(1, 'Coleccion de albaranes en data', $aux);
        }

        return $this->responder(-1, 'Acceso denegado, el token no es valido o está inactivo!', null);
    }


    /**
     *
     * Save or change a delivery note
     *
     */
    public function setDeliveryNote($token, Request $request)
    {

        $verify = $this->checkToken($token);

        if ($verify['check'] && $verify['activo'] && $verify['id_proveedor']) {

            /**
             * Checking parameters
             *
             * @param Request (id_store, id_order, id_user, id)
             *
             */
            if (($request->id_order && $request->id_store && $request->id_user) || $request->id) {

                $linea = null;

                /**
                 * Edit a Delivery Note
                 *
                 * @param Request (id)
                 *
                 */
                if (isset($request->id) && $request->id) {

                    $albaran = Albaranes::where(['externo' => true])->find($request->id);

                    if ($albaran && $albaran->id) {

                        if ($request->id_state_note) {
                            $albaran->id_estado = $request->id_state_note;
                        }

                        $albaran->save();

                        /**
                         * Editing the delivery note lines
                         *
                         * @param Request (lines)
                         *
                         */
                        if ($albaran && $albaran->id && $request->lines) {

                            $lines = json_decode($request->lines, true);

                            if (isset($lines) && $lines) {

                                foreach ($lines as $line) {

                                    //Edit products that already have been add
                                    if (isset($line['id']) && $line['id']) {

                                        $albaran_linea = AlbaranesLineas::where('id_albaran', $albaran->id)->find($line['id']);

                                        if ($albaran_linea && $albaran_linea->id) {

                                            if (isset($line['discount']) && $line['discount']) {

                                                $descuento = str_replace(',', '.', $line['discount'] ?: 0);

                                                $albaran_linea->descuento = $descuento;
                                            }

                                            if (isset($line['price']) && $line['price']) {

                                                $precio = str_replace(',', '.', $line['price'] ?: 0);

                                                $albaran_linea->precio = $precio;
                                            }

                                            if (isset($line['confirmed_quantity']) && $line['confirmed_quantity']) {

                                                $quantity_confirmed = str_replace(',', '.', $line['confirmed_quantity'] ?: 0);

                                                $albaran_linea->cantidad_confirmada = $quantity_confirmed;

                                                $albaran_linea->lineapedido->cantidad_confirmada += $quantity_confirmed;

                                                $albaran_linea->lineapedido->update();
                                            }

                                            $albaran->save();
                                            $albaran_linea->save();
                                        }
                                    }

                                }

                            } else {

                                return $this->responder(0, 'Las lineas deben estar en formato Json! Verificar las comillas altas.', null);
                            }
                        }
                    }

                } else {

                    /**
                     * Retrieving the delivery note lines
                     *
                     * @param Request (lines)
                     *
                     */
                    if ($request->lines) {

                        $request_lines = json_decode($request->lines, true);

                        if (!isset($request_lines) && !$request_lines) {

                            return $this->responder(0, 'Las lineas deben estar en formato Json! Verificar las comillas altas.', null);
                        }
                    }

                    $pedido = Pedidos::with(['lineas' => function ($q) {
                        $q->with('articulo');
                    }])->find($request->id_order);

                    /**
                     * Creating a delivery note
                     *
                     * @param Request (id_user, id_store)
                     *
                     */
                    $albaran = Albaranes::create([

                        'id_usuario' => $request->id_user,
                        'id_local' => isset($request->id_store) && $request->id_store ?: $pedido->id_local,
                        'id_proveedor' => $verify['id_proveedor'],
                        'is_proveedor' => true,
                        'externo' => true,
                    ]);

                    $albaran->save();


                    /**
                     * Saving the delivery note lines
                     *
                     * @param Request (lines)
                     *
                     */
                    if ($pedido && $pedido->lineas) {

                        foreach ($pedido->lineas as $line) {

                            $this->setLineasAlbaran($albaran->id, $line, $request, $request_lines ?: null);
                        }
                    }
                }

                if ($albaran && $albaran->id) {

                    $albaran = Albaranes::with('lineas')->find($albaran->id);
                    $albaran->save();

                    return $this->responder(1, 'Guardado correctamente', $this->parseDeliveryNote($albaran, true));
                }

                return $this->responder(0, 'No se puede editar este albarán.', null);

            } else {

                return $this->responder(0, 'Faltan datos para guardar el albarán!', null);
            }

        }

        return $this->responder(-1, 'Acceso denegado, el token no es valido o está inactivo!', null);

    }


    /**
     * Retrieve a list of bill filtering for the provider that belongs to the token
     *
     * @param $token (The token belong to a specific client and this client can have one or more store house/establishment)
     *
     * @param $lines_order (Boolean value that's if's true, show all the lines from a order and if's false, not show the lines from a order)
     *
     * @param $ids_above (If a ID is passing as a parameter, the consulting will only show's the order's above that ID)
     *
     * @return array
     *
     */
    public function getBills($token, $lines_order, $ids_above = null)
    {

        $verify = $this->checkToken($token);

        if ($verify['check'] && $verify['activo']) {

            $aux = [];

            $facturas = Facturas::where('id_proveedor', $verify['id_proveedor'])
                ->where(function ($q) use ($ids_above) {

                    if ($ids_above) {

                        $q->where('id', '>', $ids_above);
                    }
                })
                ->when($lines_order, function ($r) {

                    $r->with('lineas');
                })
                ->get();

            foreach ($facturas as $item) {

                $aux[] = $this->parseBill($item, $lines_order);
            }

            return $this->responder(1, 'Coleccion de facturas en data', $aux);
        }

        return $this->responder(-1, 'Acceso denegado, el token no es valido o está inactivo!', null);
    }


    /**
     *
     * Save or change a bill
     *
     */
    public function setBill($token, Request $request)
    {

        $verify = $this->checkToken($token);

        if ($verify['check'] && $verify['activo'] && $verify['id_proveedor']) {

            /**
             * Checking parameters
             *
             * @param Request (id_client, ids_delivery_note, id_user, id)
             *
             */
            if (($request->ids_delivery_note && $request->id_client && $request->id_user) || $request->id) {

                $linea = null;

                /**
                 * Editing a Bill
                 *
                 * @param Request (id)
                 *
                 */
                if (isset($request->id) && $request->id) {

                    $factura = Facturas::where(['externo' => true])->find($request->id);

                    if ($factura && $factura->id) {

                        if ($request->id_state_bill) {
                            $factura->id_estado = $request->id_state_bill;
                        }

                        $factura->save();

                        /**
                         * Editing the bill lines
                         *
                         * @param Request (lines)
                         *
                         */
                        if ($factura && $factura->id && $request->lines) {

                            $lines = json_decode($request->lines, true);

                            if (isset($lines) && $lines) {

                                foreach ($lines as $line) {

                                    //Edit products that already have been add
                                    if (isset($line['id']) && $line['id']) {

                                        $factura_linea = FacturasLineas::where('id_factura', $factura->id)->find($line['id']);

                                        if ($factura_linea && $factura_linea->id) {

                                            if (isset($line['discount']) && $line['discount']) {

                                                $descuento = str_replace(',', '.', $line['discount'] ?: 0);

                                                $factura_linea->descuento = $descuento;
                                            }

                                            if (isset($line['price']) && $line['price']) {

                                                $precio = str_replace(',', '.', $line['price'] ?: 0);

                                                $factura_linea->precio = $precio;
                                            }

                                            if (isset($line['quantity']) && $line['quantity']) {

                                                $quantity = str_replace(',', '.', $line['quantity'] ?: 0);

                                                $factura_linea->cantidad = $quantity;
                                            }

                                            $factura->save();
                                            $factura_linea->save();
                                        }
                                    }

                                }
                            } else {

                                return $this->responder(0, 'Las lineas deben estar en formato Json! Verificar las comillas altas.', null);
                            }
                        }
                    }

                } else {


                    /**
                     * Retrieving the delivery note lines
                     *
                     * @param Request (lines)
                     *
                     */
                    if ($request->lines) {

                        $request_lines = json_decode($request->lines, true);

                        if (!isset($request_lines) && !$request_lines) {

                            return $this->responder(0, 'Las lineas deben estar en formato Json! Verificar las comillas altas.', null);
                        }
                    }


                    /**
                     * Retrieving the IDS from delivery note request
                     *
                     * @param Request (ids_delivery_note)
                     *
                     */
                    if ($request->ids_delivery_note) {

                        $request_ids_delivery_note = json_decode($request->ids_delivery_note, true);

                        if (!isset($request_ids_delivery_note) && !$request_ids_delivery_note && !is_array($request_ids_delivery_note)) {

                            return $this->responder(0, 'Los IDs deben estar en un array ["1","2"]! Verificar el formateado.', null);
                        }

                        $albaranes = Albaranes::with(['lineas' => function ($q) {
                            $q->with('articulo');
                        }])
                            ->whereIn('id', $request_ids_delivery_note)
                            ->get();

                        /**
                         * Creating a bill
                         *
                         * @param Request (id_user, id_store)
                         *
                         */
                        $factura = Facturas::create([

                            'id_usuario' => $request->id_user,
                            'id_cliente' => $request->id_client,
                            'id_local' => isset($request->id_store) && $request->id_store ?: null,
                            'id_proveedor' => $verify['id_proveedor'],
                            'is_proveedor' => true,
                            'externo' => true,
                        ]);

                        $factura->save();


                        /**
                         * Saving the order lines
                         *
                         * @param Request (lines)
                         *
                         */
                        if ($albaranes) {

                            foreach ($albaranes as $albaran) {

                                foreach ($albaran->lineas as $lineas) {

                                    $this->setLineasBill($factura->id, $lineas, $request, $request_lines ?: null);
                                }
                            }
                        }
                    }
                }

                if ($factura && $factura->id) {

                    $factura = Facturas::with('lineas')->find($factura->id);
                    $factura->save();

                    return $this->responder(1, 'Guardado correctamente', $this->parseBill($factura, true));
                }

                return $this->responder(0, 'No se puede editar esta factura.', null);

            } else {

                return $this->responder(0, 'Faltan datos para guardar la factura!', null);
            }

        }

        return $this->responder(-1, 'Acceso denegado, el token no es valido o está inactivo!', null);

    }


    /**
     * Guarda las ventas que se mandan desde el tpv
     *
     */
    public function setSells($token, Request $request)
    {

        $verify = $this->checkToken($token);

        if ($verify['check'] && $verify['activo']) {

            if (isset($request->ventas) && is_array($request->ventas)) {

                DB::beginTransaction();

                try {

                    foreach ($request->ventas as $item) {

                        TpvVentas::create([
                            'id_producto' => $item['id_producto'],
                            'cantidad' => $item['cantidad'],
                            'ticket' => isset($item['ticket']) ? $item['ticket'] : null,
                            'fecha' => isset($item['fecha']) ? $item['fecha'] : null,
                            'fecha_hora' => isset($item['fecha_hora']) ? $item['fecha_hora'] : null,
                            'observaciones' => isset($item['observaciones']) ? $item['observaciones'] : null
                        ]);
                    }

                    DB::commit();

                } catch (\Exception $e) {

                    DB::rollBack();
                    return $this->responder(0, 'Error en los datos enviados', null);
                }

                return $this->responder(1, 'Las ventas se han guardado correctamente!', null);
            }

            return $this->responder(0, 'Ventas no es un array', null);
        }

        return $this->responder(-1, 'Acceso denegado, el token no es valido o está inactivo!', null);
    }


    /**
     *
     *
     */
    public function cron()
    {

        return $this->asignacionChecklist();
    }


    private function asignacionChecklist()
    {
        $hoy = date('Y-m-d');

        try {
            $asignaciones = AsignacionChecklists::with(['periodo', 'modelo'])
                ->where('activo', true)
                ->where(function ($q) use ($hoy) {

                    $q->whereNull('fecha_fin')->orWhere('fecha_fin', '>=', $hoy);
                })
                ->get();

            foreach ($asignaciones as $asignacion) {

                $fecha_inicio = $hoy;
                $fecha_final = date('Y-m-d', strtotime($hoy . '+ ' . $asignacion->prevision_dias . ' days'));

                if ($asignacion->fecha_fin && $fecha_final > $asignacion->fecha_fin) {

                    $fecha_final = $asignacion->fecha_fin;
                }

                do {


                    //buscamos la ultima vez que se genero un checklist de esta asignacion
                    $last_date_gen = Checklists::where('id_asignacion', $asignacion->id)->max('fecha_inicio');
                    $primero = false;

                    if (!$last_date_gen) {
                        //si no hay fecha de ultimo sinifnica que es el primero por lo tanto cogemos la fecha de la asignacion
                        $last_date_gen = $asignacion->fecha_inicio;
                        $primero = true;
                    }


                    //sumamos el perido para comprobar que entra dentro del periodo de prevision
                    $next_date_gen = date('Y-m-d', strtotime($last_date_gen . '+ ' . ($asignacion->periodo->meses ? $asignacion->periodo->meses . ' months' : $asignacion->periodo->dias . ' days')));

                    if ($next_date_gen <= $fecha_final) {

                        $nuevo = $asignacion->checklists()->create([
                            'fecha_inicio' => $primero ? $asignacion->fecha_inicio : $next_date_gen,
                            'fecha_ejecucion' => $primero ? $asignacion->fecha_inicio : $next_date_gen,
                            'allday' => $asignacion->allday,
                            'hora_inicio' => $asignacion->hora_inicio
                        ]);

                        $nuevo->roles()->sync($asignacion->roles->pluck('id')->toArray());
                        $nuevo->usuarios()->sync($asignacion->usuarios->pluck('id')->toArray());
                        $nuevo->supervisores()->sync($asignacion->supervisores->pluck('id')->toArray());

                        //Notificaciones::sendNotification($asignacion->id_usuario, 'Nuevo Checklist', 'Tienes un nuevo checklist asignado', 2, $nuevo->id);
                        //Notificaciones::sendNotification($asignacion->id_supervisor, 'Nuevo Checklist', 'Tienes un nuevo checklist asignado como supervisor', 2, $nuevo->id);
                    }


                    //saltamos a la siguiente fecha y comrpbamos que esta dentro del periodo de prevision
                    if ($asignacion->periodo->meses) {

                        $fecha_inicio = date('Y-m-d', strtotime($fecha_inicio . '+ ' . $asignacion->periodo->meses . ' months'));

                    } else if ($asignacion->periodo->dias) {

                        $fecha_inicio = date('Y-m-d', strtotime($fecha_inicio . '+ ' . $asignacion->periodo->dias . ' days'));
                    }


                } while ($fecha_inicio <= $fecha_final);
            }
        } catch (\Exception $e) {

            return $e->getMessage();
        }

        return 'done! ' . $hoy;
    }



    /**
     *
     * TODAS LOS METODÓS PARSE
     *
     * @PARAM THE RESULTS OF A QUERY
     *
     * @RETURN ARRAY
     *
     */


    /**
     * RETURS A JSON WITH INFORMATION FROM TABLE LOCALES
     *
     */
    private function parseEstablecimientos($item)
    {

        return [
            'id' => $item->id,
            'nombre' => $item->nombre,
            'descripcion' => $item->descripcion,
            'telefono' => $item->telefono,
            'web' => $item->web,
            'email' => $item->email,
            'direccion' => $item->direccion,
            'direccion_fac' => $item->direccion_fac,
            'cp' => $item->cp,
            'cp_fac' => $item->cp_fac,
            'municipio' => $item->municipio,
            'municipio_fac' => $item->municipio_fac,
            'provincia' => $item->provincia,
            'provincia_fac' => $item->provincia_fac,
            'activo' => $item->activo,
            'externo' => $item->externo
        ];
    }


    /**
     * RETURS A JSON WITH INFORMATION FROM TABLE CLIENTES
     *
     */
    private function parseClientes($item)
    {

        return [
            'id' => $item->id,
            'nombre' => $item->nombre,
            'nombre_comercial' => $item->nombre_comercial,
            'cif' => $item->cif,
            'direccion' => $item->direccion,
            'provincia' => $item->provincia,
            'municipio' => $item->municipio,
            'pais' => $item->pais,
            'cp' => $item->cp,
            'movil' => $item->movil,
            'telefono' => $item->telefono,
            'email' => $item->email,
            'web' => $item->web,
            'contacto' => $item->contacto,
            'activo' => $item->activo,
            'externo' => $item->externo
        ];
    }


    /**
     * RETURS A JSON WITH INFORMATION FROM TABLE PEDIDOS
     *
     */
    private function parseOrder($item, $with_lines)
    {

        $aux_lineas = [];

        foreach ($item->lineas as $linea) {

            if ($with_lines) {

                $aux_lineas[] = [
                    'id' => $linea->id,
                    'id_pedido' => $linea->id_pedido,
                    'id_producto' => $linea->id_producto,
                    'id_producto_proveedor' => $linea->id_producto_proveedor,
                    'referencia' => $linea->referencia,
                    'formato' => $linea->formato,
                    'formato_compra' => $linea->formato_compra,
                    'descripcion_compra' => $linea->descripcion_compra,
                    'formato_multiplo' => $linea->formato_multiplo,
                    'formato_multiplo_label' => $linea->formato_multiplo_label,
                    'formato_minimo' => $linea->formato_minimo,
                    'precio' => $linea->precio,
                    'descuento' => $linea->descuento,
                    'iva' => $linea->iva,
                    'cantidad' => $linea->cantidad,
                    'cantidad_confirmada' => $linea->cantidad_confirmada,
                    'cod_trazabilidad' => $linea->cod_trazabilidad
                ];
            }
        }

        $aux = [
            'id' => $item->id,
            'id_establecimiento' => $item->id_local,
            'id_proveedor' => $item->id_proveedor,
            'id_usuario' => $item->id_usuario,
            'id_estado' => $item->id_estado,
            'descripcion' => $item->descripcion,
            'importe' => $item->importe,
            'importe_iva' => $item->importe_iva,
            'importe_impuesto' => $item->importe_impuesto,
            'externo' => $item->externo,
            'pedido_lineas' => $aux_lineas
        ];

        return $aux;
    }


    /**
     * RETURS A JSON WITH INFORMATION FROM TABLE ALBARAN
     *
     */
    private function parseDeliveryNote($item, $with_lines)
    {

        $aux_lineas = [];

        foreach ($item->lineas as $linea) {

            if ($with_lines) {

                $aux_lineas[] = [
                    'id' => $linea->id,
                    'id_pedido' => $linea->id_pedido,
                    'id_linepedido' => $linea->id_linepedido,
                    'id_albaran' => $linea->id_albaran,
                    'id_facturas' => $linea->id_facturas,
                    'id_usuario' => $linea->id_usuario,
                    'id_producto' => $linea->id_producto,
                    'id_producto_proveedor' => $linea->id_producto_proveedor,
                    'referencia' => $linea->referencia,
                    'formato' => $linea->formato,
                    'formato_compra' => $linea->formato_compra,
                    'precio' => $linea->precio,
                    'descuento' => $linea->descuento,
                    'iva' => $linea->iva,
                    'cantidad' => $linea->cantidad,
                    'cantidad_confirmada' => $linea->cantidad_confirmada,
                    'cod_trazabilidad' => $linea->cod_trazabilidad
                ];

            }
        }

        $aux = [
            'id' => $item->id,
            'id_establecimiento' => $item->id_local,
            'id_proveedor' => $item->id_proveedor,
            'id_usuario' => $item->id_usuario,
            'id_estado' => $item->id_estado,
            'id_facturas' => $item->id_facturas,
            'descripcion' => $item->descripcion,
            'importe' => $item->importe,
            'importe_iva' => $item->importe_iva,
            'importe_impuesto' => $item->importe_impuesto,
            'repartidor_nombre' => $item->repartidor_nombre,
            'repartidor_dni' => $item->repartidor_dni,
            'cerrado' => $item->cerrado,
            'editado_por' => $item->editado_por,
            'cerrado_por' => $item->cerrado_por,
            'externo' => $item->externo,
            'albaran_lineas' => $aux_lineas
        ];

        return $aux;
    }


    /**
     * RETURS A JSON WITH INFORMATION FROM TABLE FACTURA
     *
     */
    private function parseBill($item, $with_lines)
    {

        $aux_lineas = [];

        foreach ($item->lineas as $linea) {

            if ($with_lines) {

                $aux_lineas[] = [
                    'id' => $linea->id,
                    'id_factura' => $linea->id_pedido,
                    'id_lineaalbaran' => $linea->id_linepedido,
                    'id_usuario' => $linea->id_usuario,
                    'id_producto' => $linea->id_producto,
                    'id_producto_proveedor' => $linea->id_producto_proveedor,
                    'referencia' => $linea->referencia,
                    'formato' => $linea->formato,
                    'formato_compra' => $linea->formato_compra,
                    'precio' => $linea->precio,
                    'descuento' => $linea->descuento,
                    'iva' => $linea->iva,
                    'cantidad' => $linea->cantidad,
                    'cantidad_confirmada' => $linea->cantidad_confirmada,
                    'cod_trazabilidad' => $linea->cod_trazabilidad
                ];
            }
        }

        $aux = [
            'id' => $item->id,
            'id_establecimiento' => $item->id_local,
            'id_cliente' => $item->id_cliente,
            'id_proveedor' => $item->id_proveedor,
            'id_usuario' => $item->id_usuario,
            'id_estado' => $item->id_estado,
            'id_facturas' => $item->id_facturas,
            'descripcion' => $item->descripcion,
            'fecha' => $item->fecha,
            'iva4' => $item->iva4,
            'iva10' => $item->iva10,
            'iva21' => $item->iva21,
            'importe' => $item->importe,
            'importe_total' => $item->importe_total,
            'descuento_total' => $item->descuento_total,
            'importe_impuesto' => $item->importe_impuesto,
            'repartidor_nombre' => $item->repartidor_nombre,
            'repartidor_dni' => $item->repartidor_dni,
            'cerrado' => $item->cerrado,
            'editado_por' => $item->editado_por,
            'cerrado_por' => $item->cerrado_por,
            'manual' => $item->manual,
            'num_cuenta' => $item->num_cuenta,
            'externo' => $item->externo,
            'factura_lineas' => $aux_lineas
        ];


        return $aux;
    }


    /**
     * RETURS A JSON WITH INFORMATION FROM TABLE PRODUCTO
     *
     */
    private function parseProducto($item)
    {

        return [
            'id' => $item->id,
            'id_tpv' => $item->id_tpv,
            'nombre' => $item->nombre,
            'venta' => $item->venta,
            'compra' => $item->compra,
            'externo' => $item->externo
        ];
    }


    /**
     * RETURS A JSON WITH INFORMATION FROM TABLE PROVIDER PRODUCTS
     *
     */
    private function parseProviderProducts($item)
    {

        return [
            'id' => $item->id,
            'id_proveedor' => $item->id_proveedor,
            'id_familia_ventas' => $item->id_familia_ventas,
            'id_iva' => $item->id_iva,
            'id_unidad' => $item->id_unidad,
            'nombre' => $item->nombre,
            'descripcion' => $item->descripcion,
            'referencia' => $item->referencia,
            'cod_barras' => $item->cod_barras,
            'ubicacion' => $item->ubicacion,
            'precio_venta' => $item->precio_venta,
            'precio_compra' => $item->precio_compra,
            'es_peso' => $item->es_peso,
            'formato_cantidad' => $item->formato_cantidad,
            'formato_cantidad_label' => $item->formato_cantidad_label,
            'formato_minimo' => $item->formato_minimo,
            'formato_multiplo' => $item->formato_multiplo,
            'formato_multiplo_label' => $item->formato_multiplo_label,
            'activo' => $item->activo,
            'externo' => $item->externo
        ];
    }


    /**
     * RETURS A JSON WITH INFORMATION FROM TABLE ESTADO PEDIDOS / ALBARAN / FACTURAS
     *
     */
    private function parseStates($item)
    {

        return [
            'id' => $item->id,
            'id_estado' => $item->id_estado,
            'nombre_proveedor' => $item->nombre_proveedor,
            'nombre_cliente' => $item->nombre_cliente,
            'cambio_estado' => $item->cambio_estado,
            'activo' => $item->activo
        ];
    }


    /**
     * RETURS A JSON WITH INFORMATION FROM TABLE USERS
     *
     */
    private function parseProvidersUsers($item)
    {

        return [
            'id' => $item->id,
            'name' => $item->name,
            'last_name' => $item->last_name,
            'nombre' => $item->full_name,
            'dni' => $item->dni,
            'telefono' => $item->telefono,
            'email' => $item->email,
            'activo' => $item->activo,
        ];
    }


    /**
     * SET INFORMATION ON THE TABLE PEDIDOS LINEAS
     *
     */
    private function setLineasPedido($pedido_id, $verify, $line, $request)
    {

        $producto_prove = ProductosProveedores::where('id_proveedor', $verify['id_proveedor'])->find($line['id_provider_product']);

        $producto_cliente = ProductosClienteProveedor::where('id_producto_proveedor', $producto_prove ? $producto_prove->id : null)->first();

        $cliente = new ProveedoresController;

        $search = $cliente->consultingProductosLocales('', $request->id_store, $line['id_provider_product']);

        $precio_producto = $search && $search['precioFinal'] ? $search['precioFinal'] : $producto_prove->precio_venta;

        $linea = PedidosLineas::create([

            'id_pedido' => $pedido_id,
            'id_usuario' => $request->id_user,

            'id_producto' => $producto_cliente ? $producto_cliente->id_producto : null,
            'id_producto_proveedor' => $producto_prove->id ?: null,

            'id_unidad' => $producto_prove->id_unidad ?: null,
            'iva' => $producto_prove->iva ? $producto_prove->iva->iva : null,

            'referencia' => $producto_prove->referencia,
            'formato' => $producto_prove->formato,
            'formato_compra' => $producto_prove->formato_compra,
            'descripcion_compra' => $producto_prove->descripcion_compra,
            'formato_multiplo' => $producto_prove->formato_multiplo,
            'formato_minimo' => $producto_prove->formato_minimo,
            'precio' => $precio_producto ?: 0,
            'cantidad' => $line['quantity'] ?: 0,
            'descuento' => $line['discount'] ?: 0,

            'cantidad_confirmada' => 0
        ]);

        $linea->save();

        return $linea;
    }


    /**
     * SET INFORMATION ON THE TABLE ALBARAN LINEAS
     *
     */
    private function setLineasAlbaran($albaran_id, $line, $request, $request_lines)
    {

        $confirmed_quantity = null;
        $discount = null;
        $price = null;

        if ($request_lines) {

            foreach ($request_lines as $ln) {

                if (isset($ln['id_order_line']) && $ln['id_order_line'] == $line->id) {

                    $price = str_replace(',', '.', isset($ln['price']) ? $ln['price'] : 0);
                    $confirmed_quantity = isset($ln['confirmed_quantity']) ? $ln['confirmed_quantity'] : 0;
                    $discount = str_replace(',', '.', isset($ln['discount']) ? $ln['discount'] : 0);
                }
            }
        }

        $albaran_linea = AlbaranesLineas::create([
            'id_usuario' => $request->id_user,
            'id_pedido' => $request->id_order,
            'id_linepedido' => $line->id ?: null,
            'id_albaran' => $albaran_id,
            'id_producto' => $line->id_producto ?: null,
            'id_producto_proveedor' => $line->id_producto_proveedor ?: null,
            'id_unidad' => $line->id_unidad ?: null,
            'referencia' => $line->referencia,
            'formato' => $line->formato,
            'formato_compra' => $line->formato_compra,
            'precio' => $price ?: $line->precio,
            'precio_pactado' => 0,
            'descuento' => $discount ?: $line->descuento,
            'iva' => $line->iva ?: null,
            'cantidad' => $line->cantidad,
            'cantidad_confirmada' => $confirmed_quantity ?: 0
        ]);

        $albaran_linea->save();

        return $albaran_linea;
    }


    /**
     * SET INFORMATION ON THE TABLE FACTURA LINEAS
     *
     */
    private function setLineasBill($factura_id, $line, $request, $request_lines)
    {

        $quantity = null;
        $discount = null;
        $price = null;

        if ($request_lines) {

            foreach ($request_lines as $ln) {

                if (isset($ln['id_delivery_line']) && $ln['id_delivery_line'] == $line->id) {

                    $price = str_replace(',', '.', isset($ln['price']) ? $ln['price'] : 0);
                    $quantity = isset($ln['quantity']) ? $ln['quantity'] : 0;
                    $discount = str_replace(',', '.', isset($ln['discount']) ? $ln['discount'] : 0);
                }
            }
        }

        $factura_linea = FacturasLineas::create([
            'id_usuario' => $request->id_user,
            'id_factura' => $factura_id,
            'id_lineaalbaran' => $line->id ?: null,
            'id_producto' => $line->id_producto ?: null,
            'id_producto_proveedor' => $line->id_producto_proveedor ?: null,
            'id_unidad' => $line->id_unidad ?: null,
            'referencia' => $line->referencia,
            'formato' => $line->formato,
            'formato_compra' => $line->formato_compra,
            'precio' => $price ?: $line->precio,
            'precio_pactado' => 0,
            'descuento' => $discount ?: $line->descuento,
            'iva' => $line->iva ?: null,
            'cantidad' => $quantity ?: $line->cantidad,
            'cantidad_confirmada' => 0
        ]);

        $factura_linea->save();

        return $factura_linea;
    }


    /**
     * Comprueba si existe el token
     *
     */
    private function checkToken($token)
    {

        $existe = ApiToken::where(['token' => $token, 'activo' => true])->first();

        $activo = $existe ? ($existe->activo ? true : false) : null;

        $id_cliente = $existe ? $existe->id_cliente : null;

        $id_proveedor = $existe ? $existe->id_proveedor : null;

        return $existe ? ['id_cliente' => $id_cliente, 'id_proveedor' => $id_proveedor, 'check' => true, 'activo' => $activo] : ['id_cliente' => $id_cliente, 'id_proveedor' => $id_proveedor, 'check' => false, 'activo' => $activo];
    }


    /**
     * Devuelve la respuesta de la llamada
     *
     *
     */
    private function responder($status, $mensaje, $data = [])
    {

        return Response::json(array(
            'status' => $status,
            'message' => $mensaje,
            'data' => $data
        ));
    }
}
