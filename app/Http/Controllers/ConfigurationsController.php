<?php

namespace App\Http\Controllers;

use App\Permissions;
use App\Company;
use FlyingLuscas\ViaCEP\ViaCEP;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Response;
use View;
use GeneralHelper;

class ConfigurationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @param void
     */
    public function __construct()
    {
        //$this->middleware('auth, set.locale, auth.permissions');
    }


    /****** INDEX **************************************************************************************************/


    /**
     * Show the Company Branch's and Profiles page.
     *
     * @return NotFoundHttpException|bool|View
     */
    public function indexAdministration()
    {
        if (!auth()->user()->permissions->backoffice_client->company->view) {
            return GeneralHelper::checkPermissionErrorReturn();
        }
        $all_company = Company::all();

        if ($all_company) {
            $permissions = Permissions::where(['full' => false, 'active' => true])->orderBy('name')->get();
            $company = Company::whereNull('company_id')->first();
            $language = auth()->user()->language->name;
            $en = auth()->user()->language->check->en;
            $es = auth()->user()->language->check->es;
            $ptBR = auth()->user()->language->check->ptBR;

            return view('administration.companys.companys', compact('all_company', 'company', 'permissions', 'en', 'es', 'ptBR', 'language'));
        }

        return GeneralHelper::checkRegisterErrorReturn();
    }


    /**
     * Consult the PostCode From
     *
     * @param Request $request
     * @return mixed
     */
    public function getPostCode(Request $request)
    {
        if (strlen($request->postcode) > 5) {
            $zipCodeClass = new ViaCEP;
            $zipCodeInfo = $zipCodeClass->findByZipCode($request->postcode)->toJson();

            return $zipCodeInfo;
        }

        return [];
    }
}
