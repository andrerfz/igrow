<?php

namespace App\Http\Controllers;

use App\Company;
use App\Permissions;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Response;
use PermissionsHelper;
use GeneralHelper;
use Route;
use View;
use Exception;

class CompanyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @param void
     */
    public function __construct()
    {
        //$this->middleware('auth, set.locale, auth.permissions');
    }


    /****** INDEX **************************************************************************************************/


    /**
     * Show the Company Branch's and Profiles page.
     *
     * @return bool|NotFoundHttpException|View
     */
    public function index()
    {
        if (!auth()->user()->permissions->backoffice_client->company->view) {
            return GeneralHelper::checkPermissionErrorReturn();
        }
        $all_company = Company::all();

        if ($all_company) {
            $permissions = Permissions::where(['full' => false, 'active' => true])->orderBy('name')->get();
            $company = Company::whereNull('company_id')->first();
            $language = auth()->user()->language->name;
            $en = auth()->user()->language->check->en;
            $es = auth()->user()->language->check->es;
            $ptBR = auth()->user()->language->check->ptBR;

            return view('company.profile', compact('all_company', 'company', 'permissions', 'en', 'es', 'ptBR', 'language'));
        }

        return GeneralHelper::checkRegisterErrorReturn();
    }


    /**
     * Show the Company Branch's and Profiles page.
     *
     * @return bool|NotFoundHttpException|View
     */
    public function indexDetail()
    {
        if (!auth()->user()->permissions->aplication_client->configuration->view) {
            return GeneralHelper::checkPermissionErrorReturn();
        }
        $route = Route::current();
        $id = $route->parameter('id');
        $company = Company::find($id);

        if (isset($company->id)) {
            $language = auth()->user()->language->name;
            $en = auth()->user()->language->check->en;
            $es = auth()->user()->language->check->es;
            $ptBR = auth()->user()->language->check->ptBR;

            return view('company.details', compact('company', 'en', 'es', 'ptBR', 'language'));
        }

        return GeneralHelper::checkRegisterErrorReturn();
    }


    /**
     * Show the Company Branch's and Profiles page.
     *
     * @param Request $request
     * @return bool|NotFoundHttpException|View
     */
    public function indexAdministration(Request $request)
    {
        if (!auth()->user()->permissions->backoffice_client->company->view) {
            return GeneralHelper::checkPermissionErrorReturn();
        }
        $request->company_id ? $all_company = Company::find($request->company_id) : $all_company = Company::all();

        if ($all_company) {
            $permissions = Permissions::where(['full' => false, 'active' => true])->orderBy('name')->get();
            $companys = Company::all();
            $language = auth()->user()->language->name;
            $en = auth()->user()->language->check->en;
            $es = auth()->user()->language->check->es;
            $ptBR = auth()->user()->language->check->ptBR;

            return view('administration.company.companys', compact('all_company', 'companys', 'permissions', 'en', 'es', 'ptBR', 'language'));
        }

        return GeneralHelper::checkRegisterErrorReturn();
    }



    /****** GET **************************************************************************************************/


    /**
     * Retrieve companys for DataTables
     *
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function getCompanys(Request $request)
    {
        $aux = [];
        $companys = Company::withTrashed()->where(function (Company $q) use ($request) {
            if ($request->state_filter == 1) {
                $q->where('active', true);
            }
            if ($request->state_filter == 2) {
                $q->where('active', false);
            }
            if ($request->recycle_filter === 'true') {
                $q->where('deleted_at', '!=', NULL);
            } else {
                $q->where('deleted_at', NULL);
            }
        })->with(['users'])->get();

        $permissions = auth()->user()->permissions->aplication_client->role;

        foreach ($companys as $company) {
            $aux[] = $this->parseCompanys($company, $request, $permissions);
        }

        return response()->json(['data' => $aux]);
    }


    /**
     * Retrieve companys for DataTables
     *
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function getCompanysAdministration(Request $request)
    {
        $aux = [];
        $companys = Company::withTrashed()->withoutGlobalScope('CompanyScope')->where(function (Builder $q) use ($request) {
            if ($request->company_filter) {
                $q->where('id', $request->company_filter);
            }
            if ($request->state_filter == 1) {
                $q->where('active', true);
            }
            if ($request->state_filter == 2) {
                $q->where('active', false);
            }
            if ($request->recycle_filter === 'true') {
                $q->where('deleted_at', '!=', NULL);
            } else {
                $q->where('deleted_at', NULL);
            }
        })->with(['users'])->get();

        $permissions = auth()->user()->permissions->aplication_client->role;

        foreach ($companys as $company) {
            $aux[] = $this->parseCompanys($company, $request, $permissions);
        }

        return response()->json(['data' => $aux]);
    }



    /****** SET **************************************************************************************************/


    /**
     * Edit a Company Profile
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function setCompanyProfile(Request $request)
    {
        if (!auth()->user()->permissions->aplication_client->configuration->update) {
            return GeneralHelper::checkPermissionReturn();
        }

        $company = Company::find(auth()->user()->company_id);
        $company->external_id = $request->external_id ?: null;
        $company->business_name = $request->business_name ?: null;
        $company->trading_name = $request->trading_name ?: null;
        $company->federal_tax_number = $request->federal_tax_number ?: null;
        $company->state_tax_number = $request->state_tax_number ?: null;
        $company->direction = $request->direction ?: null;
        $company->town = $request->town ?: null;
        $company->state = $request->state ?: null;
        $company->province = $request->province ?: null;
        $company->country = $request->country ?: null;
        $company->postcode = $request->postcode ?: null;
        $company->contact = $request->contact ?: null;
        $company->mobile = $request->mobile ?: null;
        $company->phone = $request->phone ?: null;
        $company->email = $request->email ?: null;
        $company->web = $request->web ?: null;
        $company->branch = $request->branch ? true : false;
        $company->retail = $request->retail ? true : false;
        $company->wholesale = $request->wholesale ? true : false;
        $company->external = $request->external ? true : false;

        if ($request->file) {
            GeneralHelper::setFileToCDN($request, $company, 'company_logo', 'logo', false, true);
        }
        if ($company && isset($company->id) && $company->save()) {
            return GeneralHelper::returnResponseSave()->saved;
        }

        return GeneralHelper::returnResponseSave()->not_saved;
    }


    /**
     * Edit a Company Profile
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function setCompanyBranch(Request $request)
    {
        $company = null;

        if ($request->company_id) {

            if (!auth()->user()->permissions->aplication_client->company->update) {
                return GeneralHelper::checkPermissionReturn();
            }

            $company = Company::find($request->company_id);

            if ($company) {
                $request->external_id ? $company->external_id = $request->external_id : null;
                $request->business_name ? $company->business_name = $request->business_name : null;
                $request->trading_name ? $company->trading_name = $request->trading_name : null;
                $request->federal_tax_number ? $company->federal_tax_number = $request->federal_tax_number : null;
                $request->state_tax_number ? $company->state_tax_number = $request->state_tax_number : null;
                $request->direction ? $company->direction = $request->direction : null;
                $request->town ? $company->town = $request->town : null;
                $request->state ? $company->state = $request->state : null;
                $request->province ? $company->province = $request->province : null;
                $request->country ? $company->country = $request->country : null;
                $request->postcode ? $company->postcode = $request->postcode : null;
                $request->contact ? $company->contact = $request->contact : null;
                $request->mobile ? $company->mobile = $request->mobile : null;
                $request->phone ? $company->phone = $request->phone : null;
                $request->email ? $company->email = $request->email : null;
                $request->web ? $company->web = $request->web : null;
                isset($request->branch) ? ($company->branch = $request->branch ? true : false) : null;
                isset($request->retail) ? ($company->retail = $request->retail ? true : false) : null;
                isset($request->wholesale) ? ($company->wholesale = $request->wholesale ? true : false) : null;
                isset($request->external) ? ($company->external = $request->external ? true : false) : null;
            }

        } else {

            if (!auth()->user()->permissions->aplication_client->company->create) {
                return GeneralHelper::checkPermissionReturn();
            }

            $company = Company::create([
                'external_id' => $request->external_id ?: null,
                'business_name' => $request->business_name ?: null,
                'trading_name' => $request->trading_name ?: null,
                'federal_tax_number' => $request->federal_tax_number ?: null,
                'state_tax_number' => $request->state_tax_number ?: null,
                'direction' => $request->direction ?: null,
                'town' => $request->town ?: null,
                'state' => $request->state ?: null,
                'province' => $request->province ?: null,
                'country' => $request->country ?: null,
                'postcode' => $request->postcode ?: null,
                'contact' => $request->contact ?: null,
                'mobile' => $request->mobile ?: null,
                'phone' => $request->phone ?: null,
                'email' => $request->email ?: null,
                'web' => $request->web ?: null,
                'branch' => $request->branch ? true : false,
                'retail' => $request->retail ? true : false,
                'wholesale' => $request->wholesale ? true : false,
                'external' => $request->external ? true : false
            ]);
        }

        if ($request->file) {
            GeneralHelper::setFileToCDN($request, $company, 'company_logo', 'logo', false, true);
        }
        if ($company && isset($company->id) && $company->save()) {
            return GeneralHelper::returnResponseSave()->saved;
        }

        return GeneralHelper::returnResponseSave()->not_saved;
    }


    /**
     * Enable/Disable a user by ID.
     *
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function toggleCompanyBranch(Request $request)
    {
        if (!auth()->user()->permissions->aplication_client->role->update) {
            return GeneralHelper::checkPermissionReturn();
        }
        if ($request->id) {
            $company = Company::withoutGlobalScope('CompanyScope')->find($request->id);
            if ($company) {
                $company->active = !$company->active;
                if ($company->save()) {
                    return GeneralHelper::returnResponseSave()->saved;
                }
            }
        }

        return GeneralHelper::returnResponseSave()->not_saved;
    }


    /**
     * Set Company logo.
     * @param Request $request , null
     * @return Response
     */
    public function setPicture(Request $request)
    {
        if (!auth()->user()->permissions->aplication_client->configuration->update) {
            return GeneralHelper::checkPermissionReturn();
        }
        if ($request->id) {
            $company = Company::find($request->id);
            $picture = GeneralHelper::setFileToCDN($request, $company, 'company_logo', 'logo', false, true);
            if ($picture) {
                return $picture;
            }
        }

        return response()->Json(['status' => 0, 'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'Não foi possível enviar corretamente!']]);
    }




    /****** DELETE **************************************************************************************************/


    /**
     * Delete a Company by ID. It's does by the mode sofdelete.
     * @param Request $request
     * @return bool|Response
     * @throws Exception
     */
    public function deleteCompanyBranch(Request $request)
    {
        if (!auth()->user()->permissions->aplication_client->company->delete) {
            return GeneralHelper::checkPermissionReturn();
        }
        if ($request->id) {
            return false;
        }

        return GeneralHelper::returnResponseDelete()->not_deleted;
    }



    /****** PARSES **************************************************************************************************/


    /**
     * Parse all query resulto do Json
     *
     * @param Company $consult
     * @param $request
     * @param $permissions
     * @return array
     */
    private function parseCompanys(Company $consult, $request, $permissions)
    {
        $aux = [
            'id' => $consult->id,
            'logo' => $consult->logo_urls ? '<img class="m-img-rounded kt-marginless" style="max-width: 150px; max-height: 50px;" alt="picture" src="' . $consult->logo_url . '" />' : '<span class="kt-badge kt-badge--dark kt-badge--xl kt-badge--rounded">' . substr($consult->full_name, 0, 1) . '</span>',
            'business_name' => $consult->business_name,
            'trading_name' => $consult->trading_name,
            'full_name' => $consult->full_name,
            'federal_tax_number' => $consult->federal_tax_number,
            'state_tax_number' => $consult->state_tax_number,
            'province' => $consult->province,
            'town' => $consult->town,
            'state' => $consult->state,
            'mobile_phone' => $consult->mobile,
            'email' => $consult->email,
            'users_count' => $consult->users()->count(),
            'branchs_count' => $consult->childs()->count(),
            'branch' => $consult->branch,
            'active_o' => $consult->active,
            'active' => PermissionsHelper::parseOptions($request, $consult, false, $permissions->update, !$consult->company_id ? $permissions->delete : false, 'role_options', true)->active,
            'actions' => PermissionsHelper::parseOptions($request, $consult, false, $permissions->update, $permissions->delete, 'role_options', true)->actions
        ];

        return $aux;
    }

}
