<?php

namespace App\Http\Controllers;

use View;

class WebController extends Controller
{
    /**
     * Show the welcome homepage.
     *
     * @return View
     */
    public function index()
    {

        return view('welcome');
    }


}
