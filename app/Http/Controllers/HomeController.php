<?php

namespace App\Http\Controllers;

use App\Company;
use Exception;
use Request;
use PermissionsHelper;
use Sentry;
use View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @param void
     */
    public function __construct()
    {
        //$this->middleware('auth, set.locale, auth.permissions');
    }


    /****** INDEX **************************************************************************************************/


    /**
     * Show the dashboard homepage.
     *
     * @return NotFoundHttpException|bool|View
     * @throws Exception
     */
    public function index()
    {
        try {
            PermissionsHelper::setPermissionsToUser(auth()->user()->id);
        } catch (Exception $e) {
            Sentry::captureException($e);
        }
        $companys = Company::all();
        $language = auth()->user()->language->name;

        return view('home', compact('companys', 'language'));
    }


    /**
     * Show the dashboard homepage.
     *
     * @return NotFoundHttpException|bool|View
     * @throws Exception
     */
    public function indexAdministration()
    {
        try {
            PermissionsHelper::setPermissionsToUser(auth()->user()->id);
        } catch (Exception $e) {
            Sentry::captureException($e);
        }
        $companys = Company::all();
        $language = auth()->user()->language->name;

        return view('administration.home', compact('companys', 'language'));
    }


    /**
     * Set company filter id on session
     *
     * @param Request $request
     * @return mixed
     */
    public function companyFilter(Request $request)
    {
        $request->session()->put('company_filter', $request->company_filter ?: '');

        return $request->session()->get('company_filter');
    }


    /**
     * Set sideBar true or false on session
     *
     * @param Request $request
     * @return mixed
     */
    public function sideBarSession(Request $request)
    {
        if ($request->session()->get('sideBar')) {
            $request->session()->put('sideBar', false);
        } else {
            $request->session()->put('sideBar', true);
        }

        return $request->session()->get('sideBar') ? 'Activo' : 'Inactivo';
    }
}
