<?php

namespace App\Http\Controllers;

use App\Company;
use App\Users;
use App\Roles;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\Eloquent\Builder;
use Request;
use Session;
use Hash;
use Exception;
use Response;
use Route;
use PermissionsHelper;
use GeneralHelper;
use View;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @param void
     */
    public function __construct()
    {
        //$this->middleware('auth, set.locale, auth.permissions');
    }


    /****** INDEX **************************************************************************************************/


    /**
     * Show the Users Configuration homepage.
     *
     * @return NotFoundHttpException|bool|View
     */
    public function index()
    {
        if (!auth()->user()->permissions->backoffice_client->employee->view) {
            return GeneralHelper::checkPermissionErrorReturn();
        }
        $roles = Roles::has('permission_without_full')->OrwhereDoesntHave('permission')->orderBy('name')->get();
        $language = auth()->user()->language->name;
        $companys = Company::all();
        $en = auth()->user()->language->check->en;
        $es = auth()->user()->language->check->es;
        $ptBR = auth()->user()->language->check->ptBR;

        return view('users.users', compact('companys', 'roles', 'en', 'es', 'ptBR', 'language'));
    }


    /**
     * Show the User Configuration homepage.
     *
     * @return NotFoundHttpException|bool|View
     */
    public function indexDetail()
    {
        if (!auth()->user()->permissions->backoffice_client->employee->view) {
            return GeneralHelper::checkPermissionErrorReturn();
        }
        $route = Route::current();
        $id = $route->parameter('id');
        $user = Users::find($id);

        if (isset($user->id)) {

            $language = auth()->user()->language->name;
            $en = auth()->user()->language->check->en;
            $es = auth()->user()->language->check->es;
            $ptBR = auth()->user()->language->check->ptBR;

            return view('users.user', compact('user', 'en', 'es', 'ptBR', 'language'));
        }

        return GeneralHelper::checkRegisterErrorReturn();
    }


    /**
     * Show the Users Configuration homepage.
     *
     * @return NotFoundHttpException|bool|View
     */
    public function indexSuper()
    {
        $roles = Roles::orderBy('name')->get();
        $language = auth()->user()->language->name;
        $companys = Company::all();
        $en = auth()->user()->language->check->en;
        $es = auth()->user()->language->check->es;
        $ptBR = auth()->user()->language->check->ptBR;

        return view('administration.users.users', compact('companys', 'roles', 'en', 'es', 'ptBR', 'language'));
    }


    /**
     * Show the User Configuration homepage.
     *
     * @return NotFoundHttpException|bool|View
     */
    public function indexSuperDetail()
    {
        $route = Route::current();
        $id = $route->parameter('id');
        $user = Users::find($id);

        if (isset($user->id)) {
            $language = auth()->user()->language->name;
            $en = auth()->user()->language->check->en;
            $es = auth()->user()->language->check->es;
            $ptBR = auth()->user()->language->check->ptBR;

            return view('administration.users.user', compact('user', 'en', 'es', 'ptBR', 'language'));
        }

        return GeneralHelper::checkRegisterErrorReturn();
    }


    /****** GET **************************************************************************************************/


    /**
     * Retrieve the users for Datatable
     *
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function getUsers(Request $request)
    {
        $aux = [];
        $users = Users::withTrashed()->where(function (Builder $q) use ($request) {
            if ($request->role_id) {
                $q->where('role_id', $request->role_id);
            }
            if ($request->state_filter == 1) {
                $q->where('active', true);
            }
            if ($request->state_filter == 2) {
                $q->where('active', false);
            }
            if ($request->recycle_filter === 'true') {
                $q->where('deleted_at', '!=', null);
            } else {
                $q->where('deleted_at', null);
            }
        })->with([
            'role' => function (Roles $qr) {
                $qr->whereNotIn('permission_id', [7, 24, 21]);
                $qr->with('permission');
            }
        ])->where('company_id', auth()->user()->company_id)->get();

        foreach ($users as $user) {
            $aux[] = [
                'id' => $user->id,
                'picture' => $user->picture ? '<img class="m-img-rounded kt-marginless" style="border-radius: 50%; max-width: 50; max-height: 50;" alt="picture" src="' . $user->picture_url . '" />' : '<div class="kt-user-card-v2 kt-user-card-v2__pic kt-badge kt-badge--xl kt-badge--brand">' . substr($user->name, 0, 1) . '</div>',
                'role_id' => $user->role_id,
                'role' => $user->role ? $user->role->name : '-',
                'full_name' => $user->name . ' ' . $user->last_name,
                'name' => $user->name,
                'last_name' => $user->last_name,
                'identification_number' => $user->identification_number,
                'social_number' => $user->social_number,
                'mobile_phone' => $user->mobile_phone,
                'email' => $user->email,
                //'centro_trabajo' => count($user->locales) > 0 ? implode(',', $user->locales->pluck('nombre')->toArray()) : '',
                //'ids_locales' => count($user->locales) > 0 ? $user->locales->pluck('id')->toArray() : [],
                'active_o' => $user->active,
                'active' => $this->getUserActions($request, $user, true, true, true, '', false)->active,
                'actions' => $this->getUserActions($request, $user, true, true, true, 'user_options', true)->actions
            ];
        }

        return response()->json(['data' => $aux]);
    }


    /**
     * Devuelve las acciones segun el usuario
     *
     * @param bool $request
     * @param bool $query
     * @param bool $see
     * @param bool $edit
     * @param bool $delete
     * @param bool $class
     * @param bool $unlock
     * @return object
     */
    private function getUserActions(
        $request = false,
        $query = false,
        $see = false,
        $edit = false,
        $delete = false,
        $class = false,
        $unlock = false
    ) {
        $actions = null;
        $active = null;
        //Check if has permissions and return HTML Option in to array
        if ((!$query->role->permission->full && $see && isset($request) && $request->recycle_filter === 'false') || ($query->role->permission->full && auth()->user()->role->permission->full && isset($request) && $request->recycle_filter === 'false')) {
            $actions .= '<div class="form-group row" style="margin-bottom: 0">';
            $actions .= '<a title="Ver" href="javascript:" class="btn btn-sm btn-clean btn-icon btn-icon-md ' . $class . '" data-action="see"><i class="la la-eye"></i></a>';
        } else {
            $actions .= '<div>';
        }
        $actions .= '<span class="dropdown">';
        $actions .= '<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="false">';
        $actions .= '<i class="la la-ellipsis-h"></i>';
        $actions .= '</a>';
        $actions .= '<div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="display: none; position: absolute; will-change: transform; top: 0; left: 0; transform: translate3d(1389px, 468px, 0);">';
        //Check if is Active or Inactive
        if ((isset($request) && $request->recycle_filter === 'true') || (!$query->role->permission->full && $edit) || ($query->role->permission->full && auth()->user()->role->permission->full)) {
            if ((isset($query->activePivot) && $query->activePivot) || (isset($query->active) && $query->active)) {
                $active .= '<span title="Ativo" class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Ativo</span>';
            } else {
                $active .= '<span title="Inativo" class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Inactivo</span>';
            }
        } else {
            if ((isset($query->activePivot) && $query->activePivot) || (isset($query->active) && $query->active)) {
                $active .= '<span title="Ativo" class="linkActive kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Ativo</span>';
            } else {
                $active .= '<span title="Inativo" class="linkActive kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Inactivo</span>';
            }
        }
        if ((!$query->role->permission->full && $edit) || ($query->role->permission->full && auth()->user()->role->permission->full)) {
            if (isset($request) && $request->recycle_filter === 'true') {
                if ($delete) {
                    $actions .= '<a title="Recuperar" href="javascript:" class="dropdown-item ' . $class . '" data-action="recycle"><i class="la la-recycle"></i> Recuperar </a>';
                }
            } else {
                if ($edit && $unlock) {
                    $actions .= '<a title="Ativar/Desativar" href="javascript:" class="dropdown-item ' . $class . '" data-action="lock"><i class="la la-unlock"></i>Ativar/Desativar</a>';
                }
                if ($edit) {
                    $actions .= '<a title="Redefinir senha" href="javascript:" class="dropdown-item ' . $class . '" data-action="password"><i class="la la-mail-forward"></i>Redefinir senha</a>';
                }
                if ($edit) {
                    $actions .= '<a title="Editar" href="javascript:" class="dropdown-item ' . $class . '" data-action="edit"><i class="la la-edit"></i>Editar</a>';
                }
                if ($delete) {
                    $actions .= '<a title="Eliminar" href="javascript:" class="dropdown-item ' . $class . '" data-action="softdelete"><i class="la la-trash-o"></i>Eliminar</a>';
                }
            }
        }
        $actions .= '<a class="dropdown-item" href="javascript:"> Cancelar </a>';
        $actions .= '</div>';
        $actions .= '</span>';
        $actions .= '</div>';

        return (object)compact('actions', 'active');
    }


    /****** SET **************************************************************************************************/


    /**
     * Save/edit a employee
     *
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function setUser(Request $request)
    {
        if ($request->role_employee && $request->name && $request->last_name && $request->mobile_phone && $request->social_number && $request->identification_number) {
            //If has a request ID, we edit the  user information's
            if (isset($request->id) && $request->id) {
                if (!auth()->user()->permissions->backoffice_client->employee->update) {
                    return GeneralHelper::checkPermissionReturn();
                }
                $user = Users::find($request->id);
                if (isset($user->id)) {
                    $user->role_id = $request->role_employee;
                    $user->name = $request->name;
                    $user->last_name = $request->last_name;
                    $user->fix_phone = preg_replace('/\D/', '', $request->fix_phone);
                    $user->mobile_phone = preg_replace('/\D/', '', $request->mobile_phone);
                    $user->identification_number = preg_replace('/\D/', '', $request->identification_number);
                    $user->social_number = preg_replace('/\D/', '', $request->social_number);
                    //$user->locales()->sync($request->locales);
                    $user->save();
                }
            } else {
                if (!$request->email) {
                    return response()->Json([
                        'status' => 0,
                        'not' => ['tipo' => 'warning', 'titulo' => '', 'mensaje' => 'O campo e-mail é obrigatórios!']
                    ]);
                }
                if (!auth()->user()->permissions->backoffice_client->employee->create) {
                    return GeneralHelper::checkPermissionReturn();
                }
                //If already exist the user, we recovery the email with those new information's
                $user = Users::withTrashed()->where('email', $request->email)->first();
                if ($user) {
                    $user->restore();
                    $user->role_id = $request->role_employee;
                    $user->name = $request->name;
                    $user->last_name = $request->last_name;
                    $user->fix_phone = preg_replace('/\D/', '', $request->fix_phone);
                    $user->mobile_phone = preg_replace('/\D/', '', $request->mobile_phone);
                    $user->identification_number = preg_replace('/\D/', '', $request->identification_number);
                    $user->social_number = preg_replace('/\D/', '', $request->social_number);
                    $user->password = Hash::make($request->pass);
                    $user->plafform_modules_permissions = null;
                    //$user->locales()->sync($request->locales);
                    $user->save();
                    $token = app('auth.password.broker')->createToken($user);
                    $user->sendNewUserNotification($user, $token, false);
                    PermissionsHelper::setPermissionsToUser($user->id);
                } else {
                    //Creating a new user
                    if (!auth()->user()->permissions->backoffice_client->employee->create) {
                        return GeneralHelper::checkPermissionReturn();
                    }
                    $user = Users::create([
                        'role_id' => $request->role_employee,
                        'name' => $request->name,
                        'last_name' => $request->last_name,
                        'email' => $request->email,
                        'fix_phone' => $request->fix_phone ? preg_replace('/\D/', '', $request->fix_phone) : null,
                        'mobile_phone' => preg_replace('/\D/', '', $request->mobile_phone),
                        'identification_number' => preg_replace('/\D/', '', $request->identification_number),
                        'social_number' => preg_replace('/\D/', '', $request->social_number),
                        'password' => Hash::make($request->name . $request->last_name),
                    ]);
                    //$user->locales()->sync($request->locales);
                    $token = app('auth.password.broker')->createToken($user);
                    $user->sendNewUserNotification($user, $token, false);
                    PermissionsHelper::setPermissionsToUser($user->id);
                }
            }

            if ($user && isset($user->id)) {
                return response()->Json([
                    'status' => 1,
                    'not' => ['tipo' => 'success', 'titulo' => '', 'mensaje' => 'Guardado corretamente']
                ]);
            }

            return response()->Json([
                'status' => 0,
                'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'Não foi possível guardar corretamente!']
            ]);
        }

        return response()->Json([
            'status' => 0,
            'not' => ['tipo' => 'warning', 'titulo' => '', 'mensaje' => 'Introduz todos os datos obrigatórios!']
        ]);
    }


    /**
     * Enable/Disable a user by ID.
     *
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function toggleActive(Request $request)
    {
        if (!auth()->user()->permissions->backoffice_client->employee->update) {
            return GeneralHelper::checkPermissionReturn();
        }
        if ($request->id) {
            $user = Users::find($request->id);

            if ($user) {
                $user->active = !$user->active;

                if ($user->save()) {
                    return response()->Json([
                        'status' => 1,
                        'not' => ['tipo' => 'success', 'titulo' => '', 'mensaje' => 'Guardado corretamente']
                    ]);
                }
            }
        }

        return response()->Json([
            'status' => 0,
            'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'Não foi possível guardar correctamente!']
        ]);
    }


    /**
     * Set a user picture.
     *
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function setPicture(Request $request)
    {
        if (!auth()->user()->permissions->backoffice_client->employee->update) {
            return GeneralHelper::checkPermissionReturn();
        }

        if ($request->id) {
            $user = Users::find($request->id);
            $picture = GeneralHelper::setFileToCDN($request, $user, 'user_picture', 'picture', false, true);

            if ($picture) {
                return $picture;
            }
        }

        return response()->Json([
            'status' => 0,
            'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'Não foi possível enviar corretamente!']
        ]);
    }


    /**
     * Send a email to user recovery you account.
     *
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function recoveryAccount(Request $request)
    {
        if (!auth()->user()->permissions->backoffice_client->employee->update) {
            return GeneralHelper::checkPermissionReturn();
        }
        if ($request->id) {
            $user = Users::find($request->id);

            if ($user) {
                $token = app('auth.password.broker')->createToken($user);
                $user->sendNewUserNotification($user, $token, true);

                return response()->Json([
                    'status' => 1,
                    'not' => ['tipo' => 'success', 'titulo' => '', 'mensaje' => 'Enviado corretamente']
                ]);
            }
        }

        return response()->Json([
            'status' => 0,
            'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'Não se pode enviar corretamente!']
        ]);
    }


    /****** DELETE **************************************************************************************************/


    /**
     * Drop a user by ID
     *
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function deleteUser(Request $request)
    {
        if (!auth()->user()->permissions->backoffice_client->employee->delete) {
            return GeneralHelper::checkPermissionReturn();
        }

        if (isset($request->id) && $request->id) {
            $user = Users::withTrashed()->find($request->id);

            if ($user->permission && $user->permission->full && !$user->role->permission->full) {
                if (auth()->user()->language->check->en) {
                    return response()->Json([
                        'status' => 0,
                        'not' => [
                            'tipo' => 'error',
                            'titulo' => '',
                            'mensaje' => 'You do not have permission to delete this user!'
                        ]
                    ]);
                }
                if (auth()->user()->language->check->es) {
                    return response()->Json([
                        'status' => 0,
                        'not' => [
                            'tipo' => 'error',
                            'titulo' => '',
                            'mensaje' => '¡No tienes permiso para eliminar a este usuario!'
                        ]
                    ]);
                }
                if (auth()->user()->language->check->ptBR) {
                    return response()->Json([
                        'status' => 0,
                        'not' => [
                            'tipo' => 'error',
                            'titulo' => '',
                            'mensaje' => 'Você não tem permissão para eliminar esse usúario!'
                        ]
                    ]);
                }
            }

            switch ($request->trashed) {

                case 'softdelete':

                    if ($user && $user->active) {
                        if (auth()->user()->language->check->en) {
                            return response()->Json([
                                'status' => 0,
                                'not' => [
                                    'tipo' => 'warning',
                                    'titulo' => '',
                                    'mensaje' => 'This user is active and cannot be deleted!'
                                ]
                            ]);
                        }
                        if (auth()->user()->language->check->es) {
                            return response()->Json([
                                'status' => 0,
                                'not' => [
                                    'tipo' => 'warning',
                                    'titulo' => '',
                                    'mensaje' => '¡Este usuario está activo y no se puede eliminar!'
                                ]
                            ]);
                        }
                        if (auth()->user()->language->check->ptBR) {
                            return response()->Json([
                                'status' => 0,
                                'not' => [
                                    'tipo' => 'warning',
                                    'titulo' => '',
                                    'mensaje' => 'Este usúario está ativo e não pode ser eliminado!'
                                ]
                            ]);
                        }
                    }

                    if ($user && $user->locked) {
                        if (auth()->user()->language->check->en) {
                            return response()->Json([
                                'status' => 0,
                                'not' => [
                                    'tipo' => 'warning',
                                    'titulo' => '',
                                    'mensaje' => 'This user is locked and cannot be deleted!'
                                ]
                            ]);
                        }
                        if (auth()->user()->language->check->es) {
                            return response()->Json([
                                'status' => 0,
                                'not' => [
                                    'tipo' => 'warning',
                                    'titulo' => '',
                                    'mensaje' => '¡Este usuario está bloqueado y no se puede eliminar!'
                                ]
                            ]);
                        }
                        if (auth()->user()->language->check->ptBR) {
                            return response()->Json([
                                'status' => 0,
                                'not' => [
                                    'tipo' => 'warning',
                                    'titulo' => '',
                                    'mensaje' => 'Este usúario está bloqueado e não pode ser eliminado!'
                                ]
                            ]);
                        }
                    }

                    if ($user->id === auth()->user()->id) {
                        if (auth()->user()->language->check->en) {
                            return response()->Json([
                                'status' => 0,
                                'not' => [
                                    'tipo' => 'warning',
                                    'titulo' => '',
                                    'mensaje' => 'You cannot delete your own user!'
                                ]
                            ]);
                        }
                        if (auth()->user()->language->check->es) {
                            return response()->Json([
                                'status' => 0,
                                'not' => [
                                    'tipo' => 'warning',
                                    'titulo' => '',
                                    'mensaje' => '¡Estás intentando eliminar tu propio usuario.!'
                                ]
                            ]);
                        }
                        if (auth()->user()->language->check->ptBR) {
                            return response()->Json([
                                'status' => 0,
                                'not' => [
                                    'tipo' => 'warning',
                                    'titulo' => '',
                                    'mensaje' => 'Esta tentando apagar seu próprio usúario!'
                                ]
                            ]);
                        }
                    }

                    if ($user->delete()) {
                        return GeneralHelper::returnResponseDelete()->deleted;
                    }

                    return GeneralHelper::returnResponseDelete()->not_deleted;

                case 'recycle':
                    if ($user && $user->restore()) {

                        if (auth()->user()->language->check->en) {
                            return response()->Json([
                                'status' => 1,
                                'not' => ['tipo' => 'success', 'titulo' => '', 'mensaje' => 'Recovered correctly!']
                            ]);
                        }
                        if (auth()->user()->language->check->es) {
                            return response()->Json([
                                'status' => 1,
                                'not' => [
                                    'tipo' => 'success',
                                    'titulo' => '',
                                    'mensaje' => '¡Recuperado correctamente!'
                                ]
                            ]);
                        }
                        if (auth()->user()->language->check->ptBR) {
                            return response()->Json([
                                'status' => 1,
                                'not' => ['tipo' => 'success', 'titulo' => '', 'mensaje' => 'Recuperado corretamente!']
                            ]);
                        }
                    }
                    return GeneralHelper::returnResponseDelete()->not_deleted;

                case 'delete':
                    if ($user && $user->forceDelete()) {

                        return GeneralHelper::returnResponseDelete()->deleted;
                    }
                    return GeneralHelper::returnResponseDelete()->not_deleted;

                default:
                    return GeneralHelper::returnResponseDelete()->not_deleted;

            }
        }

        return GeneralHelper::returnResponseDelete()->not_deleted;
    }


    /******** PERMISSIONS ************************************************************************************/


    /**
     * Retrieve User permissions
     *
     * @param Request $request
     * @return Response
     */
    public function getUserPermissions(Request $request)
    {
        $aux = [];

        if ($request->user_id) {
            $user = Users::find($request->user_id);
            $aux = PermissionsHelper::getUserPermissions($user, $user->plafform_modules_permissions ? false : true, true, 'client');
        }

        return response()->json(['data' => $aux]);
    }


    /**
     * Change User permission
     *
     * @param Request $request
     * @return Response
     */
    public function setUserPermission(Request $request)
    {
        if (!auth()->user()->permissions->backoffice_client->employee->update) {
            return GeneralHelper::checkPermissionReturn();
        }

        if ($request->user_id && $request->plataform && $request->module && $request->permission) {
            $user = Users::find($request->user_id);

            if ($user && $user->id) {
                $aux = PermissionsHelper::setUserPermission($request->user_id, $request->plataform, $request->module, $request->permission, $request->value);

                if ($aux) {
                    return GeneralHelper::returnResponseDelete()->saved;
                }
            }
        }

        return GeneralHelper::returnResponseDelete()->not_saved;
    }


    /**
     * Delete User permissions
     *
     * @param Request $request
     * @return Response
     */
    public function deleteUserPermissions(Request $request)
    {
        if (!auth()->user()->permissions->backoffice_client->employee->delete) {
            return GeneralHelper::checkPermissionReturn();
        }
        if ($request->user_id) {
            $aux = PermissionsHelper::deleteUserPermissions($request->user_id);

            if ($aux) {
                return GeneralHelper::returnResponseDelete()->deleted;
            }
        }

        return GeneralHelper::returnResponseDelete()->not_deleted;
    }


    /******** OTHERS ************************************************************************************/


    /**
     * Set sideBar true or false on session
     *
     * @param Request $request
     * @return bool|Session
     */
    public function sideBarSession(Request $request)
    {
        if ($request->session()->get('sideBar')) {
            $request->session()->put('sideBar', false);
        } else {
            $request->session()->put('sideBar', true);
        }

        return $request->session()->get('sideBar') ? 'Activo' : 'Inactivo';
    }
}
