<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Config;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * Show the application's login form.
     *
     * @return mixed
     */
    public function showLoginForm()
    {

        $language = Config::get('app.locale');

        return view('auth.login', compact('language'));
    }


    /**
     * Show the login form.
     *
     * @return mixed
     */
    public function showAdministrationLoginForm()
    {

        $language = Config::get('app.locale');

        return view('administration.auth.login', compact('language'));
    }


    /**
     * The user has logged out of the application.
     *
     * @param Request $request
     * @return mixed
     */
    public function logout(Request $request) {

        auth()->logout();

        if (request()->getHost() == config('app.admin').config('app.dashboard').config('app.domain')) {

            return redirect()->route('admin.login', app()->getLocale());
        }
        if (request()->getHost() == config('app.dashboard').config('app.domain') ) {

            return redirect()->route('login', app()->getLocale());
        }

        return false;
    }

}
