<?php

namespace App\Http\Controllers\Auth;

use App\Company;
use App\Languages;
use App\Http\Controllers\Controller;
use App\Users;
use Hash;
use Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use GeneralHelper;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
     * Show the application registration form.
     *
     * @return mixed
     */
    public function showRegistrationForm()
    {

        $language = config()->get('app.locale');

        return view('auth.register', compact('language'));
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|max:8|confirmed',
            'company_register' => [
                'required',
                'string',
                static function ($attribute, $value, $fail) {

                    if (config()->get('app.locale') == 'en') {

                        if (strlen($value) < 9) {
                            $fail('The Tax Number must be at least 9 digits.');
                        }
                        if (strlen($value) > 9) {
                            $fail('The Tax Number are nine digits.');
                        }
                    }

                    if (config()->get('app.locale') == 'es') {

                        $check = GeneralHelper::checkCompanyNumber($value, 'es');

                        if ($check == 2) {
                            $fail('El CIF ya está en uso.');
                        }
                        if ($check == 3) {
                            $fail('El CIF no es válido.');
                        }
                    }

                    if (config()->get('app.locale') == 'pt-BR') {

                        $check = GeneralHelper::checkCompanyNumber($value, 'pt-BR');

                        if ($check == 2) {
                            $fail('Este CNPJ está em uso.');
                        }
                        if ($check == 3) {
                            $fail('Este CNJP não é valido.');
                        }
                    }
                },
            ],
            'company_name' => 'required|string|max:255',
            'phone' => 'required|string|min:9',
            'town' => 'required|string|max:255',
            'privacy' => 'required',
            'terms' => 'required',
        ], [
            'name.required' => app()->isLocale('en') ? 'We need to know your name!' : (app()->isLocale('es') ? 'Necesitamos saber tu nombre!' : 'Precisamos saber seu nome!'),
            'last_name.required' => app()->isLocale('en') ? 'We need to know last name address!' : (app()->isLocale('es') ? 'Necesitamos saber tu apellido!' : 'Precisamos saber seu sobrenome!'),
            'email.required' => app()->isLocale('en') ? 'We need to know your e-mail address!' : (app()->isLocale('es') ? 'Necesitamos saber tu correo electrónico!' : 'Precisamos saber seu endereço de e-mail!'),
            'password.required' => app()->isLocale('en') ? 'The password is required.' : (app()->isLocale('es') ? 'La contraseña es obligatorio.' : 'A senha é obrigatório!'),
            'company_register.required' => app()->isLocale('en') ? 'The company tax number is required.' : (app()->isLocale('es') ? 'El CIF es obligatorio.' : 'O CNPJ é obrigatório!'),
            'company_name.required' => app()->isLocale('en') ? 'We need to know your company name!' : (app()->isLocale('es') ? 'Necesitamos sabere el nombre de la empresa!' : 'Precisamos saber o nome de sua empresa!'),
            'phone.required' => app()->isLocale('en') ? 'The company phone is required.' : (app()->isLocale('es') ? 'El telefono da empresa es obligatorio.' : 'O telefone da empresa é obrigatório!'),
            'town.required' => app()->isLocale('en') ? 'The company city is required.' : (app()->isLocale('es') ? 'EL municipio da empresa es obligatorio.' : 'A cidade da empresa é obrigatório!'),
            'privacy.required' => app()->isLocale('en') ? 'We need to know if you agree!' : (app()->isLocale('es') ? '¡Necesitamos saber si estás de acuerdo!' : 'Precisamos saber se você concorda!'),
            'terms.required' => app()->isLocale('en') ? 'We need to know if you agree!' : (app()->isLocale('es') ? '¡Necesitamos saber si estás de acuerdo!' : 'Precisamos saber se você concorda!')
        ]);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Users
     */
    protected function create(array $data)
    {
        return Users::create([
            'name' => $data['name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }


    /**
     * The user has been registered.
     *
     * @param $request
     * @param mixed $user
     * @return mixed
     */
    protected function registered($request, $user)
    {

        $company = Company::create([
            'federal_tax_number' => $request['company_register'],
            'business_name' => $request['company_name'],
            'phone' => $request['phone'],
            'town' => $request['town'],
            'form_register' => 1,
            'max_company' => 1,
            'max_users' => 2,
            'terms' => 1,
        ]);

        $language = Languages::where('name', app()->getLocale())->first();
        $role = $language->id == 1 ? 5 : ($language->id == 2 ? 12 : 19);

        $user->lang_id = $language->id;
        $user->role_id = $user->role_employee ?: $role;
        $user->company_id = $company->id;
        $user->privacy = 1;
        $user->terms = 1;
        $user->save();


        //Permissions::setFreemiunUserPermission($user);
        //Cookie::queue('videoFreemiun_'.$user->id, 0, 3*1440);


        return redirect()->route('app.company_profile', app()->getLocale());
    }
}
