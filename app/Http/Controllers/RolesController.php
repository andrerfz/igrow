<?php

namespace App\Http\Controllers;

use App\Permissions;
use App\Roles;
use App\Company;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Response;
use Exception;
use GeneralHelper;
use View;
use Route;
use PermissionsHelper;

class RolesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @param void
     */
    public function __construct()
    {
        //$this->middleware('auth, set.locale, auth.permissions');
    }


    /****** INDEX **************************************************************************************************/


    /**
     * Show the Users Configuration homepage.
     *
     * @return NotFoundHttpException|bool|View
     */
    public function index()
    {

        if (!auth()->user()->permissions->backoffice_client->role->view) {

            return GeneralHelper::checkPermissionErrorReturn();
        }

        $permissions = Permissions::where(['full' => false, 'active' => true])->orderBy('name')->get();
        $language = auth()->user()->language->name;
        $companys = Company::all();
        $en = auth()->user()->language->check->en;
        $es = auth()->user()->language->check->es;
        $ptBR = auth()->user()->language->check->ptBR;

        return view('users.roles', compact('permissions', 'companys', 'en', 'es', 'ptBR', 'language'));
    }


    /**
     * Show the Role detail configuration homepage.
     *
     * @return NotFoundHttpException|bool|View
     */
    public function indexDetail()
    {

        if (!auth()->user()->permissions->backoffice_client->role->view) {

            return GeneralHelper::checkPermissionErrorReturn();
        }

        $route = Route::current();
        $id = $route->parameter('id');
        $role = Roles::find($id);

        if ($id) {

            $permissions = Permissions::where(['full' => false, 'active' => true])->orderBy('name')->get();
            $language = auth()->user()->language->name;
            $en = auth()->user()->language->check->en;
            $es = auth()->user()->language->check->es;
            $ptBR = auth()->user()->language->check->ptBR;

            return view('users.role', compact('role', 'permissions', 'en', 'es', 'ptBR', 'language'));
        }

        return GeneralHelper::checkRegisterErrorReturn();
    }


    /**
     * Show the Users Configuration homepage.
     *
     * @return NotFoundHttpException|bool|View
     */
    public function indexSuper()
    {
        $roles = Roles::orderBy('name')->get();
        $permissions = Permissions::orderBy('name')->get();
        $language = auth()->user()->language->name;
        $companys = Company::all();
        $en = auth()->user()->language->check->en;
        $es = auth()->user()->language->check->es;
        $ptBR = auth()->user()->language->check->ptBR;

        return view('administration.users.roles', compact('companys', 'roles', 'permissions', 'en', 'es', 'ptBR', 'language'));
    }


    /**
     * Show the User Configuration homepage.
     *
     * @return NotFoundHttpException|bool|View
     */
    public function indexSuperDetail()
    {
        $route = Route::current();
        $id = $route->parameter('id');
        $role = Roles::find($id);

        if ($id) {

            $permissions = Permissions::orderBy('name')->get();
            $language = auth()->user()->language->name;
            $en = auth()->user()->language->check->en;
            $es = auth()->user()->language->check->es;
            $ptBR = auth()->user()->language->check->ptBR;

            return view('administration.users.role', compact('role', 'permissions', 'en', 'es', 'ptBR', 'language'));
        }

        return GeneralHelper::checkRegisterErrorReturn();
    }


    /****** GET **************************************************************************************************/


    /**
     * Retrieve roles for DataTables
     *
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function getRoles(Request $request)
    {
        $aux = [];

        $roles = Roles::withTrashed()->where(function (Builder $q) use ($request) {
            $q->has('permission_without_full');
            if ($request->permission_filter) {
                $q->where('permission_id', $request->permission_filter);
            }
            if ($request->state_filter == 1) {
                $q->where('active', true);
            }
            if ($request->state_filter == 2) {
                $q->where('active', false);
            }
            if ($request->recycle_filter === 'true') {
                $q->where('deleted_at', '!=', NULL);
            } else {
                $q->where('deleted_at', NULL);
            }
        })->orwhere(function (Builder $q) use ($request) {
            $q->whereDoesntHave('permission');
            if ($request->permission_filter) {
                $q->where('permission_id', $request->permission_filter);
            }
            if ($request->state_filter == 1) {
                $q->where('active', true);
            }
            if ($request->state_filter == 2) {
                $q->where('active', false);
            }
            if ($request->recycle_filter === 'true') {
                $q->where('deleted_at', '!=', NULL);
            } else {
                $q->where('deleted_at', NULL);
            }
        })->with(['users', 'permission'])->get();

        $permissions = auth()->user()->permissions->backoffice_client->role;

        foreach ($roles as $role) {

            $aux[] = [
                'id' => $role->id,
                'url' => '/admin/users/roles/' . $role->id,
                'name' => $role->name,
                'users_count' => $role->users()->count(),
                'permission_name' => $role->permission->name,
                'permission_view' => $role->permission->view ? '<i class="fa fa-check-circle fa-lg kt-font-success"></i>' : '<i class="fa fa-exclamation-circle fa-lg kt-font-warning"></i>',
                'permission_create' => $role->permission->create ? '<i class="fa fa-check-circle fa-lg kt-font-success"></i>' : '<i class="fa fa-exclamation-circle fa-lg kt-font-warning"></i>',
                'permission_update' => $role->permission->update ? '<i class="fa fa-check-circle fa-lg kt-font-success"></i>' : '<i class="fa fa-exclamation-circle fa-lg kt-font-warning"></i>',
                'permission_delete' => $role->permission->delete ? '<i class="fa fa-check-circle fa-lg kt-font-success"></i>' : '<i class="fa fa-exclamation-circle fa-lg kt-font-warning"></i>',
                'permission_id' => $role->permission_id,
                'active_o' => $role->active,
                'active' => $role->company_id == auth()->user()->company_id ? PermissionsHelper::parseOptions($request, $role, true, true, true, 'role_options', false)->active : '--',
                'actions' => $role->company_id == auth()->user()->company_id ? PermissionsHelper::parseOptions($request, $role, false, $permissions->update, $permissions->delete, 'role_options', true)->actions : '--'
            ];
        }

        return response()->json(['data' => $aux]);
    }


    /**
     * Retrieve roles for DataTables
     *
     * @param Request $request
     * @return Response
     */
    public function getRolesAdministration(Request $request)
    {
        $aux = [];
        $roles = Roles::with(['users'])->get();

        foreach ($roles as $role) {

            $aux[] = [
                'id' => $role->id,
                'url' => '/administration/users/roles/' . $role->id,
                'name' => $role->name,
                'users_count' => $role->users()->count(),
                'permission_view' => $role->permission->view ? '<i class="fa fa-check-circle kt-font-success"></i>' : '<i class="fa fa-exclamation-circle kt-font-warning"></i>',
                'permission_create' => $role->permission->create ? '<i class="fa fa-check-circle kt-font-success"></i>' : '<i class="fa fa-exclamation-circle kt-font-warning"></i>',
                'permission_update' => $role->permission->update ? '<i class="fa fa-check-circle kt-font-success"></i>' : '<i class="fa fa-exclamation-circle kt-font-warning"></i>',
                'permission_delete' => $role->permission->delete ? '<i class="fa fa-check-circle kt-font-success"></i>' : '<i class="fa fa-exclamation-circle kt-font-warning"></i>',
                'permission_id' => $role->permission_id,
                'active_o' => $role->active,
                'active' => PermissionsHelper::parseOptions($request, $role, true, true, true, 'role_options', false)->active,
                'actions' => PermissionsHelper::parseOptions($request, $role, true, true, true, 'role_options', true)->actions
            ];
        }

        return response()->json(['data' => $aux]);
    }


    /****** SET **************************************************************************************************/


    /**
     * Creat/edit a Role
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function setRole(Request $request)
    {
        if ($request->name) {
            $role = null;

            if (isset($request->id) && $request->id) {

                if (!auth()->user()->permissions->backoffice_client->role->update) {
                    return GeneralHelper::checkPermissionReturn();
                }
                $role = Roles::find($request->id);

                if ($role && !$role->locked && $role->company_id) {
                    $role->name = $request->name;
                    $role->permission_id = $request->permission;
                    $role->save();
                }

            } else {

                if (!auth()->user()->permissions->backoffice_client->role->create) {
                    return GeneralHelper::checkPermissionReturn();
                }

                $role = Roles::create([
                    'name' => $request->name,
                    'permission_id' => $request->permission ?: null,
                ]);

                //Permissions::setPermissionsToRoles();
            }

            if ($role && isset($role->id)) {
                return response()->Json(['status' => 1, 'not' => ['tipo' => 'success', 'titulo' => '', 'mensaje' => 'Guardado corretamente']]);
            }

            return response()->Json(['status' => 0, 'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'Não foi possível guardar corretamente!']]);
        }

        return response()->Json(['status' => 0, 'not' => ['tipo' => 'warning', 'titulo' => '', 'mensaje' => 'Introduz todos os datos obrigatórios!']]);
    }


    /**
     * Enable/Disable a user by ID.
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function toggleActive(Request $request)
    {
        if (!auth()->user()->permissions->backoffice_client->role->update) {
            return GeneralHelper::checkPermissionReturn();
        }

        if ($request->id) {
            $roles = Roles::find($request->id);

            if ($roles) {
                $roles->active = !$roles->active;

                if ($roles->save()) {
                    return response()->Json(['status' => 1, 'not' => ['tipo' => 'success', 'titulo' => '', 'mensaje' => 'Guardado corretamente']]);
                }
            }
        }

        return response()->Json(['status' => 0, 'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'Não foi possível guardar correctamente!']]);
    }


    /****** DELETE **************************************************************************************************/


    /**
     * Delete a Role by ID. It's does by the mode sofdelete.
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function deleteRole(Request $request)
    {
        if (!auth()->user()->permissions->backoffice_client->role->delete) {
            return GeneralHelper::checkPermissionReturn();
        }

        if (isset($request->id) && $request->id) {
            $role = Roles::withTrashed()->doesnthave('users')->find($request->id);

            if (!$role && !$role->id) {
                return response()->Json(['status' => 0, 'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'Este cargo está associado a algum usúario!']]);
            }

            $eliminado = response()->Json(['status' => 1, 'not' => ['tipo' => 'success', 'titulo' => '', 'mensaje' => 'Eliminado corretamente!']]);
            $no_eliminado = response()->Json(['status' => 0, 'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'No se ha podido eliminar!!']]);

            switch ($request->trashed) {

                case 'softdelete':
                    if (count($role->users) > 0) {
                        return response()->Json(['status' => 0, 'not' => ['tipo' => 'warning', 'titulo' => '', 'mensaje' => 'O usúario ' . $role->users[0]->full_name . ' pertenece a este cargo!']]);
                    }
                    if ($role && $role->active) {
                        return response()->Json(['status' => 0, 'not' => ['tipo' => 'warning', 'titulo' => '', 'mensaje' => 'Este cargo está ativo e não pode ser eliminado!']]);
                    }
                    if ($role && $role->locked) {
                        return response()->Json(['status' => 0, 'not' => ['tipo' => 'warning', 'titulo' => '', 'mensaje' => 'Este cargo está bloqueado e não pode ser eliminado!']]);
                    }
                    if ($role->company_id && $role->delete()) {
                        return $eliminado;
                    }

                    return $no_eliminado;

                case 'recycle':
                    if ($role && $role->restore()) {

                        return response()->Json(['status' => 1, 'not' => ['tipo' => 'success', 'titulo' => '', 'mensaje' => 'Recuperado corretamente!']]);
                    }
                    return $no_eliminado;

                case 'delete':
                    if ($role && $role->forceDelete()) {

                        return $eliminado;
                    }
                    return $no_eliminado;

                default:
                    return $no_eliminado;

            }

        }

        return response()->Json(['status' => 0, 'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'Não foi possível eliminar!']]);
    }

}
