<?php

namespace App\Http\Middleware;


use Closure;
use Illuminate\Http\Request;


class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (auth()->guard($guard)->check()) {

            if (request()->getHost() == config('app.admin').config('app.dashboard').config('app.domain') ) {

                return redirect(app()->getLocale() . '/administration');
            }

            if (request()->getHost() == config('app.dashboard').config('app.domain') ) {

                return redirect(app()->getLocale() . '/admin');
            }

        }

        return $next($request);
    }
}
