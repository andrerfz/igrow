<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use App;
use Illuminate\Http\Request;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param Request $request
     * @return mixed
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {

            if (request()->getHost() == config('app.admin') . config('app.dashboard') . config('app.domain')) {

                return route('admin.login', app()->getLocale());
            }

            if (request()->getHost() == config('app.dashboard') . config('app.domain')) {


                return route('login', app()->getLocale());
            }
        }
    }
}
