<?php

namespace App\Http\Middleware;


use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Closure;
use App;
use PermissionsHelper;


class RedirectIfNotAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws Exception
     */
    public function handle($request, Closure $next)
    {

        if (!$request->user()->plafform_modules_permissions) {

            $user = PermissionsHelper::setPermissionsToUser(auth()->user()->id);

        } else {

            $user = $request->user();
        }

        $errors = new MessageBag(['permiso' => ['No tienes permiso!']]);

        if ($user && auth()->check()) {

            if (request()->getHost() == config('app.admin').config('app.dashboard').config('app.domain') && !$user->role->permission->full) {

                auth()->logout();

                return redirect()->route('admin.login', app()->getLocale())->withErrors($errors)->withInput();
            }

            if (request()->getHost() == config('app.dashboard').config('app.domain') && !$user->permissions->aplication_client->global->view) {

                auth()->logout();

                return redirect()->route('login', app()->getLocale())->withErrors($errors)->withInput();
            }
        }

        return $next($request);
    }
}
