<?php

namespace App\Helpers;


use App\Platform;
use App\Permissions;
use App\User;
use App\Users;
use DB;
use Exception;
use stdClass;


class UsersPermissionsClass
{


    /**
     * Retrieve a array with all permission given from the user role or a new set value passing before.
     * If any value is different from the user role permission, we call that User Extension
     *
     *
     * @param User $user - User object
     * @param boolean $update_modules - Update those modules from each platform, or remove if doesn't exist any more.
     * @param boolean $array - Retrieve a array for DataTables with all information's about User Permissions or a Object JSON
     * @param boolean $platform_name - Passing the name from each platform that want to retrieve
     *
     * @return array|stdClass
     *
     * @throws Exception
     *
     */
    public function getUserPermissions(User $user, $update_modules = false, $array = false, $platform_name = null)
    {

        //Update those modules from each platform, or remove if doesn't exist any more.
        if ($update_modules) {
            $user = $this->setPermissionsToUser($user->id);
        }

        $permissios_array = [];

        $user_role_permission = Permissions::find($user->role->permission->id);

        $platforms = Platform::with(
            ['platform_language', 'modules' => static function ($q) {
                $q->with('module_language');
            }]
        )->get();

        $module_permission = new stdClass();

        $user_permissions = json_decode($user->plafform_modules_permissions);

        foreach ($platforms as $platform) {

            $module_permission->{$platform->name} = new stdClass();

            foreach ($platform->modules as $module) {

                $module_permission->{$platform->name}->{$module->name} = new stdClass();

                if (isset($user_permissions->{$platform->name}, $user_permissions->{$platform->name}->{$module->name}) && strpos($platform, $platform_name)) {

                    $view = isset($user_permissions->{$platform->name}->{$module->name}->{'view'}) ? $user_permissions->{$platform->name}->{$module->name}->{'view'} : $user_role_permission->view;
                    $create = isset($user_permissions->{$platform->name}->{$module->name}->{'create'}) ? $user_permissions->{$platform->name}->{$module->name}->{'create'} : $user_role_permission->create;
                    $update = isset($user_permissions->{$platform->name}->{$module->name}->{'update'}) ? $user_permissions->{$platform->name}->{$module->name}->{'update'} : $user_role_permission->update;
                    $delete = isset($user_permissions->{$platform->name}->{$module->name}->{'delete'}) ? $user_permissions->{$platform->name}->{$module->name}->{'delete'} : $user_role_permission->delete;

                    $permissios_array[] = [

                        'platform_' => $platform->name,
                        'module_name_' => $module->name,

                        'view' => $this->printLabelPermiso($platform->name, $module->name, 'view', $view, $user_role_permission->view != $view ?: false),
                        'create' => $this->printLabelPermiso($platform->name, $module->name, 'create',$create, $user_role_permission->create != $create ?: false),
                        'update' => $this->printLabelPermiso($platform->name, $module->name, 'update', $update,$user_role_permission->update != $update ?: false),
                        'delete' => $this->printLabelPermiso($platform->name, $module->name, 'delete', $delete,$user_role_permission->delete != $delete ?: false),

                        'platform' => ucfirst(str_replace('_', ' ', $platform->platform_language->name)),
                        'module_name' => ucfirst(str_replace('_', ' ', $module->module_language->name)),
                    ];

                    $module_permission->{$platform->name}->{$module->name}->{'view'} = $view;
                    $module_permission->{$platform->name}->{$module->name}->{'create'} = $create;
                    $module_permission->{$platform->name}->{$module->name}->{'update'} = $update;
                    $module_permission->{$platform->name}->{$module->name}->{'delete'} = $delete;
                }
            }
        }

        if ($array) {

            return $permissios_array;
        }

        return $module_permission;
    }


    /**
     * Retrieve a HTML with permissions
     *
     * @param $platform
     * @param $module
     * @param $permission
     * @param $value
     * @param bool $extendido
     * @return string|int|null
     */
    private function printLabelPermiso($platform, $module, $permission, $value, $extendido = false)
    {
        $aux = null;
        $aux .= $extendido ? '<div class="btn btn-outline-brand btn-square">' : '<div>';
        $aux .= '<a style="padding-right: 0; cursor: pointer;" class="link_permission fa '.($value ? 'fa-check-circle fa-lg kt-font-success' : 'fa-exclamation-circle fa-lg kt-font-warning').' "  data-platform="' . $platform . '" data-module="' . $module . '" data-permission="' . $permission . '" data-value="' . ($value ? 1 : 0) . '" ></a>';
        $aux .= $extendido ? '</div>' : '';

        return $aux;
    }


    /**
     * Create/Update Users permissions given the roles permissions associated to user
     *
     * @param $user_id
     * @return Users|int
     *
     * @throws Exception
     */
    public function setPermissionsToUser($user_id)
    {

        try {

            DB::beginTransaction();

            $user = Users::with(['role' => static function ($q) {
                $q->with('permission');
            }])->find($user_id);

            $platforms = Platform::with(
                ['modules' => static function ($q) {
                    $q->orderBy('name');
                }]
            )->get();

            $module_permission = new stdClass();
            $user_permissions = json_decode($user->plafform_modules_permissions ?: '{}');

            foreach ($platforms as $platform) {

                //Update's user's platform
                if (!isset($user_permissions->{$platform->name})) {

                    $user_permissions->{$platform->name} = new stdClass();
                }

                if (!isset($module_permission->{$platform->name})) {

                    $module_permission->{$platform->name} = new stdClass();
                }

                foreach ($platform->modules as $module) {

                    if (isset($user_permissions->{$platform->name}) && !isset($user_permissions->{$platform->name}->{$module->name})) {

                        //Update's user's permissions
                        $user_permissions->{$platform->name}->{$module->name} = new stdClass();
                    }

                    if (isset($module_permission->{$platform->name}) && !isset($module_permission->{$platform->name}->{$module->name})) {

                        $module_permission->{$platform->name}->{$module->name} = new stdClass();
                    }
                }
            }

            if ($module_permission != NULL) {

                //Remove any module that doesn't exist more from User Permission.
                foreach ($user_permissions as $module_key => $values) {
                    foreach ($user_permissions->{$module_key} as $key => $value) {

                        if (!isset($module_permission->{$module_key}->{$key})) {

                            unset($user_permissions->{$module_key}->{$key});
                        }
                    }
                }

                $user->plafform_modules_permissions = json_encode($user_permissions);
            } else {

                $user->plafform_modules_permissions = json_encode($module_permission);
            }

            $user->save();

            DB::commit();

            return $user;

        } catch (Exception $e) {

            DB::rollback();
            throw $e;
        }
    }


    /**
     * Edit User permission
     *
     * @param integer $user_id - User Id
     * @param string $platform - Platform name
     * @param string $module - Module name
     * @param string $permission - Permission Action name
     * @param boolean $value - Permission Action Value
     *
     *
     * @return boolean|int
     *
     */
    public function setUserPermission($user_id, $platform, $module, $permission, $value)
    {

        if ($user_id && $platform && $module && $permission) {

            $user = Users::find($user_id);

            if ($user) {

                $user_permissions = json_decode(($user->plafform_modules_permissions ?: '{}'));

                $user_permissions->{$platform}->{$module}->{$permission} = $value ? false : true;

                $user->plafform_modules_permissions = json_encode($user_permissions);
                $user->save();

                return true;
            }
        }

        return false;
    }


    /**
     * Delete User permission
     *
     * @param integer $user_id - User Id
     *
     * @return bool|int
     * @throws Exception
     *
     */
    public function deleteUserPermissions($user_id)
    {

        if ($user_id) {

            $user = Users::find($user_id);

            if ($user) {

                $user->plafform_modules_permissions = '{}';
                $user->save();

                $this->setPermissionsToUser($user_id);

                return true;
            }
        }

        return false;
    }


    /**
     * Devuelve las opciones formateados para la vista
     *
     * @param bool $request
     * @param bool $query
     * @param bool $see
     * @param bool $edit
     * @param bool $delete
     * @param bool $class
     * @param bool $unlock
     *
     * @return string|int|null
     */
    public function parseOptions($request = false, $query = false, $see = false, $edit = false, $delete = false, $class = false, $unlock = false)
    {

        $actions = null;
        $active = null;

        //Check if has permissions and return HTML Option in to array

        if ($see && isset($request) && $request->recycle_filter === 'false') {

            $actions .= '<div class="form-group row" style="margin-bottom: 0">';
            $actions .= '<a title="Ver" href="javascript:" class="btn btn-sm btn-clean btn-icon btn-icon-md '. $class .'" data-action="see"><i class="la la-eye"></i></a>';
        } else {

            $actions .= '<div>';
        }

        $actions .= '<span class="dropdown">';
        $actions .= '<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="false">';
        $actions .= '<i class="la la-ellipsis-h"></i>';
        $actions .= '</a>';
        $actions .= '<div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="display: none; position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(1389px, 468px, 0px);">';

        //Check if is Active or Inactive
        if (isset($request) && $request->recycle_filter === 'true') {

            if ((isset($query->activePivot) && $query->activePivot) || (isset($query->active) && $query->active)) {

                $active .= '<span title="Ativo" class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Ativo</span>';
            } else {

                $active .= '<span title="Inativo" class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Inactivo</span>';
            }

        } else {

            if ((isset($query->activePivot) && $query->activePivot) || (isset($query->active) && $query->active)) {

                $active .= '<span title="Ativo" class="linkActive kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Ativo</span>';
            } else {

                $active .= '<span title="Inativo" class="linkActive kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Inactivo</span>';
            }
        }


        if (isset($request) && $request->recycle_filter === 'true') {

            if ($delete) {
                $actions .= '<a title="Recuperar" href="javascript:" class="dropdown-item ' . $class . '" data-action="recycle"><i class="la la-recycle"></i> Recuperar </a>';
            }

        } else {

            if ($edit && $unlock) {
                $actions .= '<a title="Ativar/Desativar" href="javascript:" class="dropdown-item ' . $class . '" data-action="lock"><i class="la la-unlock"></i>Ativar/Desativar</a>';
            }
            if ($see || ($edit && $unlock)) {
                $actions .= '<a class="divider"></a>';
            }
            if ($edit) {
                $actions .= '<a title="Editar" href="javascript:" class="dropdown-item ' . $class . '" data-action="edit"><i class="la la-edit"></i>Editar</a>';
            }
            if ($delete) {
                $actions .= '<a title="Eliminar" href="javascript:" class="dropdown-item ' . $class . '" data-action="softdelete"><i class="la la-trash-o"></i>Eliminar</a>';
            }

        }


        $actions .= '<a title="Cancelar" class="dropdown-item" href="javascript:">Cancelar</a>';
        $actions .= '</div>';
        $actions .= '</span>';

        $actions .= '</div>';

        return (object)compact('actions', 'active');
    }

}