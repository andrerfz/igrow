<?php

namespace App\Helpers;


use App\TpvVentas;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use Config;
use Sentry;


class DatabaseIntegracionesClass
{

    /**
     * Realiza la conexion con la base de datos Dinámicamente
     *
     * Edita la configuracion
     *
     * @param $drive
     * @param $host
     * @param $port
     * @param $database
     * @param $username
     * @param $password
     *
     */
    public function setConnection($drive, $host, $port, $database, $username, $password)
    {

        switch ($drive) {

            case ($drive === 'sqlite') :

                Config::set("database.connections.sqlite_dynamically", [
                    'driver' => 'sqlite',
                    'database' => $database,
                    'prefix' => '',
                ]);
                break;
            case ($drive === 'mysql') :

                Config::set("database.connections.mysql_dynamically", [
                    'driver' => 'mysql',
                    'host' => $host,
                    'port' => $port,
                    'database' => $database,
                    'username' => $username,
                    'password' => $password,
                    'unix_socket' => '',
                    'charset' => 'utf8mb4',
                    'collation' => 'utf8mb4_unicode_ci',
                    'prefix' => '',
                    'strict' => true,
                    'engine' => null,
                ]);
                break;
            case ($drive === 'pgsql') :

                Config::set("database.connections.pgsql_dynamically", [
                    'driver' => 'pgsql',
                    'host' => $host,
                    'port' => $port,
                    'database' => $database,
                    'username' => $username,
                    'password' => $password,
                    'charset' => 'utf8',
                    'prefix' => '',
                    'schema' => 'public',
                    'sslmode' => 'prefer',
                ]);
                break;
            case ($drive === 'sqlsrv') :

                Config::set("database.connections.sqlsrv_dynamically", [
                    'driver' => 'sqlsrv',
                    'host' => $host,
                    'port' => $port,
                    'database' => $database,
                    'username' => $username,
                    'password' => $password,
                    'charset' => 'utf8',
                    'prefix' => '',
                ]);
                break;
        }

    }


    /**
     * Recupera los datos de las integraciones en base a configuracion de conexion del cliente
     *
     * @param $last_line
     * @param $database
     * @param $diccionario
     *
     * @return array
     */
    public function getDatos($last_line, $database, $diccionario)
    {

        $select = $diccionario->select_expression;
        $tabla = $diccionario->table_expression;
        $joins = $diccionario->join_expression;
        $where = $diccionario->where_expression;

        $query = 'SELECT ' . $select . ' FROM ' . $tabla . ' ' . $joins . ' ' . $where . ' ' . ($last_line ?: 0);

        $consult = DB::connection($database)->select($query);

        DB::disconnect($database);

        return $consult;
    }


    /**
     * Guarda los datos en la tabla del modelo pasado como parametro en base a una consulta anterior
     *
     * @param $data
     * @param  $model
     *
     * @return void
     */
    public function setDatos($data, $model) {

        DB::beginTransaction();

        try {

            $aux = [];
            foreach ($data as $linea) {

                foreach ($linea as $key => $item) {

                    $aux[$key] = $item;
                }

                $observer = $model::create($aux);

                $observer->save();
            }

            DB::commit();

        } catch (\Exception $e) {

            DB::rollBack();

            report($e);

            Sentry::captureException($e);
        }
    }

}

