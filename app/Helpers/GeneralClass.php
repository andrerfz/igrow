<?php

namespace App\Helpers;

use App\Company;
use Response;
use Storage;
use Sentry;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class GeneralClass
{


    /**
     * Save, edit and delete Image from Digital Ocean Spaces
     *
     * @param $request
     * @param $model
     * @param $folder
     * @param $column
     * @param bool $save
     * @param bool $edit
     * @param bool $delete
     * @return mixed
     */
    public function setFileToCDN($request, $model, $folder, $column, $save = false, $edit = false, $delete = false)
    {
        if ($delete && $model && $model->id) {
            Storage::disk('spaces')->delete($model->$column);
            $model->forceDelete();

            return response()->Json(['status' => 15, 'not' => ['tipo' => 'success', 'titulo' => '', 'mensaje' => 'Guardado corretamente']]);
        }

        if ($edit && $model && $model->id) {
            Storage::disk('spaces')->delete($model->$column);
            $file_url = Storage::disk('spaces')->put($folder, $request->file);
            $model->$column = $file_url;

            if ($model->save()) {
                return response()->Json(['status' => 15, 'not' => ['tipo' => 'success', 'titulo' => '', 'mensaje' => 'Guardado corretamente']]);
            }
        }

        if ($save && $request->file) {
            if ($model && $model->id) {
                $file_url = Storage::disk('spaces')->put($folder, $request->file);
                $model->$column = $file_url;

                if ($model->save()) {
                    return response()->Json(['status' => 15, 'not' => ['tipo' => 'success', 'titulo' => '', 'mensaje' => 'Guardado corretamente']]);
                }
            }

            if ($model->save()) {
                return response()->Json(['status' => 15, 'not' => ['tipo' => 'success', 'titulo' => '', 'mensaje' => 'Guardado corretamente']]);
            }

            return response()->Json(['status' => 0, 'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'Não foi possível guardar corretamente!']]);
        }

        return response()->Json(['status' => 0, 'not' => ['tipo' => 'warning', 'titulo' => '', 'mensaje' => 'Introduz todos os dados obrigatórios!']]);
    }


    /**
     * Validating a TIE
     *
     * @param $company_number
     * @return mixed
     */
    private function tie_validation($company_number)
    {
        return false;
    }


    /**
     * Validating a CIF
     *
     * @param $company_number
     * @return mixed
     */
    private function cif_validation($company_number)
    {
        $company_number = strtoupper($company_number);
        if (preg_match('~(^[XYZ\d]\d{7})([TRWAGMYFPDXBNJZSQVHLCKE]$)~', $company_number, $parts)) {
            $control = 'TRWAGMYFPDXBNJZSQVHLCKE';
            $nie = array('X', 'Y', 'Z');
            $parts[1] = str_replace(array_values($nie), array_keys($nie), $parts[1]);
            $cheksum = substr($control, $parts[1] % 23, 1);
            return ($parts[2] == $cheksum);
        } elseif (preg_match('~(^[ABCDEFGHIJKLMUV])(\d{7})(\d$)~', $company_number, $parts)) {
            $checksum = 0;
            foreach (str_split($parts[2]) as $pos => $val) {
                $checksum += array_sum(str_split($val * (2 - ($pos % 2))));
            }
            $checksum = ((10 - ($checksum % 10)) % 10);
            return ($parts[3] == $checksum);
        } elseif (preg_match('~(^[KLMNPQRSW])(\d{7})([JABCDEFGHI]$)~', $company_number, $parts)) {
            $control = 'JABCDEFGHI';
            $checksum = 0;
            foreach (str_split($parts[2]) as $pos => $val) {
                $checksum += array_sum(str_split($val * (2 - ($pos % 2))));
            }
            $checksum = substr($control, ((10 - ($checksum % 10)) % 10), 1);
            return ($parts[3] == $checksum);
        }

        return false;
    }


    /**
     * Validating a CNPJ
     *
     * @param $company_number
     * @return mixed
     */
    private function cnpj_validation($company_number)
    {
        $company_number = preg_replace('/[^0-9]/', '', (string)$company_number);
        if (preg_match('/(\d)\1{13}/', $company_number) || (strlen($company_number) != 14)) {
            return false;
        }
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++) {
            $soma += $company_number{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        if ($company_number{12} != ($resto < 2 ? 0 : 11 - $resto)) return false;
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++) {
            $soma += $company_number{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;

        return $company_number{13} == ($resto < 2 ? 0 : 11 - $resto);
    }


    /**
     *  Check if a Company number is already in use
     *
     * @param $company_number
     * @param $country
     * @return bool|int
     */
    public function checkCompanyNumber($company_number, $country)
    {
        if ($country === 'pt-BR' && !$this->cnpj_validation($company_number)) {
            return 3;
        }
        if ($country == 'es' && !$this->cif_validation($company_number)) {
            return 3;
        }
        $client = Company::where('federal_tax_number', $company_number)->first();
        if ($client) {
            return 2;
        }

        return 1;
    }


    /**
     *  Permission return message
     *
     * @return bool|Response
     */
    public function checkPermissionReturn()
    {
        if (auth()->user()->language->check->en) {
            return response()->Json(['status' => 10, 'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'Sorry, you don\'t have permission!']]);
        }
        if (auth()->user()->language->check->es) {
            return response()->Json(['status' => 10, 'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'Lo sentimos, no tienes permiso!']]);
        }
        if (auth()->user()->language->check->ptBR) {
            return response()->Json(['status' => 10, 'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'Desculpe-nos, mas você não tem permissão!']]);
        }

        return false;
    }


    /**
     *  Permission return message
     *
     * @return bool|NotFoundHttpException
     * @throws NotFoundHttpException
     */
    public function checkPermissionErrorReturn()
    {
        if (auth()->user()->language->check->en) {
            return abort(403, 'Sorry, you don\'t have permission!');
        }
        if (auth()->user()->language->check->es) {
            return abort(403, 'Lo sentimos, no tienes permiso!');
        }
        if (auth()->user()->language->check->ptBR) {
            return abort(403, 'Desculpe-nos, mas você não tem permissão!');
        }

        return false;
    }


    /**
     *  Permission return message
     *
     * @return bool|NotFoundHttpException
     * @throws NotFoundHttpException
     */
    public function checkRegisterErrorReturn()
    {
        if (auth()->user()->language->check->en) {
            return abort(403, 'Sorry, dosen\'t exist the register!');
        }
        if (auth()->user()->language->check->es) {
            return abort(403, 'Lo sentimos, no existe el registro!');
        }
        if (auth()->user()->language->check->ptBR) {
            return abort(403, 'Desculpe-nos, não encontramos nenhum registro!');
        }

        return false;
    }


    /**
     *  Permission return message
     *
     * @return object|Response
     */
    public function returnResponseSave()
    {
        if (auth()->user()->language->check->en) {
            $saved = response()->Json(['status' => 1, 'not' => ['tipo' => 'success', 'titulo' => '', 'mensaje' => 'Guardado corretamente']]);
            $not_saved = response()->Json(['status' => 0, 'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'Não foi possível guardar correctamente!']]);
        }
        if (auth()->user()->language->check->es) {
            $saved = response()->Json(['status' => 1, 'not' => ['tipo' => 'success', 'titulo' => '', 'mensaje' => 'Guardado corretamente']]);
            $not_saved = response()->Json(['status' => 0, 'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'Não foi possível guardar correctamente!']]);
        }
        if (auth()->user()->language->check->ptBR) {
            $saved = response()->Json(['status' => 1, 'not' => ['tipo' => 'success', 'titulo' => '', 'mensaje' => 'Guardado corretamente']]);
            $not_saved = response()->Json(['status' => 0, 'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'Não foi possível guardar correctamente!']]);
        }

        return (object)compact('saved', 'not_saved');
    }


    /**
     *  Permission return message
     *
     * @return object|Response
     */
    public function returnResponseDelete()
    {
        if (auth()->user()->language->check->en) {
            $deleted = response()->Json(['status' => 1, 'not' => ['tipo' => 'success', 'titulo' => '', 'mensaje' => 'Guardado corretamente']]);
            $not_deleted = response()->Json(['status' => 0, 'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'Não foi possível guardar correctamente!']]);
        }
        if (auth()->user()->language->check->es) {
            $deleted = response()->Json(['status' => 1, 'not' => ['tipo' => 'success', 'titulo' => '', 'mensaje' => 'Guardado corretamente']]);
            $not_deleted = response()->Json(['status' => 0, 'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'Não foi possível guardar correctamente!']]);
        }
        if (auth()->user()->language->check->ptBR) {
            $deleted = response()->Json(['status' => 1, 'not' => ['tipo' => 'success', 'titulo' => '', 'mensaje' => 'Guardado corretamente']]);
            $not_deleted = response()->Json(['status' => 0, 'not' => ['tipo' => 'error', 'titulo' => '', 'mensaje' => 'Não foi possível guardar correctamente!']]);
        }

        return (object)compact('deleted', 'not_deleted');
    }

}

