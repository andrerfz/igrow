<?php

namespace App;

use App\Scopes\CompanyScope;
use Eloquent;

class ModulesCompany extends Eloquent
{

    protected $table = 'modules_to_company';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'expire_date'
    ];

    /** SCOPES ********************************************************************************************************/

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CompanyScope());
    }

}
