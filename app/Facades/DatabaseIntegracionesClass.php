<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class DatabaseIntegracionesClass extends Facade
{

    protected static function getFacadeAccessor() { return 'databaseIntegracionesClass'; }
}