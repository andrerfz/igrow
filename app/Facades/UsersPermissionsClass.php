<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class UsersPermissionsClass extends Facade
{
    protected static function getFacadeAccessor() { return 'usersPermissionsClass'; }
}