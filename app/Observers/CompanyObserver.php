<?php

namespace App\Observers;

use App;
use App\Company;

class CompanyObserver
{
    /**
     * Handle the roles "created" event.
     *
     * @param  Company  $roles
     * @return void|int|int
     */
    public function created(Company $roles)
    {
        //
    }


    /**
     * Listen created event.
     * 
     * @param Company $item
     * @return void|int|int
     */
    public function creating(Company $item)
    {

        $item->hash_id = md5(rand()) . md5(rand()) . md5(rand());
        
    }


    /**
     * Handle the user "saving" event.
     *
     * @param Company $roles
     * @return void|int
     */
    public function saving(Company $roles)
    {
        //
    }

    /**
     * Handle the roles "updated" event.
     *
     * @param  Company  $roles
     * @return void|int
     */
    public function updated(Company $roles)
    {
        //
    }

    /**
     * Handle the roles "deleted" event.
     *
     * @param  Company  $roles
     * @return void|int
     */
    public function deleted(Company $roles)
    {
        //
    }

    /**
     * Handle the roles "restored" event.
     *
     * @param  Company  $roles
     * @return void|int
     */
    public function restored(Company $roles)
    {
        //
    }

    /**
     * Handle the roles "force deleted" event.
     *
     * @param  Company  $roles
     * @return void|int
     */
    public function forceDeleted(Company $roles)
    {
        //
    }
}
