<?php

namespace App\Observers;

use App;
use App\Languages;
use App\Roles;

class RolesObserver
{
    /**
     * Handle the roles "created" event.
     *
     * @param  Roles  $roles
     * @return void
     */
    public function created(Roles $roles)
    {
        //
    }

    /**
     * Handle the user "saving" event.
     *
     * @param Roles $roles
     * @return void
     */
    public function saving(Roles $roles)
    {
        if (auth()->check()) {

            $language = Languages::where('name', App::getLocale())->first();

            $roles->company_id = auth()->user()->company_id;
            $roles->lang_id = $language ? $language->id : null;
        }
    }

    /**
     * Handle the roles "updated" event.
     *
     * @param  Roles  $roles
     * @return void
     */
    public function updated(Roles $roles)
    {
        //
    }

    /**
     * Handle the roles "deleted" event.
     *
     * @param  Roles  $roles
     * @return void
     */
    public function deleted(Roles $roles)
    {
        //
    }

    /**
     * Handle the roles "restored" event.
     *
     * @param  Roles  $roles
     * @return void
     */
    public function restored(Roles $roles)
    {
        //
    }

    /**
     * Handle the roles "force deleted" event.
     *
     * @param  Roles  $roles
     * @return void
     */
    public function forceDeleted(Roles $roles)
    {
        //
    }
}
