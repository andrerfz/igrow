<?php

namespace App\Observers;

use App\Languages;
use App\User;
use App;

class UserObserver
{
    /**
     * Handle the user "creating" event.
     *
     * @param User $user
     * @return mixed
     */
    public function creating(User $user)
    {
        if (auth()->check()) {

        }
    }

    /**
     * Handle the user "saving" event.
     *
     * @param User $user
     * @return mixed
     */
    public function saving(User $user)
    {
        if (auth()->check()) {

            $language = Languages::where('name', App::getLocale())->first();

            $user->company_id = auth()->user()->company_id;
            $user->lang_id = $language ? $language->id : null;
        }
    }

    /**
     * Handle the user "updated" event.
     *
     * @param User $user
     * @return mixed
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param User $user
     * @return mixed
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the user "restored" event.
     *
     * @param User $user
     * @return mixed
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param User $user
     * @return mixed
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
