<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Modules extends Eloquent
{
    use SoftDeletes;


    protected $table = 'modules';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'locked',
        'active'
    ];


    /** RELACIONES ****************************************************************************************************/

    public function platform()
    {
        return $this->belongsTo(Platform::class, 'id', 'platform_id');
    }

    public function module_language()
    {
        return $this->hasOne(ModulesLanguage::class, 'module_id', 'id');
    }
}
