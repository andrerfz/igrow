<?php

namespace App;

use App\Scopes\LanguageScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent;

class Permissions extends Eloquent
{
    use SoftDeletes;

    protected $table = 'permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lang_id',
        'nombre',
        'description',
        'view',
        'create',
        'update',
        'delete',
        'full',
        'locked',
        'active'
    ];


    /** SCOPES ********************************************************************************************************/


    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LanguageScope());
    }


    /** RELACIONES ****************************************************************************************************/


    public function languages()
    {
        return $this->hasOne(Languages::class, 'id', 'lang_id')->withTrashed();
    }

    public function role()
    {
        return $this->hasOne(Roles::class, 'permission_id', 'id')->whereNull('company_id');
    }

    public function users()
    {
        return $this->hasMany(Users::class, 'permission_id', 'id')->withTrashed();
    }
}
