<?php

namespace App;

use App\Scopes\CompanyScope;
use Eloquent;

/** @mixin Eloquent */

class Users extends User
{


    /** SCOPES ********************************************************************************************************/


    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CompanyScope());
    }

}
