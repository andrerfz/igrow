<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App;

class GeneralClassServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('GeneralClass', function () {

            return new \App\Helpers\GeneralClass;
        });
    }
}
