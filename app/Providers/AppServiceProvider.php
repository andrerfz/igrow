<?php

namespace App\Providers;


use Illuminate\Support\ServiceProvider;


use App\Observers\RolesObserver;
use App\Observers\UserObserver;
use App\User;
use App\Roles;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Roles::observe(RolesObserver::class);
        User::observe(UserObserver::class);
    }
}
