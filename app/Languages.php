<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Languages extends Eloquent
{

    use SoftDeletes;


    protected $table = 'languages';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'locked',
        'active'
    ];


    /** ATTRIBUTES *****************************************************************************************************/


    protected $appends = ['check'];


    public function getCheckattribute()
    {

        $en = auth()->user()->language->name == 'en' ?: false;
        $es = auth()->user()->language->name == 'es' ?: false;
        $ptBR = auth()->user()->language->name == 'pt-BR' ?: false;

        return (object)compact('en', 'es', 'ptBR');
    }


    /** RELACIONES ****************************************************************************************************/


    public function permissions()
    {
        return $this->hasMany(Permissions::class, 'lang_id', 'id')->withTrashed();
    }
}
