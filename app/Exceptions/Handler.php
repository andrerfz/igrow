<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\MessageBag;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param \Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        if (app()->bound('sentry') && $this->shouldReport($exception)){
            app('sentry')->captureException($exception);
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {


        if ($exception instanceof CustomException) {


            if ($exception->getCode() === '401') {

                if (Auth::user()->language->name === 'en') {
                    $errors = new MessageBag(['permiso' => ['Session expired. Try to relogin.']]);
                }
                if (Auth::user()->language->name === 'es') {
                    $errors = new MessageBag(['permiso' => ['La sesión se ha caducado volve a iniciar la sesion.']]);
                }
                if (Auth::user()->language->name === 'pt-BR') {
                    $errors = new MessageBag(['permiso' => ['A sua sessão expirou. Por favor, volte a fazer o login novamente.']]);
                }

                if (Request::getHost() == config('app.admin').config('app.dashboard').config('app.domain') && !Auth::user()->role->permission->full) {

                    Auth::logout();

                    return redirect()->route('admin.login', app()->getLocale())->withErrors($errors)->withInput();
                }

                if (Request::getHost() == config('app.dashboard').config('app.domain') && !Auth::user()->permissions->aplication_client->global->view) {

                    Auth::logout();

                    return redirect()->route('login', app()->getLocale())->withErrors($errors)->withInput();
                }
            }


            if ($exception->getCode() === '403') {

                if (Auth::user()->language->name === 'en') {
                    return abort(403, 'Sorry, you don\'t permission!');
                }
                if (Auth::user()->language->name === 'es') {
                    return abort(403, 'Lo sentimos, no tienes permiso!');
                }
                if (Auth::user()->language->name === 'pt-BR') {
                    return abort(403, 'Desculpe-nos, mas você não tem permissão!');
                }
            }

            return response()->view('errors.500', [], 500);
        }

        return parent::render($request, $exception);
    }
}
