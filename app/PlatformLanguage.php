<?php

namespace App;

use App\Scopes\LanguageScope;
use Eloquent;

class PlatformLanguage extends Eloquent
{

    protected $table = 'platform_to_language';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /** SCOPES ********************************************************************************************************/

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LanguageScope());
    }

}
