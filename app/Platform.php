<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent;

class Platform extends Eloquent
{
    use SoftDeletes;

    protected $table = 'platform';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'locked',
        'active'
    ];


    /** RELACIONES ****************************************************************************************************/

    public function modules()
    {
        return $this->hasMany(Modules::class, 'platform_id', 'id');
    }

    public function platform_language()
    {
        return $this->hasOne(PlatformLanguage::class, 'platform_id', 'id');
    }
}
