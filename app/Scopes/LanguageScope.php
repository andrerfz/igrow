<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class LanguageScope implements Scope
{

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param Builder $builder
     * @param Model $model
     * @return mixed
     */
    public function apply(Builder $builder, Model $model)
    {

        $table = $model->getTable();

        if (auth()->check()) {

            $lang_id = auth()->user()->lang_id;

            $builder->where(function ($q) use ($table, $lang_id) {

                $q->where($table . '.lang_id', $lang_id);
            });
        }
    }

}