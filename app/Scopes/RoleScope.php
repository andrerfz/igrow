<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class RoleScope implements Scope
{

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param Builder $builder
     * @param Model $model
     * @return mixed
     */
    public function apply(Builder $builder, Model $model)
    {

        $table = $model->getTable();

        if (auth()->check()) {

            //Client Demo
            if (auth()->user()->company->form_register) {

                $builder->where(function ($q) use ($table) {

                    $q->whereIn($table . '.id', [3, 4, 5, 10, 11, 12, 17, 18, 19]);
                });
            }
        }
    }

}