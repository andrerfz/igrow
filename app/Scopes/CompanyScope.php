<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class CompanyScope implements Scope
{

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param Builder $builder
     * @param Model $model
     * @return mixed
     */
    public function apply(Builder $builder, Model $model)
    {

        $table = $model->getTable();

        if (auth()->check()) {

            $company = auth()->user()->company_id;

            if ($table == 'company') {

                $builder->where(function ($q) use ($table, $company) {

                    $q->where($table . '.id', $company);

                })->orWhere(function ($q) use ($table, $company) {

                    $q->where($table . '.company_id', $company);
                });

            } else {

                $company = auth()->user()->company_id;

                $builder->where(function ($q) use ($table, $company) {

                    $q->where($table . '.company_id', $company);

                })->orWhere(function ($q)use($table){

                    $q->whereNull($table . '.company_id');
                });
            }
        }
    }

}