<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Support\Facades\Auth;

class NewUser extends ResetPasswordNotification
{
    use Queueable;



    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $token, $forgot)
    {
        $this->user = $user;
        $this->token = $token;
        $this->forgot = $forgot;
    }



    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }



    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($this->forgot) {

            if (Auth::user()->language->name === 'us') {
                return (new MailMessage)
                    ->subject('Welcome to iGrow.')
                    ->greeting('Welcome, '.$this->user->name.'!')
                    ->line('You are receiving this email because your account are blocked.')
                    ->line('Reset your password to access to your organization again.')
                    ->action('Reset password', url(config('app.dashboard.url').route('password.reset', $this->token, false)))
                    ->salutation('Best regards, '. config('app.name'));
            }
            if (Auth::user()->language->name === 'es') {
                return (new MailMessage)
                    ->subject('Bienvenido a iGrow!')
                    ->greeting('Bienvenido, '.$this->user->name.'!')
                    ->line('Usted recibió este correo electrónico porque su cuenta fue bloqueada recientemente.')
                    ->line('Restablezca su contraseña para obtener acceso a su organización.')
                    ->action('Restablecer contraseña', url(config('app.dashboard.url').route('password.reset', $this->token, false)))
                    ->salutation('Atentamente, '. config('app.name'));
            }
            if (Auth::user()->language->name === 'pt-BR') {
                return (new MailMessage)
                    ->subject('Bem-vindo à iGrow.')
                    ->greeting('Bem-vindo, '.$this->user->name.'!')
                    ->line('Você recebeu este e-mail porque sua conta bloqueada recentemente.')
                    ->line('Redefina sua senha para obter acesso a sua organização.')
                    ->action('Redefinir senha', url(config('app.dashboard.url').route('password.reset', $this->token, false)))
                    ->salutation('Atentamente, '. config('app.name'));
            }
        } else {

            if (Auth::user()->language->name === 'us') {
                return (new MailMessage)
                    ->subject('Welcome to iGrow.')
                    ->greeting('Welcome, '.$this->user->name.'!')
                    ->line('You are receiving this email because we received a password reset request for your account.')
                    ->line('Reset your password to access to your organization again.')
                    ->action('Reset password', url(config('app.dashboard.url').route('password.reset', $this->token, false)))
                    ->salutation('Best regards, '. config('app.name'));

            }
            if (Auth::user()->language->name === 'es') {
                return (new MailMessage)
                    ->subject('Bienvenido a iGrow!')
                    ->greeting('Bienvenido, '.$this->user->name.'!')
                    ->line('Usted recibió este correo electrónico porque su cuenta se creó recientemente.')
                    ->line('Restablezca su contraseña para obtener acceso a su organización.')
                    ->action('Restablecer contraseña', url(config('app.dashboard.url').route('password.reset', $this->token, false)))
                    ->salutation('Atentamente, '. config('app.name'));
            }
            if (Auth::user()->language->name === 'pt-BR') {
                return (new MailMessage)
                    ->subject('Bem-vindo à iGrow.')
                    ->greeting('Bem-vindo, '.$this->user->name.'!')
                    ->line('Você recebeu este e-mail porque sua conta foi criada recentemente.')
                    ->line('Redefina sua senha para obter acesso a sua organização.')
                    ->action('Redefinir senha', url(config('app.dashboard.url').route('password.reset', $this->token, false)))
                    ->salutation('Atentamente, '. config('app.name'));
            }
        }
    }



    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [];
    }


}
