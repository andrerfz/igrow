<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;


class ResetPassword extends ResetPasswordNotification
{
    use Queueable;



    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }



    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }



    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (App::getLocale() === 'us') {
            return (new MailMessage)
                ->subject(Lang::getFromJson('Reset Password Notification'))
                ->line(Lang::getFromJson('You are receiving this email because we received a password reset request for your account.'))
                ->action(Lang::getFromJson('Reset Password'), url(config('app.dashboard.url').route('password.reset', ['token' => $this->token], false)))
                ->line(Lang::getFromJson('This password reset link will expire in :count minutes.', ['count' => config('auth.passwords.users.expire')]))
                ->line(Lang::getFromJson('If you did not request a password reset, no further action is required.'));
        }
        if (App::getLocale() === 'es') {
            return (new MailMessage)
                ->line('Has recibido este correo electrónico porque recibimos una solicitud de restablecimiento de contraseña para tu cuenta.')
                ->action('Restablecer contraseña', url(config('app.dashboard.url').route('password.reset', $this->token, false)))
                ->line(Lang::getFromJson('El link para restablecer la contraseña expira en: count minutos.', ['count' => config('auth.passwords.users.expire')]))
                ->line('Si no has solicitado restablecer la contraseña, no se requieren más acciones por tu parte.');
        }
        if (App::getLocale() === 'pt-BR') {
            return (new MailMessage)
                ->line('Você recebeu este e-mail porque recebemos uma solicitação de redefinição de senha para sua conta.')
                ->action('Redefinir senha', url(config('app.dashboard.url') . route('password.reset', $this->token, false)))
                ->line(Lang::getFromJson('O link para redefinir sua senha expira em: count minutos.', ['count' => config('auth.passwords.users.expire')]))
                ->line('Se você não solicitou a redefinição da senha, nenhuma ação adicional é necessária de sua parte.');
        }
    }



    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [];
    }


}
