<?php

namespace App;

use App\Scopes\CompanyScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent;
use Storage;

class Company extends Eloquent
{
    use SoftDeletes;

    protected $table = 'company';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hash_id',
        'company_id',
        'logo',
        'business_name',
        'trading_name',
        'federal_tax_number',
        'state_tax_number',
        'direction',
        'town',
        'state',
        'province',
        'country',
        'postcode',
        'contact',
        'mobile',
        'phone',
        'email',
        'web',
        'branch',
        'retail',
        'wholesale',
        'terms',
        'form_register',
        'max_company',
        'max_users',
        'external_id',
    ];



    /** ATTRIBUTES *****************************************************************************************************/


    protected $appends = ['logo_url', 'full_name', 'branch_name'];


    public function getLogoUrlAttribute()
    {
        return $this->attributes['logo'] ? Storage::disk('spaces')->url($this->attributes['logo']) : asset('assets/media/users/logo.jpg');
    }

    public function getFullNameAttribute()
    {
        if ($this->attributes['trading_name'] && $this->attributes['business_name']) {
            return $this->attributes['trading_name'] . ' (' . $this->attributes['business_name'] . ')';
        }
        if ($this->attributes['trading_name']) {
            return $this->attributes['trading_name'];
        }

        return $this->attributes['business_name'];
    }

    public function getBranchNameAttribute(){
        return $this->getParentName($this);
    }

    public function getParentName($este){
        if ($este->parent) {
            return $this->getParentName($este->parent) . ' > ' . $este->nombre;
        }

        return $este->nombre;
    }

    /** SCOPES ********************************************************************************************************/


    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CompanyScope());
    }


    /** RELACIONES ****************************************************************************************************/

    public function permissions()
    {

        return $this->hasMany(Permissions::class , 'lang_id', 'id')->withTrashed();
    }

    public function parent(){

        return $this->belongsTo(__CLASS__, 'company_id', 'id');
    }

    public function childs(){

        return $this->hasMany(__CLASS__, 'company_id', 'id');
    }

    public function parents(){

        return $this->parent()->with('parent');
    }

    public function children(){

        return $this->childs()->with('childs');
    }

    public function users(){

        return $this->hasMany(Users::class, 'company_id', 'id');
    }

    public function modules(){

        return $this->belongsToMany(ModulesCompany::class, 'modules_to_company', 'company_id', 'id')->withPivot('id_producto', 'id_proveedor');
    }
}
