<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Notifications\NewUser;
use App\Notifications\ResetPassword;
use Storage;
use PermissionsHelper;

class User extends Authenticatable
{

    use Notifiable;
    use SoftDeletes;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'lang_id',
        'company_id',
        'place_id',
        'role_id',

        'plafform_modules_permissions',

        'picture',
        'name',
        'last_name',
        'bio',
        'identification_number',
        'social_number',
        'place_address',
        'degrees',
        'school_level',
        'direction',
        'city',
        'province',
        'postal',
        'telephone',
        'email',
        'email_verified_at',
        'password',

        'doc_1',
        'doc_2',
        'doc_3',
        'doc_4',
        'doc_5',
        'doc_6',
        'doc_7',
        'doc_8',
        'doc_9',
        'doc_10',

        'locked',
        'activo'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];


    /** NOTIFICATIONS **************************************************************************************************/


    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function sendNewUserNotification($user, $token, $forgot)
    {
        $this->notify(new NewUser($user, $token, $forgot));
    }


    /** ATTRIBUTES *****************************************************************************************************/


    protected $appends = ['picture_url', 'full_name', 'permissions'];

    public function getPictureUrlAttribute()
    {

        return $this->attributes['picture'] ? Storage::disk('spaces')->url($this->attributes['picture']) : asset('assets/media/users/default.jpg');
    }

    public function getFullNameAttribute()
    {

        return $this->attributes['name'] . ' ' . $this->attributes['last_name'];
    }

    public function getPermissionsAttribute()
    {

        return PermissionsHelper::getUserPermissions(auth()->user(), auth()->user()->plafform_modules_permissions ? false : true, false, 'client');
    }


    /** MUTATORS ******************************************************************************************************/


    public function setEmailAttribute($value)
    {

        $this->attributes['email'] = strtolower($value);
    }

    public function setNameAttribute($value)
    {

        $this->attributes['name'] = ucfirst(strtolower($value));
    }

    public function setLastNameAttribute($value)
    {

        $this->attributes['last_name'] = ucwords(strtolower($value));
    }


    /** RELACIONES ****************************************************************************************************/


    public function language()
    {

        return $this->hasOne(Languages::class, 'id', 'lang_id')->withTrashed();
    }

    public function company()
    {

        return $this->hasOne(Company::class, 'id', 'company_id')->withTrashed();
    }

    public function role()
    {

        return $this->belongsTo(Roles::class, 'role_id', 'id')->withTrashed();
    }

}
