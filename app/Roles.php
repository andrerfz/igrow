<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes\CompanyScope;
use App\Scopes\LanguageScope;
use App\Scopes\RoleScope;

class Roles extends Eloquent
{
    use SoftDeletes;

    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'permission_id', 'company_id', 'name', 'description', 'locked', 'active'
    ];

    /** SCOPES ********************************************************************************************************/

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LanguageScope());
        static::addGlobalScope(new CompanyScope());
        static::addGlobalScope(new RoleScope());
    }

    /** RELACIONES ****************************************************************************************************/

    public function users()
    {

        return $this->hasMany(Users::class, 'role_id', 'id')->withTrashed();
    }

    public function permission()
    {

        return $this->hasOne(Permissions::class, 'id', 'permission_id')->withTrashed();
    }

    public function permission_without_full()
    {

        return $this->hasOne(Permissions::class, 'id', 'permission_id')->where('full', false)->withTrashed();
    }

}
